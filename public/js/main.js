class HeaderBtn extends React.PureComponent {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      React.createElement("span", {
        onClick: this.props.onClick,
        className: "nr h-btn " + (this.props.on ? "highlight" : "") },

      this.props.title,
      React.createElement("i", { className: _font_awsome_select(this.props.sort) })));


  }}


class DlgCaption extends React.PureComponent {
  _get_time_str() {
    return new Date().toISOString();
  }

  shouldComponentUpdate(nextProps, nextState) {
    return false; //never
  }

  render() {
    return (
      React.createElement("div",
      {
        className: "toolbar"
      },
      React.createElement("span",
      {
        className: "fl-right"
      },
      React.createElement("span", null),
      React.createElement("span",
      {
         className: "txt-time"
      }, this._get_time_str()))));

  }}


class App extends React.Component {
  constructor(props) {
    super(props);
    ll("getInitialState");
    this._btn_recent_click = this._btn_recent_click.bind(this);
    this._eval_recent_sort = this._eval_recent_sort.bind(this);
    this._eval_total_sort = this._eval_total_sort.bind(this);
    this._btn_total_click = this._btn_total_click.bind(this);
    this._has_all_data_arrived = this._has_all_data_arrived.bind(this);
    this._camper_graceous_init = this._camper_graceous_init.bind(this);
    this._camper_merge_data = this._camper_merge_data.bind(this);

    this._fetch_data(
    "https://s3-us-west-2.amazonaws.com/s.cdpn.io/594328/xhr-test-weekly.json",
    "recent");

    this._fetch_data(
    "https://s3-us-west-2.amazonaws.com/s.cdpn.io/594328/xhr-test-all.json",
    "alltime");

    async_exec(this.props.modal._show);
    this.state = {
      state: 0,
      sort_on: "total",
      sort: "desc",
      queue: [] };

  }

  _eval_recent_sort() {
    var rc = "all";
    if (this.state.sort_on == "recent") {
      rc = this.state.sort;
    }
    ll({
      recent: rc });

    return rc;
  }

  _eval_total_sort() {
    var rc = "all";
    if (this.state.sort_on == "total") {
      rc = this.state.sort;
    }
    ll({
      total: rc });

    return rc;
  }

  _btn_recent_click(evt) {
    var sort_on = "recent";
    var sort = "desc";
    if (this.state.sort_on == "recent" && this.state.sort == "desc") {
      sort = "asc";
    }
    this.setState({
      sort_on: sort_on,
      sort: sort });

  }

  _btn_total_click(evt) {
    var sort_on = "total";
    var sort = "desc";
    if (this.state.sort_on == "total" && this.state.sort == "desc") {
      sort = "asc";
    }
    this.setState({
      sort_on: sort_on,
      sort: sort });

  }

  _has_all_data_arrived() {
    return this.camper_results && this.camper_results.complete == 2;
  }

  _camper_graceous_init() {
    this.camper_results = this.camper_results || {};
    this.camper_results.data = this.camper_results.data || new Map();
    this.camper_results.complete = 0;
  }

  _camper_merge_data(campers, context) {
    //console.log('here', Object.keys(campers))
    const base = "https://www.freecodecamp.org";
    for (const camper of campers.directory_items) {
      //console.log(camper)
      const { likes_received, user: { username, avatar_template } } = camper;
      const found = this.camper_results.data.get(username) || {
        username,
        imguri: `${base}${avatar_template.replace("{size}", "64")}`,
        alltime: 'NA',
        recent: 'NA' };

      this.camper_results.data.set(username, found);
      found[context] = likes_received;
      //console.log(this.camper_results.data.size);
    }
  }

  _fetch_data(uri, context) {
    console.log(uri);
    this._camper_graceous_init();
    const xhr = new XMLHttpRequest();

    xhr.open("GET", uri);
    xhr.setRequestHeader("Content-Type", "application/json");
    xhr.setRequestHeader("Access-Control-Allow-Origin", "https://s.codepen.io");
    xhr.setRequestHeader("Accept", "application/json");
    xhr.withCredentials = false;

    xhr.onerror = evt => {
      this.state.queue.push({
        tag: "fail " + context,
        xhr_error: "recent data fetch failed",
        xhr_status: xhr.status,
        xhr_ready_state: xhr.readyState,
        xhr_status_text: xhr.statusText });

      this.setState({
        state: -1,
        queue: this.state.queue.slice() 
      });
    };

    xhr.onload = function onload(evt) {
      console.log("data-received");
      if (evt.target.status >= 200 && evt.target.status < 300) {
        this._camper_merge_data(JSON.parse(evt.target.response), context);
        this.camper_results.complete++;
        this.state.queue.push({
          tag: "ok " + context,
          xhr_status: xhr.status,
          xhr_ready_state: xhr.readyState,
          xhr_status_text: xhr.statusText });

        const state = this._has_all_data_arrived() ? 2 : this.state.state;
        if (this._has_all_data_arrived()) {
          console.log('all data has arrived');
          async_exec(this.props.modal._hide);
          console.log(`state is now: ${state}, old state was:${this.state.state}`);
        }
        this.setState({
          state,
          queue: this.state.queue.slice() });
        //partial data fetched
        return;
      }
      this.state.queue.push({
        tag: "fail " + context,
        xhr_status: xhr.status,
        xhr_ready_state: xhr.readyState,
        xhr_status_text: xhr.statusText });

      this.setState({
        state: -1,
        queue: this.state.queue.slice() });

    }.bind(this);

    try {
      xhr.send();
      console.log("request sent");
    } catch (e) {
      console.log("error on request sent");
      this.state.queue.push({
        tag: "fail, " + context + ", exception on send",
        xhr_exception: e,
        xhr_status: xhr.status,
        xhr_ready_state: xhr.readyState,
        xhr_status_text: xhr.statusText });

      this.setState({
        state: -1,
        queue: this.state.queue.slice() 
      });
    }
  }

  componentWillMount() {
    ll("componentWillMount");
  }

  componentWillReceiveProps(nextProps) {
    ll("componentWillReceiveProps");
  }

  componentWillUpdate(nextProps, nextState) {
    ll("componentWillUpdate");
  }

  _bounce_in() {
    ll("bounce");
    add_class("stamp", ["bounceIn", "animated"]);
  }

  componentDidUpdate(prevProps, prevState) {
    ll("componentDidUpdate");
    this._bounce_in();
  }

  componentWillUnmount() {
    ll("componentWillUnmount");
  }

  render() {
    console.log(`rendering app`);
    if (!this._has_all_data_arrived()) {
      return React.createElement("div", null);
    }
    console.log(`rendering app data`);
    //sort
    const { sort, sort_on, state } = this.state; // "this" problem;
    console.log(`ln:257 state is: sort:${sort}, sort_on:${sort_on}, state:${state}`);
    const data = Array.from(this.camper_results.data.values()).sort((a, b) => {
      var rc;
      switch (sort_on) {
        case "recent":
          if (sort === "asc") {
            rc = a.recent - b.recent;
          } else {
            rc = b.recent - a.recent;
          }
          break;
        case "total":
          if (sort === "asc") {
            rc = a.alltime - b.alltime;
          } else {
            rc = b.alltime - a.alltime;
          }
          break;
        default:
          rc = 0;}


      return rc;
    });

    console.log(`ln:282 data length before slicing:${data.length}`);

    if (sort === "desc") {
      data.splice(Math.max(data.length - 100, 100));
    } else {
      data.splice(100);
    }

    console.log(`ln:282 data length after slicing:${data.length}`);
    
    var l_usr_stat = data.reduce((p, c, i) => {
      if (c.username.length > p.str.length) {
        p.str = c.username;
        p.idx = i;
      }
      return p;
    },
    {
      str: "",
      idx: -1 });



    console.log(`l_usr_stat: ${l_usr_stat.idx} ${l_usr_stat.str}`);

    const dataRows = data.map((c, idx, arr) => React.createElement(DataRow, {
      siralama: this.state.sort == "asc" ? arr.length - idx : idx + 1,
      key: c.username,
      username: c.username,
      imguri: c.imguri,
      alltime: c.alltime,
      recent: c.recent }));



    return (
      React.createElement("div", { className: "inner-canvas container" },
      React.createElement("div", { id: "stamp", className: "stamp fit" },
      " ",
      React.createElement("img", { id: "brownie-stamp", src: imgbase64 })),

      React.createElement(DlgCaption, null),
      React.createElement(DlgHeader, null),
      React.createElement("div", { className: "fixed-header" },
      React.createElement("div", { className: "table" },
      React.createElement("div", { className: "table-header" },
      React.createElement("span", null, " Sıralama: "), " ", React.createElement("span", null, " Kullanıcı Adı: "), " ",
      React.createElement(HeaderBtn, {
        id: "btn_recent",
        title: "Son 30 gün içerisindeki puanlar ",
        on: sort_on == "recent",
        sort: this._eval_recent_sort(),
        onClick: this._btn_recent_click }),
      " ",
      React.createElement(HeaderBtn, {
        id: "btn_total",
        title: "Toplam Puanlar",
        on: sort_on == "total",
        sort: this._eval_total_sort(),
        onClick: this._btn_total_click }),
      " "),
      " ",
      " "),
      " "),
      " ",
      React.createElement("div", { className: "table-scrollable-data" },
      React.createElement("div", { className: "table moved" },
      " ",
      " ",
      React.createElement("div", { className: "table-header" },
      React.createElement("span", null, " Rank: "), " ", React.createElement("span", null, " FCC User Name: "), " ",
      React.createElement(HeaderBtn, {
        key: "dummy1",
        title: "Point accrued in Last 30 days ",
        on: false,
        sort: "all" }),
      " ",
      React.createElement(HeaderBtn, {
        key: "dummy2",
        title: "Points Total ",
        on: false,
        sort: "all" }),
      " "),
      " ",
      dataRows, " "),
      " "),
      " "));


  }

  componentDidMount() {
    ll("componentDidMount");
    if (!this._has_all_data_arrived()) {
    }
  }

  shouldComponentUpdate(nextProps, nextState) {
    ll("shouldComponentUpdate");
    ll({
      next_state: nextState,
      next_props: nextProps,
      cur_state: this.state });

    if (nextState.state.state == 0) {
      return false;
    }
    if (
    nextState.state < 0 && (
    this.state == undefined || this.state.state >= 0))
    {
      return true;
    }
    if (nextState.state == 2 && this.state.state != 2) {
      return true;
    }
    if (
    nextState.state == 2 &&
    this.state.state == 2 && (
    nextState.sort_on != this.state.sort_on ||
    nextState.sort != this.state.sort))
    {
      return true;
    }

    return false;
  }}


class DlgHeader extends React.PureComponent {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      React.createElement("div", { className: "header" },
      React.createElement("div", null,
      " ",
      " "),
      " "));


  }}


class SpinnerDialog extends React.Component {
  constructor(props) {
    super(props);
    ll("dd-getInitialState");
    this.anim_queue = [];
    this._dunk_dlg = this._dunk_dlg.bind(this);
    this._show = this._show.bind(this);
    this._animation_end = this._animation_end.bind(this);
    this._fn_down_animate = this._fn_down_animate.bind(this);
    this._fn_up_animate = this._fn_up_animate.bind(this);
    this._queue_anim_action = this._queue_anim_action.bind(this);
    this._hide = this._hide.bind(this);
    //this._show();
  }

  _execute_op(operation) {
    if (operation.state != "exec") {
      operation.state = "exec";
      operation.op();
    }
  }

  _queue_anim_action(operations) {
    if (operations == null || operations == undefined) {
      return;
    }
    if (!(operations instanceof Array)) {
      operations = [operations];
    }
    while (operations.length > 0) {
      this.anim_queue.unshift(operations.shift());
    }
    var first_operation = this.anim_queue[this.anim_queue.length - 1];
    this._execute_op(first_operation);
  }

  _animation_end() {
    ll("anitmation-end");
    ll("animque-length:" + this.anim_queue.length);
    this.anim_queue.pop(); 
    this._dunk_dlg();
    while (this.anim_queue && this.anim_queue.length > 0) {
      var peek = this.anim_queue[this.anim_queue.length - 1];
      if (peek.type != "anim") {
        peek = this.anim_queue.pop();
        this._execute_op(peek); //execute
        continue;
      }
      this._execute_op(peek); 
      break;
    }
  }

  _dunk_dlg() {
    return remove_class("dialog", ["animated", "fadeInDown", "fadeOutUp"]);
  }

  _fn_down_animate() {
    const dlg = this._dunk_dlg();
    add_class("modal-pane", "on");
    dlg && add_class(dlg, ["fadeInDown", "animated"]);
  }

  _fn_up_animate() {
    var dlg = this._dunk_dlg();
    dlg && add_class(dlg, ["fadeOutUp", "animated"]);
    remove_class("modal-pane", "on");
  }

  _show(callback) {
    ll("dd-show");
    this._queue_anim_action({
      type: "anim",
      op: this._fn_down_animate });

    this._queue_anim_action(callback);
  }

  _hide(callback) {
    ll("dd-hide");
    this._queue_anim_action({
      type: "anim",
      op: this._fn_up_animate });

    this._queue_anim_action(callback);
  }

  componentDidMount() {
    ll("dd-componentDidMount");
    const dlg = document.getElementById("dialog");
    dlg && dlg.addEventListener("animationend", this._animation_end);
  }

  componentWillUnmount() {
    ll("dd-componentWillUnmount");
    var dlg = this.refs.dialog;
    dlg && dlg.removeEventListener("animationend", this._animation_end);
  }

  componentWillMount() {
    ll("dd-componentWillMount");
  }

  componentWillReceiveProps(nextProps) {
    ll("dd-componentWillReceiveProps");
  }

  componentWillUpdate(nextProps, nextState) {
    ll("dd-componentWillUpdate");
  }

  componentDidUpdate(prevProps, prevState) {
    ll("dd-componentDidUpdate");
  }

  shouldComponentUpdate(nextProps, nextState) {
    ll("dd-shouldComponentUpdate");
    return false;
  }

  render() {
    ll("dd-render");
    return (
      React.createElement("div", { id: "dialog", className: "load-dialog fit" },
      React.createElement("div", { className: "dlg_header" },
      React.createElement("p", null, " Loading Data "), " "),
      " ",
      React.createElement("div", { className: "dlg-body" },
      React.createElement("div", { className: "spinner" }, " "), " "),
      " ",
      React.createElement("div", { className: "dlg-footer" }, " "), " "));


  }}


/*
     Simple Elements
     */


function DataRow(props) {
  return (
    React.createElement("div", { className: "table-data-row animated fadeInRight" },
    React.createElement("span", null, " ", props.siralama, " "), " ",
    React.createElement("span", null,
    " ",
    React.createElement("img", { src: props.imguri }), "\xA0\xA0",

    props.username),

    React.createElement("span", { className: "nr" }, " ", props.recent, " "), " ",
    React.createElement("span", { className: "nr" }, " ", props.alltime, " "), " "));


}


function ll(data) {
  //console.log( data );
}

function async_exec(func) {
  window.setTimeout(func, 0);
}

function add_class(elt, class_names) {
  if (typeof elt === "string") {
    elt = document.getElementById(elt);
    if (!elt) {
      console.log("cannot find element:" + elt);
      return;
    }
  }
  if (!(class_names instanceof Array)) {
    class_names = [class_names];
  }
  class_names.forEach(function (_class) {
    elt.classList.add(_class);
  });
  return elt;
}

function remove_class(elt, class_names) {
  if (typeof elt == "string") {
    elt = document.getElementById(elt);
    if (!elt) {
      console.log("cannot find element:" + elt);
      return;
    }
  }

  if (!(class_names instanceof Array)) {
    class_names = [class_names];
  }
  class_names.forEach(function (_class) {
    elt.classList.remove(_class);
  });
  return elt;
}

function sort(data, prop) {
  data.sort(function (a, b) {
    if (a[prop] > b[prop]) {
      return 1;
    } else if (a[prop] < b[prop]) {
      return -1;
    }
    return 0;
  });
}

function _font_awsome_select(sort) {
  var rc;
  switch (sort) {
    case "all":
      rc = "fa fa-sort";
      break;
    case "asc":
      rc = "fa fa-sort-asc";
      break;
    case "desc":
      rc = "fa fa-sort-desc";
      break;
    default:
      rc = "fa  fa-sort";}

  return rc;
}

window.onload = function () {
  console.log("start");
  var spinner = ReactDOM.render(
  React.createElement(SpinnerDialog, null),
  document.getElementById("modal-pane"));

  ReactDOM.render(
  React.createElement(App, { modal: spinner }),
  document.getElementById("container"));

};


var imgbase64 = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAALwAAADyCAYAAAD3G6+3AAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyRpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuMy1jMDExIDY2LjE0NTY2MSwgMjAxMi8wMi8wNi0xNDo1NjoyNyAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENTNiAoTWFjaW50b3NoKSIgeG1wTU06SW5zdGFuY2VJRD0ieG1wLmlpZDpDNEVEOTBDRTVCQzIxMUUzODc2OTk4OUY1QTdENjUzQSIgeG1wTU06RG9jdW1lbnRJRD0ieG1wLmRpZDpDNEVEOTBDRjVCQzIxMUUzODc2OTk4OUY1QTdENjUzQSI+IDx4bXBNTTpEZXJpdmVkRnJvbSBzdFJlZjppbnN0YW5jZUlEPSJ4bXAuaWlkOkM0RUQ5MENDNUJDMjExRTM4NzY5OTg5RjVBN0Q2NTNBIiBzdFJlZjpkb2N1bWVudElEPSJ4bXAuZGlkOkM0RUQ5MENENUJDMjExRTM4NzY5OTg5RjVBN0Q2NTNBIi8+IDwvcmRmOkRlc2NyaXB0aW9uPiA8L3JkZjpSREY+IDwveDp4bXBtZXRhPiA8P3hwYWNrZXQgZW5kPSJyIj8++l1fAgAAs71JREFUeNrsXQecXFXVP296L9t7L9lU0kMKJXQITUCkoyJVqoKCnyAKqIgICNKUJl2QXgMEEtJ7Nptks73Xmd2dnV6//7lvZrMpJBtIMMre/F5m580rt5x7zv+ce8650uNbG2m0HJQyFse1OPQ4HsGxGsctOH6Gw4bjLRxXxK+9DkchjtdxvDPadSMvV1QU7Nf1qtEuOyBFieMoHBYcb+AojsViqyVJMvCPsWj0ZEmhWIzPU/Cpi99zTjQSycc1WpybFD93CY5f4vgjjiNxJONYhqNztIsPTFGMdsE3Lvo4kX+C4984lkbC4WeY2BsrN1ZuX71yNQg6FefPYmLf9PmnXz5/+61PgfijCqVyBhO7o621ZfPiz5fyw3DvLXEu/zlzfFy3Ep+HjXbzKMH/Jwtz3mn8RzQSZohyKgg1GPT7PPh7tlKlmsu/OTval/Y0Ny0cfmP+uAnq0667yUeYEYlzvsHBlq7G+rfwjBDuTcKpBaFAwO93u12YEHn4+wGcY2lRjqNktPu/fpFGMfx+lwWAK38Hvabj740gxgy1Vpu++r23P6r84rOtP7r3gRu+xjNjifFInFjy6ov/rly8qObqh5/8Bbh8CBNkE945OX7tfXHoM4rh9xPDjxL8/pVcEN9mcF0L8HcYkESZINKQ398qKZXdKrV6yl4pG4U/QLx7la54XpNCpXKA4w89D++O4N3K+NfLcTw5SvD7R/CjkGbkFpcxgw4HK50Wl6O359GfXv5IX2dHW+ICtU6XM4zYY5gQEcbpuKYdxOvtbmqo5+sHurs62rZXV3tdA331G9dvZCA00NPdyZCIr+cj/rz84cQO6BR+7LorH92ydMly/u7q7TkLH5k4JsXhzmgZtdLstahZR2Ra2sd1d+L4P2YOJrvdL7RUo0l/2Z8fSmfiH34hE7lv0NUPYtU3bNywKau0vMTv8bSr1BrJlJSsB3uXlGq1TmMwREHMypTsHCVzbRBvg8melIwJsCE1Lz8Hv6lVGo1GqzcYh0SxQqn44R/uT8WMSOPv5qTk+fjgCSfhve9B2lyIv/tHMN6RYRBqFMP/D5aTSDbxLY1bP1jhfAjH8Tg24LiDZNMfK5rH4WjA8RKOAI5TcLwbhxg+ELJuOM5OlHAoFOwHJwdRJ7fXbt+UXVo2EZPBo1Sp7SBe7UgrCqWXIZGtuapqrTUtLRuwR61Ua9TmpKSUPV3P0oGVX9yjxd9/xiT5efwnbu8JODbheDlO6I+y/oGjHceNOBbHrT+nx8+9gMP7vw5p/tc4PBNzHo4mNpLguBfHzcN+fxzQIF+hVJ0Y/34siDUbBPMMOO2vEhwbMPtM0NHpva0tx6fk5FJr9bZtT950zcu3vvLWTTqTaYirMwzp7+rs1Fss5oDP225VpacVTZo8O/6zeX8rr9HpBffOnzBxOksDQJ/V5uTkrEGnE1heqTRYrLbh17/7yAMvYRKqT7nqugt9g4NH41o+/RMcTwy77Bi0jW39F8W/Z6Dez2Iicpuvxvm0eJvPRZsXxCc0W4McOFpGOfyhW47GoD2JQeNFnxp8fobPnzDMcLS3ttrTM9mawjZzYnPfx0898f4R555/pC09IzMBR7oaGxrs6RnpIGom1tdw3ST8XQrO68CkYClwIiaLOvEMoJFwZ33dOkCX6Tqj0XqwGtZeU71Ob7bYNTqdRWswGsHJxeIVJhmv3ioBfaaEAgEX2vcp2nEC4I2e22JNTUtDvUwJHWDxKy9+UDptRml22ZgxcSKPdjc1NlqSU1L0ZrMFE+BVSAxDnPBZwv0Cx4OjSuuhU0wJzg5OLYidCZdkW/UVbAlx9zl7337o/ieBk7t24GFJmnjUMRsAFyI7YEkw8ORNP/1TR33t1vips5nY45w3GYR+aoLYubRWb63Eu4JFh0059mASOxdMqCmWlNTM5i2b1/k9bnfiPAh9OhO7UEi0WpY8Z4LYDeD2g6/+/rcP97Y01w43EBVOmlyjM5kDNEyzfufhvzzesrVqvdwviu8zscf7kKEY2/+PHSaxpFEO/58pPFHvAoe6GAPUCu7VD2I8AQPt+t0ZJz7408eeOgdEMiZuygvjOsaoWSAG1V7Mhay8NoEI0vGsfVo9mNgZPoNIvi2mERPvVCjU+zJpxi09LWhSGtqi38e1LbicdQ1TAqb99fJLHznpimvmlM88fAak2zpM+F70zyS8d1mc69f8t3L4/1aC/y2OX+9JiQNBPBsOBs6E6E8bNcJ9nVkVjQQD/ldA+IcbzJbCXX/H+eWYHLNHIc3BL7zgUoTDBvx8AZ9Y/PIL7y564dm3hzRwYFsMxhXDiF2Y3wJejxvcf4Dt4e2127cD/gSaqio3Mw4H1q0D3On+XyVgtiC1bd9WzX2Bz23g2F723eE2QzKGvIOugeF9xQtbgEnnDyd26BDVj19/1aPM/dG/h+MUm0QtX0cxPxSgwX9D4WnMDlo1EK2VKq02i0+OnXuEZ9y8I127XsyEzWK9u7mxkQcbCp23v7uzWVIC06jVTgH+7UkBxvI4BqCo+f93WXYsBljTzSu8Bos1BHqW0N5BSaEMDvT0dLudjg5Pf5+jbt2aDbx6zMwhvho8VMxJKcqzf/F/DejTWFwBZhNmDy5bHzeBjmL4A1nAWRaBsxy1r+sGnc5etkp0NtQxR1On5eWngcO5jVZbAY2WPU4HpgFIP0eMYm5AQh0YxNbCiZOnRkLBoMFqs49AX8DkUbAj3fZRSHNgSjY4DztN0bO/uvnvNWtWrd31Aojlfr/HM9jf1VHf09JUn1NeMSGzuDQX0CZllNj3zvD4P73ZnAwIk2+y2ZLKZ8yaAQ4ebK+r2Rzwej1e18Buq7fsLvH8Hbc9NdDT3QViN3fU1U7+b2nwobzwxN6IJwZ83snAIcLufOaNtwQgjjXDlFT+Lrl6eloVSoUqt2LcDIjZCHMlQBfzKD3vJ76Vza5qvdmiL5064whIzE5Ha0tTzpiKyUA74YSrA37XLbjmeh8YihgLa2oqB66w3vQZjqpRSLP/JQui8iNwj/F7uwjK1Jb+7m7H2Dnz5o2S68Er0IVqe1taWtDPR+9DXfCA2ZxBcjDMIQlpDkkOD65+M7jJ+P6uzg5AFU9GUfFOQQ9sbwfWrM0fN6EwvbA4d5QkD25JzsrJSsrMTm7avGkzuLpu1/FwdrS3RMPhSEpuXgF0picgXdk1ITRqpRlhcba35fNnR13N+k2LPnlzV7wOjOlD5yqUarUFyuwodDnIBX1sABHb7ZlZSuD9GLs2c3RW4nfoVV9sXvL5B3GYaQXXjYxCmpGX8u6mhofS8guPh7Lqw3e3QqlMTfxYv2Hd0qTMrExbekbRKCl++yUUCLi3Lv9yCTD+bBC/cKnAOLkAaMLQAZJcjt42S3IKuypvZP32UIM0hxrBP4vj4j39wB6DjraW9oIJk8azmVgaFhM6Wr7VEmN3ja7Ghu0YB21mcUnJV+B5Dklk1+PGQ4ngDyVIcw0TOzsuMWyJ+7YMFbVGw0EPvEAkjRL7fxYVsD+PzmSSdEZjcNcfWb+C3uXGEE0EvHlgFMN/RXG0tx3Fn9tXr1j1j59f98hwjNiytaqqv7urIzk7Z/oovR0axZaWPlZrMNg3fPrxF5xhIXH+3b89+M93H3ngeQF1opEj8KE7lOp9yFhpqlcsq579vXOoZOqMitJpM9XA7cLGy74gqbn5doVKqRsls0OraPQG25iZs3PY1z4W03BgunTmjbcck/i9s76u7u6zFoQPJdh8qHD4sROOnD9OzEC12qZUqaZx5zGsqVmzclXA5w1qdPqkURI7tArGSg8uX9i0uXJ9KOAX4YEqjWYMH/x3UmaWBcR++qiVZufCUOYjZhi7WQT8frbSeBQqlXl/YkNHy7epwcZi4UCAwwF1HKD+FTEH7EN/76jSSsJ1l33bNcDpWxgPDuvIaOPmTRugAAVGif1Q1mAl9r7Ub1u5bIXPPTjkudqybcvWlW+/IbKuAfJw7MIhsUD4Hyd4r8slIo8joXA9ydkDEh2pKJ48ZZLRZhsN5DjUoY1Gox8394hZBrNlKMg8Gg43hoIBkfFBUiiN21etSP6uQxoO6Ih8+drLj849+wdXjpLN/26pXrl81f2XnncSaM2ZGPfvEqRhR6/PuR8Y25VNnxnb1eY+Wv63CpRXH4idrTfsTcm5cq75rnD4cSDutZwTfZQMvvPlgCiyhxqHZ8sLZ9Nld9GboaD+HxM7LyQtefXF90bH/LtVatauWrf+k4+EYSLg8zKM/RHJOfX/8W0ptQd14SkWjT4nKRTnxr8eo42nSlSq1T3J2bkr8OfJ9D+Q62S0jKzoDMYWhaTowJ9HavWGwjihJ2hlPmiFA8QP6m4nBxPSTOVtX9jVaPlb/144/oijpliSU0YtLqNlOEOMbln25YqMwqKc5OycPO+g6zqD2fLX/yZIw37sHNH+Wcjvv5dttEG/z9dWvfVpfHeODvFoGV5A72FHa8v7vsHBViH5lcqr8fF+/Dj5UOfwqZixiyGWxux1VnPQaSgUBKzZZwat0fK/VzgnEGdLGbaxQ2xPsDYem3xanPgPPQ4/6HQezcTe393VufT1Vz/0u92Du7UBUxrEHti+euUq/O4eHf7vXqlbt2YtbyiRgDTDiZ2Z4er33/m0ZduWLSB2JTj/DQf6/QeC4DkPY2nQ5xUBvqioz5qatiwai+7kKx2NhCNbli5Zhkb0l06bMVZr0OtHh/+7V4omTS7VGY26zYsXLQXh75rxjZNFrYQEEIlvA14PZ3bmAJP0QwXSFGBS/gtEPm0v4inWWLlxc1JWdioao8O1+lHfmO+4sgpZHwr4u8HhdW3bqxuzy8pLtAajaQ/oV+yFhY8+fF4b1w//Y5BGGd+PdBrnKIyvlu5E7Jyoh5P2pOTkGrR6vT7u+jtK7N/xwkSs0ekz1FqdKb2g0MipEB1trc27rLiLyDaGPfiwRyORh3Fu3H8M0kDcsH/6DP77xd/e/vSyf7/64a7X+D2eTj5M9qRizGDr6FCPlp2ID8qr0WYvBUFH3H3OhsSGbonCSV+vHFd0OzNOXGvD9yO+6Tu/9sJT7do1xrxxE9zmpCT9Ob/8v7JYJDqUWcAz0N/XsHH9lrFz5k3ZV37y0TJaQPSZfNRvXL9ObzKZMotLy/i8JSXVBshdBAQhgn866mpVydk53zqG5+XS+yB+ToGYSQdE2SlwgzPRRqMxP5RShVqjTf8WNwwYLf/lJeT394VCQW/Q51PyNjzDg0k4bz1jeZzjjehuwbH+W8HwIPI/4eNKiKHcXYmdi7Ozo9470N8LjJY5SuyjZX+KWqez40PTUbt93fAgfsGZFQoliJ13MzwWv/HOhF8rAdf+EqQZs0zsBvf+Yw//a+nrr34wNDsDAX/dujXrMwqKitILiyaNDt9o+TrFYLakVMyed3JnQ13NcLNlb0tz4wM/vughzhINRlsW8HnPOOgED/ER6OvsEJWYeeoZ7orZc0PDZiBjLjckj2Z02EbLN4HZgvAt1qBGq0usxpLeYtH86I/3N2t0Wj0rt8tef9V30JVW4P287qYGkT4NysMPh6wxbrerp7W5MXfM2NEsvqPlgJTkrOxpoLWtnoF+DzuWGa023vXlPv7N6xpw5o2bEIpPjv3aVXx/ODyLkKq0/MKpu/7A26hbU1I19B3e0ny0HPiiN1u1erNZvRvssViTiidP5SS79x8sSJMaT5um4YUk3iAs8cOgw9Fbv3H9eqPNXkijvu2j5QAWc1JSkau3tzu+KZsovLEa2+fZaoOvNwB1LDjgBN/T3FSq0mjyOe/jg5dddG/d+rWrdsxCsz6nfIxdkkg5OkSj5UAXS0pKUnJ2rj3x3ety9d97wdn3AEI383fAniMPOME/+JOLW7oa6usUSqXyjncWXjHm8DkixyM0ZU9j5cZKjd6QrwCuGR2e0XKgC+BLbk9LUzOQhfCZN1qtSX9YtPza1Nx83nwhuOb9d7ccDA7f5u7vE2JFkqQK9onhv6FF6zKLS+wUi43a20fLQSu2tAyLOSlZ0BwvRoEGx7KfjbvP2QOiX7M/zxrJSivj8pdwnLvL+VhHXc12S3KKEfg9Z3RYRsvBKiBqX09zY0NafmHJroudwPJVkkJx6hUVBQ0HisN/n4mdN/ttqqqs4oiVxETArNPERrn7aDnYJRaTTLYkzXBi5x3U2YACYh8H2rzzgEGa7uZGoRQM9HR3fPyPxx8LeD0ikomT3oeDwYjJnpQ1OiKj5WAWlUajkxSSxtXb05U4V71y+Yct27YI44l30DUBSEVxQAh+06JPt7AJKCU3r/iy+x66w2C2CM816K9SNBpl4/+o7X20HPQS9Pt94PBDKfrmnn3uRROOOPpE/rt129btgDSxA0Lw//rD795a9/EHnwsMo1CkJBzCoCAHoEGn0gG1vcd2+dyTRiEdgHf8r8zR7w6vsaWlsSVQnQgSgfJqAS3qOupqax+6/JKHRtoZ+yR4iApDxey5Gbue97vdfYyhDtjARcOkMNpJnZFPkt7MbpnxNkQZwxH/U9rTSWEwQ1EJf7134DkKSypJRpt4338xphUfSlsGKcx21tx2uSB6ECfDfyYNqM/tdout7nfJdJGal58FGk09YFYa4PQvgKGOYOLm2ZWUmSUsMtFIOCD2t4pvTfONBg9cWwVVoH/Vl+RY9jllHH8K6QoKSGJ9WG2kqM+BS1TUs/hTUuk0lHz4kRQJBvYw0Ht7T5SNuuRZtxqUIpFpwmEU27E10X8XV8eYKzQ6cn75BUkqFVlnHg6ZH+8PpQqMI4ViQQ/F/IPi2gPz2ihJWgMpdFaKenopFg7i2d/eWiM7jKH4QG96Jnre+G6gu7sns7ikFL95wO0nA9bUfFMOrw0G/GJbwq3Lvvxk8xeLhtyBO+vr6jvqamq/cUuAUDRpueRc/gVt/u1NtP3vD1LdEw+SSmsmT0MdtT/3ECmiSgoOhmjLb39Og00tpLIlj4CDxXYcPFgGO4VaW2n5T86nhpeeJTWrItKuvkexEUCskVw/Uti2r2fF9kh4KlsqeevraMU1F5FjxWJS6m3yteA9vODd9fpzNLB8MUka4wghXGwvbZW/S1o9BR291Pb0XynQ2Yn+/HYjNnkz5KbNm6oSYYBdDfVbX77r9ofYeghiN4IWK74xpMGMCfd3dQrNeNbpZy6Ye865ZyZ+yygqLsgoKsn/ptxKCXjhra2m9T+/nLLPuIRKLr6SPG1NpIxKVPfcP2jrs49CS9dSqKeFCs48h7JPP49iaiXuY2gT+erBU2gEB5LUBlbzSWUyUdcXH9GAo4+Sp8ygaEzNSS53HmxJNQz+oF8VagaLkDIG+Vo+p9LJ5/hvvp45aCwsS6nkHMCMFHyPfLWUideLlFo8Vy9fy2oRC8qh+/g6lXxuCELE4Z3GjOboqf2jt0llSabscy4BN/eK3xUWG/ka62n9b24hV101JoJRntTKvfn1yRJWtFWSJar4znXhfeQk7qMI/rRS+ztv0MY//QbSBPBTZ/xWdQi92WLJHz+xIhEFlT9u/KQb/vH8z9U6nQGToX/5G693fmOCB9yJPHXzDQ+1Vm/byrss40gZ4vAN9Q2NlRurvhl3l9DHKqr/x8MgYBMVXfgj6qutIX3JeCKrjga3V1LxBVdTDNxYn5FEBdf+GreEqfFv95Pjy89IabbuLnbVWpxPk2leZ6DYYB9FQxGKDvRTx8fvU+4RR1LOOT+kSF8XzjkEF5R0Ft4IllRmm4AITGyMj2MxiWIghJh7kKKuflyHQQ6HAYV8uE8HfSKeWUKlJQWIuO2Vp6jzrVfxDP3OyrWYKCpw5gwZwmECKsCLI71deIaNK4rngqismTLh6i2YE5gQ4SgweoYgdH4f369QSBRoaaL2D96hnAVnkrm0Ik7wStJajNSzcQ2ZCoso47QLKMYOTnjW0HN4YsWiOzEGfpfCmAT6xt9aE8X6AVdCIGh7loB8kgo1hbTFE8i5ZhnlnnkBmSfNpEh/z7fK4Rm/129YVzlEuEqVAZw9nxN+vfGXe5/+6B+PjcjFYJ/+Ly3btrziG3Sdij93EhmpOXlZUCRD36gVIKzQgJOcG1YCt5+OzpXIB+6eUlpCzU8+TO7WFgrWbibf9s2kzcMAqG00sBRc5vaf0ZR7HpIJVSwLSGIgWaFlrF/3199T78rFZB8/kRyV66nihl+TLrOQ3HXbaPJDz5LKqKcvz1pA6UccS3kXXkmBjmoKgvhqnn6cSq++hdKOPJpq/3oftX74JtkrxtHAlkoqvORqSpozn9Ze/n0qvepnlHTUKbTxugsp44RTKf9Hl1Dfio209pZracyVN1DWaedSeLBX1i1jcWLVW6n5haeo+c3nKXnSVHJBqqUfdzLlnXUJVd1+A0mRAKXMOZYyz76Egh1ttPX3t4CQg5R65HGUdfalJEVjtOVOcO7tmwQciwKz5591Pog6xind0Hca6n3jX9T61ut4VoiCdZWkMc0CDPwZBXo7KG3esZR+8lmkNBnxXFl3USVnkQ8MZssfbhNc3JhdQO6mOpr0h8fExK159D5Km3sklV52HXV9/jL1bd9GaZgzgxuWkrakXEz8hAJ9sIs5OSXJaLPt5irs7nN2LX75BXYTdn9jDh/n8keVTpvxPd6LE4prG+eg4fMQJTaNTp/6TeCMBK4Y8aOe0RBZysaD8DdQ2NFDSROmUPe6daRPSiLLxOngPgEMcBCMVEHejnYMjZq0aZmYbsEdGBOwR6k1YvB+TdVPP0bF515KIbeXBmobyDb2MOrfuh6DbaHkaXMBB96hrjWrKXXOMbT94Xtp24N/BH+UqHPJYoq43VT/5GO06U93Ut6ZF6KDlNRfs53sk2dTy79eooHqbWQZO4Mciz+njs8Wki4pRfSir7WRIiBKfUGRgDyxWGwILqiMdqp77C+0/q5fUM6p54HBG6hj6XKyl5bRul9eB2zsIE1KFm157EEK97TS+luvowikiyY3jzY/+AeKOZ20ETCl7l8vUtF5V2AyuclSMZ5s4yfh7wHZ+qTVkc81QN6WBko9egFp7Sm07parydPeRuaxkwBFfoeJ3Y4+sAqJoUSdAk2NtPSS7+E+LxVfeA21fvwRSUYrubeso6p776CC7/8Iz2uivmWLyNvZC1SnIvv0IygCiSntU385sBNBpVYbQG9DGcjYLZ3d1FNy80pAozeOmMfu6wJHe5vwN+5paWm8++wFd7v7+w9YFuAYY0NTErifCUpYNbnA6RVWO6WcsIAikSilTptF2ZdfS5r0NHC1FBrYuJEG1i+irHlHUDUkAEE6SDp5ABXg9m1vvkz1LzxNsx97ibLO/yH1VW+lssuuJENhOjmWfEaW8vGkBCE1PPd3KsbvlgmHUfsn71LKESeSYco8yj3hRFJ5umjr3+6jKXfdT8WXXymIPe97F5CxuIg6F75OBRdcRuaJuVT/zIOUOnsepZ90CrnWN1Hn269QwSkLqPnV58hdu01WGMF5lbY0Gli7jLY+8iea+Ku7qPym68hZU025p52OyeWiXtSr4s4/Ud4FP6L0ihJqevEp6qtcRxPv/hsVXP5LShs/njrefppaF75LRz73GtlnHwvFvZEyjz2JoiDyWNAnSzcolZI5mQx2G5VeA+XeE6QO4Pxxt99LRT/9FSUV5lPIFyApJodj8kSsvPtWAbWOee9DioEZBAHbis67FFLoVco4FpLr3IvFdbbZxwHaKciKCZp37S/JMGEyngOpEpesEuCggk3J0vDvpvj3gyMBPn76iRfWL/zwY0H8TucxIHrVASH4bcu/FH7HaXn5+X9asuYnJpv9wGwQzB2DvtBlZtC43zxA3uZttOXxByn/7ItpsKYWGP1jyj37IiK/H5jcRp7Wbqq971eUc/aVELPHU8g1GGciUYFNlcDTbW+9Blx7OuWceSytuuYyKHB1VHjhxeQHE+zftBbECdhkMpCnsZqyTzmbetavxa0xKjjpVIjvP1Hq3KPI0dJFltwcKrjkJ7T5zlupv2o9FePvwW2bKAw9IOuEM6n5+TdpYPM6KrroJ8IaWPvwnYAnpwGWnUUBZ2987UA2EepTDNT+2YdkLSkDNLiBah58gHpXL6Xx19xEXdvqyDqmlNJmTaDWD94lKyRRqH+AkidPB/dPp/ZXn6GkmfOor7GJco89nlLnH03rbroYAiRGGUccR6HBQSEdWZeIRZTU+OxjmIzTyF6SQl1vvwhIdxhlzJhMra89T7qMPLKNmwCJAL3FmEJ+wCbnupU0+dbbydfupPU3/5hSpkyn9GNOoBgg2GB9DaDhb6jo4qtIbTVS079foMwjTyKgTuD3bgoOekkYTND3kUCYgn0DnP9atDkCpZbbQdHYAVgo3HM5/fqfL5ix4PS5MjNu6r6iomBENmrlqT/de4LWu89a0JVVUjo+s6SsSJKkrGFpjr9RUZps5AK+7l+1jJJnHkO9mzZQ3/pVVH7OOdT40rP4vpE0NhtZwZX9nZ204fqLqLeykpImTqatTz1CSWUllHnSaeA8ACPhGDmXf06Nr78AUlNTqKGW/E3VFILypUnOI+dnb1PbwndIn1dE4b5u6vj0YwoDszsXvU8BDJy3pY780B2y5x9L21/4J/mdfRTubKFw63byByKkT88m5+LPqGPlcoo6O3C+mmI6MyXPmkf1D95N9W++Tulz5lH1U08ArqgFtlbqdUKfGFizjmqfeYwCHi9JIBTX5rW410RWwJHOD94EV+X391LvF+9Q+onnUtcXC8nf30fBATf1fv4epc+dT82ffIbvfeSDxIr0dVLADwhYXEKG3ELBTUO4tvW5v1LL+28IaKM0ZVAvlHp/bw+FcW3r6/+koituJlPZGCERwm4ftTz/OHWvWklKCTBx2zowlGayjJ8i4Jd73TJqwQTkvk2ddwK1Pg9Mv+QLMBUD2Q+bRV2QHGuv+zFlzD6CDGOmUD1g4YZfX0f5J36PjOXlVPPne2jz724F8/keqZPT8M7AASd4hVKZhMPcWV9X+8o9v7nP2d62+YAQPH533nnq8Y2l02aOS8nJzTlQYEZptpOvqYk2/+Ymqn3qr+Srq6bJd95LimgACmox5YAb9y//jGxjJoAYm8mYl032SVOof81SyjhyPhnBdfQ5BaRKzwUR9JJz4b8pacosUkDF0GbmUSkUVQnKWf/SD6EHTKHcM86nwVWfU9TlpIz5JwDfD1AhuJc5OxM4fzuNvf5X5KneRNrUFNLZLALvl914B0RgFM/4mKwTJpF93CQyV0zANWnkXLGUYsDbthnzBPTqW76I0ubMITO6SA9cr8rIpZjXS70fvkamohIyFhWTubCYIjg3sG0LY0VKm38SJlMqDUAJrLjtXop2NoATp5MpLxcwaDmVXXULBdrqyJRfgHpAP8jMpjE330WBzla0w0HWyTM5rwX5KleTF9Ks5KpfkCIWIgcmeCZgobmsnByYNEVX/BzS4SQKOdpEv3ur1kBZ3SIgGUF6JlVUkLOqihQBNwXrq8g+9xgq/cm1UEoHqX/Je2QDbi+ENBusXEF6WxIp7anCcpU2/3hSGG3kb6mB/qSh1KNOwERHfZrrQJAaSpl3DCnU0GciB2dV29Pf57zztONv625s4PjWERlQ9rXSysbWq3AcHvL750FRTT2gs1Sto4Gtm8m1rYqSp84iY9k4EISbVGaTsODEXC4oSB5ht9akQkkNBCgCMa62WYU4j4S8cTN4VJjxNMD/fE0MylWUXRWA1yNQ8JTAtcwJI1AOYwpJPD8GsUs6LcX8QUFM4VhEPFubkir/hufyORU/A9xZmWQjjclIXZ98RCtuvIIm//oeSjvmdAywQiCYSH8/qVCvsM9PEZ9LrheL9BjXPYMUUPTqn/4rbX74Pprx58dBDCeAG0vCEhMLhkgy6CgIjqxNSxdmwagX2BxSIgKlVJMMxTgQFDg6gnYpUbmw14Nah+Sw/UgMbbKD+NAunA8NDICzysgz4kL7zWYKu3rjSjSjoCg4OdoDdOquqqTl11xKBkz8Kfc9g0sCpE6yor90FMVzInieMikJddVikg3ivS7B6ZWQUmHfAEX9HlIZbKTSGinoceDZASEllCoNBd19HIR64FZ7d3VyiIRDkkL5MZAHr7A+AFjT9I0IPhqJPAexcdFBWisWeE9psLJ4Ev4xYc+gPIBxWzHb1ImXsNkmIPChgiSVWhCExPewwSgqL9yI85EIzvNniLf2FvZxtqsLM5ywZOjFYlUMCrHElpSwT9jE2VoUC7HNWSvew8/m5/LyOdeBv0tqPXm2bqKPTz+O0qdOo3mvfkxRKCHhgZ74ApdGWI3Ec7lO8YGWVEpSWtKp9ZmH6fNf3EgTL7iIJj/wFJrlBTH3i3UIfn4UUINNi9yGofbwOW5PVG4X+/9wm0W/cJ9w36Bdoo3cc9wf3B61Wnab4HUOrpdofzy5M1/P7gfg7NG+QfrighPIWV1DJ/77IzJPn4cJ0om6yP3K5lRhcYrfL2n0ol/ZAsX1Fu8XOEHJTibyeLCSiufL/Rv8Nl0PqgG354Po278upJmMB/yNzZGr3n3rU6PVptWbzJYDVsO4Bs+dGQ1AAYoT5U5aPRPN8HP8Kc5FdziXJZQi/h6TndAS/jniumHiVIhWcV3ifklWtIb+HvabWEmVfxcTCL+HHN1ktAPqXH8riNhEkYHeISIaqmuiHsPqJSaZ30vpY8up8LLrQUQSRZn7xa0cssiP1yWxypuodzS6+3OH6ryjH4fqy5/h0JCz3I72Sztfj/uZG6vBfMsu+hHZoByHB7pApMOem5i4ifuHj8fwVW7xPbrb8w92CQeD/hVvv/GJOSnJqDebC/weT+DDJ/722dey0qxf+KFZHq9ouLet5cWg3//tLq0dUgXcNOAhPRTF8lvugu6QB4jgHKEFQhJOXNbps6j4p7eRymqhCBP7f3h7K2Y0klqigsuuo5RjT4Eiy/Aj8l81KkA04e6mhn8GA37mPNRRV5PydSHNEb7BwQs1ev2lSpVK/d0k8j1sZsIQBXBD5vhE+xUKwJODiTwapkMnfU8s7hcU2wF5/ovLoMOx0pyczNtevsZpIUdK8Dfh+PM360e5IyWNZi8LD3Hxy5BBiO3IV3c40wpvHCJ9VWa1+HmBzyNfc/BiceysiS/MKIfgBddvSF/YE6HH79tthTEB20Kh3dvHuoFCkk120eiwtinkftvToo0kyfUYxomF7w/7U8Xhjvg9HP5q6QP+JXSUPT2bJ3I4MrK2keyXE2Mv8V2HRB3vw5EuOsUZgeinAzGNY7GPeAdAEH1wX740JVBUf8dK5PZVK1an5uXn2DMyM+MacTjo8/u0RqNxn9tNirFCY4N7c02Nik5RCuctLT8fSlyfTFQ7PT4WV8j2RsRRoTQptSaSTBqIuqCMkWOxEUCHqCBupTmZOJgr7HJRyO+mqE92zeAVQ5XOSGr8HhWWnL448Sp21I/fE4ruIdFhHO8ykbETWsJnXa0jKSoJvMw+5mKiBn1DrWUDm0Ry0MueFH1JbwDEcgvC56pIUMKFIslKmQG6hcLPMXHx7pJ2rk+Ej+juo8LEC6Ln9oq6iEkTtzSF9rSmExnykZd1MU/8XTxWsmfnyNdYw7Kric4g9I8Y5wnYx7iF/H5f3MVFH8fzgfqN6zfljKkoNZgtJ4BcL8HpJ/dK8LVrV+eUTJ1uANGHl7/52pNHnnfRBQmC5zTFTVWVG0unzpjFQbV745QKSzJ5KjdQwxN/oRg4kJLrlJjtUkKBjJICnExrs5OhZAwlT5tD2rxCCg86ZcuMaHBMWEfCvd3U8PeHKNDfRyrevn6XZzGHZKuAxp5M5jHjKXnWkaTOzJItKNG9TBSmFhCf2pxC/qZa6l36KTnXriJfT7dQ6JiElKijPiWN7FOmU+rc40iXX0yhQQd63CcHYugs5KlaT43/fDJuRVINUX0kEODYX8r73vfJMGEGCNUkFL8oFNjt999DQaeDyq6+Ee0uIYXBituiFOnuovpHfkMBNr+yo1d0B+dmDph17AlkmT6PVCnpYiWz69//pM4vPiW1xULBvj7hcpB57o8prEL92Isy0ZfMcVUG6nj579S94kvSJSXLklW2xqFOPsrmZ888klTJeDYmDVtzvDXbqPGZx9CNGFeNeqjvQx4PmUAa2aefTZqCsSSx12fYD8KPUdPj91N/bQ3pku1inL8a/cksgk3MtvIKyjr9+6RMyyXSqZjg9kr0nY312zU6vS69oLBc1CcQ8K5+/+2n7ekZ14PgbcD2k/co4IZ/ef/xhxsvuOPuZs7Wesk99z0MrqGOE7sbnRIYM3P2bHT+PjG9UqujUJ+DGhcupBg4joFtwswF0MGRYDAuQaW4gxX6FoRnSE2m0kuvofTTfgCa8PEUlq0NCtnE1rZsKXk6u8iQnp4QW/KzhiwyMoeT3v43mbOyqPSyayn1xDPjylhod/GKdypMdrxDQS3/fIwaX32G3N29wh1Y8NloPCQQREw1tdS2dDEZX3qGCs65mLLOAfNgPxZPnzADsvNX0+eLBCfXWixDQ+nt6yeNXkdph02gcHcHaYvHkH78bBr48nNq/Oh90GKE7GWlwolMkVtGpsNmUjDYSq1LPidPn5tMGWmymRTtCwy6RZSXrbyMIj3tpM4rJ/uRp5CroZ7qFn4i3CHCwRD1bt2CSaYg+7TDKarRk8piHjIPKpVq6qvaTPUffUK24oIhKgx5vILo7BUVFP30LVJYU8g6ez5pQPhhV79oWwxSUAuFW4wjOLu7o4vSD5tEyWXFNLB2JVlmHEW64hLBrdkxr23NOrIV5cnuBTxWmKwJk2ti/FiKqdjpDf3EjMKy/FMKg7ysc4+Hcm+OW9z2TGPZpeVjg35/P+84Y7TZk3Umk+382++6H32lY/v8+o8/XL8vgp947ePP/BqEnVhc0khxHBj0+1z9XZ1NJntSxogwlODeWrLmZFAUHFotiCAqiEjBNmBwTfbrDgeCYhmf7bjssbfxj3fSZBCi/YjjwfE46CIqu76Ce5sz0yDNFeDGFgFfonFCUGq1AhpFxLMcQiy7Ortp/R0300RwrbSTzqQo4/FIII7B5YANhdFKUlii7ff9H9W/+Ro4lEnYtw1JdjLn5ZPebhN6iM/RQ66mJgpgUHyDHqp88A80WLOFxtxyNyRZigyHtBqyZmcKTsmRQQq1WsxB5tA6SDC1DRKvsY58na1kHDuVetavZlFMGqueXK2tZML7vJ+8QYay8cLn3pSRLvyHdCBWwX0BL9RGvWAk+ux8inkHqX/FJ2QsKidDRibZ0M+6lGRhu/f1Oqnq8YdpAtprn3eCkGACCqFfo/jU2e3iemZCsbhJVWtEX6enkT4jS/SRa+XnIPZU0hyVL2z6lmyMY0waktQ8CVVqFQi6iLTZeeT6chENLPuQtLlXiMAWI5iXLTtN9GWCcXCHsLQcrsBH2dsVEo9XYw2paaSChO5bsYQUKiXZT/ievAYRDn2Va4Ha7XQ4/V6vmwlekolVJ/+mUh9/2ZXXnXzVtV3A8W/vyQ5vAub5DA+ZCxy7Ewfn2EFU2p+Wlz/CLQMlDBBEuV4vnI3MebkQoUvBRXxkzsmhigsvBlcDdyovp5SxY8mIjvaw2yo6NgJMG/EOkEEDjgbRri0cgwEIiBjUtOlHUvL4SdS9cgk4sYO0IKYx559P6dNmiOclgzuZc3LJ19UpTwB0tBuEabMbxHK+Oi07rsxGhY+ISmuhuod/TzWvvEB6Rm2QFhkzZlLJOd8nW0EBmfNxFBaJ+iWPmyRWgL14ti41nbrXriFlwIVJmA5CwCS0J1H6jHmUPucI6t2wWkzikNdPpWedRfmnnEoBRy8FejuF56QxNYVqX3gW0t9PGhBawDVIKZMOo4jPIya+PiefkqfOAVFmUteq5RB0AdCshsZcAMkyb57A7YE+J3QAEG9ukfB/yZl/EoX7e8mxeRNpQWT+ARd5unsoaUw5RTlegGNReTEqEiQj7slfcAZ525vJuaVKwK6Ck0+hsh+cLyRbgFO/gEANpRWANqkgPgXadRxZIFl7wMn9qK8xNRXc8UYxnlwXdqrjlV5jyVhMMCJLcQXlHnMCDdRUkbu1TTyv5OzvU+78Y9G3hWQtLqa0yVMpfepU6l63lgYaW8ian092nA8DKqktmOw89graYd/fQzFYrSmYrGqfy+XVGhjr7jQh0vBxHuj7i3cfeaBxJzs8bx+iVKlKOHrkyZt++lgicaXg7j5w9+7O1v1SkyF6eTnbOP1w0heNwSwNy1xBrxccmTsoBOUwCqiSDiIrPuNMAU9UvLyOicEMwbNlnQj8AHWSAhxUf9g0cMBJYqDDjI0hArXAsREQTgjPY1ycPH4CVVx0icDNKnDQwKALWLKWfFvXUaBpOwZedtlV2TOp94uPqO6V58gAYmeRno9BL8VkjOJ5/q52Gti8kVxVm0TdjZi0E3/9J8o64hiBk00Y6MYP3ifH5x+Rf8OX4Csm0k0+nOzHnEK2MeNAvF7Bmf1Op7g/0Nsl9Asj7utZvpi8bS2iLyRwSV9vLw1CguhzC8i3bZPguqaZ0zH/ooAZLuGqYM4vFP44AegWAdSNFVJWTnn0lJl5ZALSVAGTc38ybOBJOlBfT1seuo9izi7h68/heopokLQFJWSeievNVtF3zIG5bnyvp7Fe3K9i6SpWToMYRwsZp8wkXV6pWCCLApqoMVFVGN8AYFoIE0QFJVpl0Mvwi6VI+UQyTZkjXEc4joFXm1V6oxijkNslxpCNAhx2qUtOJr3VRFpIHoFeWEpCed2jJWmP/jT9Lq9roHOYhSb28T8ef71u/VoBadx9zh/vBmm2LVuqm3TMcZzhSXXqT2/coNHpFsShjFeNcznlFTP2b51GNjNF3QMU6XfEQ/kk0Vk8iIIAoAiawRW1ueVksWWS4uUXQfQsvkPC448xnWfzGtLml8h4H2I8Cu4vpCO7FXDfuOVn8UnT7GNIlZ5NFgxW8vLlwPxLBBQKgtNawLH7ly2idE4BAnwqDQ5Q82v/pCiEGU+e1PETKf+kU8i5fg1FUD8W68aiUjKC06qT0vDeQShT2TTmpjvI3VBD7p4e8U5HbT0U2hRyr/yMzIcfSeFYKtnHTKDWTz7mvqT+6mpInrHC5cAy70QyFI6jxlsvB+xSiHeE3ZjkOi31VG6ilAkThN4ThILOBORYs2TIyGSBtOF+DOA3tclM9qNOFm1lPScGBTrKXgds0ouvEwTdg4AuVnI2NFHzwo+pOCmVIn2AlgxZwl4KO2NCSRUuBoBBoX4nBc2QhB5eIJtLhvJJqF5QVniZyH1stRqIJ0xQCFwewsQLA/erMCGsRy8Acavk2N4ofg/KE5VQp4Tvfaivl6SgHgzDQfY5x5IGkozrO/bnt1OgtlIE3nhamnGfd0jnGMn2HkmZWcUBr6ebt0o1Wm12ppUJR81v1RqMufy7o61Vu9tK699++pPPOG8kBkmfUVT8WCI5aiQUDjo72ps5L/z+G0Ml+fEJJQWP4NQSmuQ0MbPV9hTSj5sO5cRGXQs/EBg8iEbbSksFfg+D6wq8CK4ga+vSMBMPiQkkLEIq2XKgSUonTWoWRQ1W0mZmyLGnbBmF0sVSJQpI4qmtEvrEYN02EGMVYBc4E7hP7vz55GlqFJyZX2GsOIxsx51FmsxcEXbI0irqcZImI4PyzzoP1/mERaYfyiwHM/ubaynU0Sakh3XKdEyCVGFx8YJ7hz3MySwUA2Tx93TinmoBXdir0lJYwCvZNNjYICtuHK+K5/jbOmiwdruII2UcbykqlvWTUBhKqJ00BYAbFiv0EHOcIBRx6ogJwrcVFpIaOhRzzeZPP6X6558ihd8lK+SaeGDGcDs9W5fUahlnG8ykTE4XnF2YJkV/K4beEYubTpUcLyxiYjGRIGmVSSmyD49wgVDIC1qJd6BeKugyLJXUQB7qbNQvqwSTBToO9BDztKMEV2fLmAbXsVlcnsAjW0sZ6Ol2+AYHE54AUmZx6fWYCDM4o8GiF559azeCf3xr4/zssjEFuzjjRHl2grtP3dP2lPu9OK9kDz83tH5g9Kw8ITb7F39KVb/6KTW8/S8KBUICi2cffRz521vE78IWvYeGS0PrTLJEkMVfSHAkBc4FIfqFyRDn+WDlh69hDioBErqbaigcD1MzZGaBMGzkg2hmgtdl5ZB59rEgDKVwCWDuLEyC7HsS9pP1sOlkTLIxM4C0GiBvn2wq9WxcBVjggRJXDIlSiLYGhOmOCVkNyBBqq6Pez94AfMTzACWy5p+IyXOB8IYMAqcL/38m4EFAnKpVmPwevC9ChrR0AbvC7D2JKaDJyBE8RKwTsIPXLh0TGBgA9JpPpcD8AUA9tdlETUuWilgCydlGEcAQSaEZtkgTFZyencEUwtEMHNY3GF8P2QvB4R6GQ2IoUJfYjhy7uy2+cTv4rBZSnb1BIx2NFO5owjMAgyA5JNTRCqmVeuoFlHLaRWSacRSHuH+lwrprScsvrLCkpKTyUtFOVVSrtef9+rdzh0dD8R+nsYFeJWZnNDIU4IGKdjXUbU/Oziky2ZNSvgmxc/M14Kys9K39y58Fp2dTYcDZI+y7LKYLjzuaSi+9jPo3Am87eoRJUmVNJiUHWIX9OxlyhTmTnfJAIAqtSzYxW1NJl2Gm3vc/o+7VK0UWhGjAR8as7KGBUDKsGvRQABg6Yc3UJSXJk4q5GT71ZRMgYXTCb35X43EkCPyqN4s4VnefSzaNej0CzwadvQLCaTILKXXGLOpcsVwoXAONjcKawdyrd81q8Uz2gWfsyjoLW2hc7R3krNxI2UccQSopRi0rlvDaEKnVSrJDsQ8P9ImJwZ6Pqow8meF+Bb5lJuVpb6WSSy+nYkiOmldfEebEqsc4JLKXMk79AdpnjK+Eyo5pvOAmiRWsxLLFvv182LuVYQwHkzPjUexhcoglw7h1pvaN1zHOJqEH+EEHuYDQmSefTcp0SFFlVBA4uyGLJTd2UgsH9+zeseeV1VhnfV0dCL8swZzjDFsBaHNljImM6GrB4QcdDuH+u2XpkuUv33XHs4ncfbz4BK5f9k2Jfeel62h8AcUvVlbZxKgE8TOf8HV3UdeSL4Tyo0D12CxmnDRzxwLRbq7LIQEnbBXjwQGzQcTN1PLsi1T5p9tZ0AplUQvOyhCJA78TLscxEYkpiYGQ4hNBuOjiizY1ndTJGcKn/quWSaSd4IAMsll6sH96sBMKpUpBFtRbY9RjDkWof+tmUtqAocHNXM2N4p6kMWNJQh9EALfSoLSz3dzrcNIAlFd1ThG56uqFxYrNgLayMeTjCCzG5akZAgrujfOx3hJF/V0bV1PWrMMpffJhQjqw1azmnffIw/YHwDOhTLJLgkIh4APb+GNxPYD25hYQ2+F5OixN017ckSRhIXK1tFDH6jXUuX4TdVVVi2gsT+VKcn7wKoVFnAIkasArnPRoiFFLIyQtScoqLS9XKHdshL38zdc//PS5p96M66Kng8sLLK8CqCczuE1uxThT3thxmrg9kwYdvb3Ozo7WokmTZ3xjWueXQtQypyu/8GI56AEdFgAcGGxrp44VK6hj/QbqWLuOik4+kXIXnEHq4snAhxCx3v6dnToZr0ME8qStfADSgpfZQfy85O919gu8LBY6oBOUnHmWeFewr1cs42sYa1qA21PTBCvjZzAEUGpUshUJ+oRYUIqE9tjZbFUJQXwH+mVPSV4PULN1Iu6Wy5yYCcmQlUu2klK0aSPeDYUQdWHTK5srVax0lo8Rlip+F5vpeLGKTYkD0AmSZh5FPhCDErqFMQtwC20dFIp5lNSZeeDOepkovpLbydYwXkgKNjfTuOtuonV3/1ZwfW7jhjtvo1n3PoC+iOfISbhGKOV4AgWvFEv7chuJMw2+ngO2cQ9bXmJ78JeTF52ClII2a3l1lw0XYA62iVOEudRd+SVp0zPIDEU2Fg7T18120NPc2BrweN0FEyeJ7ZjGH3m0UVRUEHwgdt3UcUIhUb189x3PX/Hgo3PsGZkT8H1C4gFQXC0Gq7XogPnlgbuzRs8WGA8reHJIEGXOnEX28ROo6u+PY6Ad1PzZIkqdfjjQKhTMrHwoZ0Y5/8nONlbByZwN9cKcyXNUC2LVg0NLkhycUHbu+WQGMfWtXw1xHRM4nZWlKCuluYWCq0bwHA/gjc81KAiXiUShVH2lf5pSbSBXdSWUUQepONcMYJMezMLb2SHqw+bTiNcFSZEKwp1DXWtXizyPLW+8SGGfnLzJDMXXUpAvlNIw+sE6ZQYmRwk43wYQZTM1PPOwWLEl8oK7l8ku7VB8WRpq0rKGJVPaO/fjhT+2YLGvy6Tb76Vll5+HCasgP9pY/dxTaK9B6B6JgBc5yEUjgsLl4JWvzjggkezTI8iJzZeJLGx7co1mxgIoWHDGmWQtLBbp+hgO8aenoUaYlhVGMCmNgcifCMzff6/N1Nz8HCDyIUKxJKeIfQ14A49Fzz/zuuz8g35p2LTho1DA372br0J9be2g09n/zZD7MKHHpkSI2mB3p8C7zGEts48hZXEF2aYfRbbcXLGiyNcN1laTe8NScq9bis5Uxi09O54lL1OHKPvwWVR65hlUdNJJpLOaAfV94L79VPy9syht2gxyba2UcabfA3h0OPQBO4XBcS2l48hSXCy4fxjKcuuiRWTILxGiN+rzCuLdiepjIZGbkpXhltdfwuDoBCRjuCSxGwUUcba8KNlXBvArHI6SrXyCCAnkNBjO6moabG0TNJQyeapQ9njXRU6ipC+dSKmzjxQwJTjoph5WLlnZVmvJmptHIbesOLN0kgOifV+NMxJKIurGHJe5r6+liaxTp9KEn/1KSBs19J6edWupa/Uq6FVm2dLFK99KheD0su/LLhheSuTopHhqDmaVSjkain2Owru4PEs76xgsAdiE6eJwzpqt0OW6KIJ7OYePBfBOAykd6emNp/b7ei7K4WAw2rptay1D8Z3OBwJ+W3rGUE5UBbDNkwD7k3bTfAsKs0x2+9fD7zGZa3AgNJuiYgnPPw53Y4VJpMHTCYuGvnws+ftdAt8y1xEcB3qHJiWTvNWbIJbr5Ky1ICaF3ipMd0wYvPBUcfGllHf0MVQCbl50+pkUArEzLm1Z+JGwUGgsVnAxE6Us+AHpysYK60PMO0AKEE7u6eeCuAfFIkrnyhXUtXEj2Q+bKriXxItC7CbMopqzANiySQrGqPbB35GjehupdVrBQTOPOApctEdMHJXRKPJKctsYXpnQLnNxqVihVcc5qYbXCKCEMn6NuFykttoBr7LJPuNI0oP4QsDaDG/C/gAZM9NJm5wkoqyYgypsyaRgyBUPeRwidJHyzyCbZ+PRUVw3jjvlVVQ2FbJbROYZF1P5xZeRD1KFIQ8TuRzqqBCKJ0M+dvcQdheFtCOySUxko0hbKCO3qGAi4pIIr4zLTnTDJQLn5FGhbZymb8jHSWRHk5Vc67zjKP2iq8g+9xSqf+8dWnrDldTw8J0Qan5SmJJGvOi004KSVqvLLivP450mh5/XmUzWuWef+yToXLjFKMANzuY/3nn4gZerlnyxLKH1Att3Br2+r7WfPc/8aAhiq6tDLGoIb2I2IwbQIChiBsxsDXThQFsjdb/2KlXdcwsF3F5ZowdXMhcUYNDlsD85OkgSCzJBZ7cYEPa14I7nNBwxEPjA5g2UMnMe5Rx3gsCHffUNVPPKi8IObTpsBmlKJgisL3MPxtrdlHHKuZR9zInkbm4Slpq6f/+Ltv/zGQq0NFDUC4VYpcdYs7sqJM76VVR525XU/MmH4NoW8nb3UPFpZwgbuR9tZIaoSUoVtmuhUAa9pARGtpWPFzZ7Hjp2JbBXjCU16hF0yCZjA69CgygMufmAcTOg5/QLaRb0eMiSly9s1yFIHGYa2oycIe/Gnbi520XBjg6K8EKSSvaLZ6VVDaXfVFQsUvTFIOHCPgcVXvkLyjvpNBqEAimISiEr4GzfZ+nIz+c1i6EJxcEumMwBHkdXn9BVeIKERUA5xqmkjPSoJwfG7zBjSsLNIdDVJocLQnKwWZL1JW1GFukysoVp2t+Ba9qbaKC2jvqaACuhtLtXLaTBVYvRhLiP//7gebS7t7WlKxSQLQ6ci/KBH1/00KDD0YNJkOMddIlN+VSoiJDgM089vUepUmcm7jcnpejVWq3260AZ9hkZXPklbb7nVjY2Qcm0UBQV8gP7bgVWFwEO4A5MnD5o6GKBCUpTsL9PcG0eDF62Fq7AKh1gyABV3X4debq6RXCBOTNDOOFt+st9wjMy56ijSJmcQ+U3/pYGtm8ld2cXuRoaaNvLL7LfNOUdeyII/BySLDY5UNrvphgkRsXNvxNwpHPFMtKnZ1LHqpXCv4RXNo1ZOaK72Q1goLFOnpCAGax4Fp9+hnCJGKjaOOSerCseD2kgP5vZPOPWJChmzR++IwdhY9KnTpyEueAWq81sotPmlwq/dslsp7R5x1PbJwtB4H7oByoyA+LFQPhi0Qa6gjozl+2iQ8TIkk6hMVH7iw9TwztvkBqTT5eWJuB9ByQWw5bc+ceQEZOKYmrZTm7QUcWt95C3o4UcW7cJXxgpHkQi+lqshofjASmMMCw0uGk1bbnvtwKmsbRkbs95czbefy/ZS4up6Jzzcd68YzUdr6m59xYBzbQ2i3BD5uc1ffi+mOzCBfr1fw15yrL1yFaYRwboNszwHJ+9KxLXWg6fD5qQdopJ3vvivkJhTk4xJHao0ej0ykvuvnerWqcTvmEtW6pE5l3Vps8WLp5ywsnHAdZcO9zMAzGqDrN5wWjcfwbPy9UYrJ6qbRBRVjKmJYsGBiFCXe2dce852cbKNlomcL1JT2N+cD7Zx46jgcp1MqfEeU0KCA9c0rm9hjw9DgxS9tCM9nT3kn9gUGTlcn78EtnmLKAJN99JK2+8TAyKH7DB5xygzCnTaHDxO0TWNDJNmQ1uz+kvnCLP/GH3PUl1j/yRWt5/S+gQvr4wubvZXr46js5igusp1UqR67Lgoksoedx46tu4RnZ3xqQ1T5hG2sKSOL6OrzCHcP6w6SJHTdfGTZQ5fZpYQPK1NrH4JS0vvqVlCNfnYLSfkqYeTpaSUmpZspRSx44RUs7b2S7wMddTacR4Rf27MHilMOf2VNeQrSBPVrjx+qDHS96eHkqeOJFMHc3CqmSYNBMQxEEaWzpN+s39tPLaS0QaQZHVgMdBpZQtLwpph0FQqRT+No7qWsFo9HbrkLfkQEu7uDcIKe1tbYWONIu0PDb+IJhNE/Vur6fk8gLZW5IZR69TLBIKM27CPRjvYi9QPxgaO9Ex3FWzj49nQISPsG1/pDwe2D2E8VMB6grtWW82p+B4VI51ra196pYb3hUE//gNV//uFy+/kVY0afJOOD7g8bi0BsPXijRmnKaxWilr1nThLqu2WONYTsaHsTg+VIDbsnWEZ7cVg80d0b9xrZwrER3DrrQqcN5Qez2U0GmAD13AvVbZ1wb32yC9DOkZAq6E3W5wh9cp69KbqOLaW6jx5X/gtyxh8TAXlwgl0bN+qfDE0x82S3COsKuHlJZkKr/5Lko/+kTqXryQXNsqhcdeMCCv4GrxPp3NRqacPCick9lxU1h+IgGfiGAyjptC5jnHsSCPR/zLYp1D31TmJMo8+ljyQ/HMmDldNtEJ+zeem50vu78GPOJQJKVT3qnfA1zqpLQpUyDYDOQDv+GnqTOzBR6OBWI76amce8cIaZQ9YwramjkUY5CIO+DUeuxt6dm0VrgM6MdPAcTsJF1hKU289Xe07ZF7xaovr+ay3V8ERA1bg2Cmw64YGdOmCD1CAx1CrFXhN/Z4ZNMr6Yw0uHY5lNJ+SjnzErBWFSWNrxBp5U05uXJ4ZFQm7gTkkVdDZKLnIwSdjC1qIlsl6wesr4mEtCMP+WMmHQz4PWLBiU1wCeeygf4+EPt9/d1dGxIxreOg4S5TaTSWXbTewYDPO2i02vZ/W0oRa6ahCBQkX/UGCnu9IolQYtFGfIpOUMgTIBQUXoghl2wUUoHILJNnk77iMHRWSM73otRSqK0Jg7dM+LeLne45Hww+OZ8j6wts40459XzS5JVQYPtG8qxbKpv48B4/W4fA6ZIOP4ZM0+bGQ9LiCjaUPIXOJCKCfJtXAkt+LiYqWzvY30Y21wH7Ay+zZ6bguiAEw5hJZAB3VyileApqaTeMzRAqGgyTZ/Xn5IJUEHnoIcaTTzs/7q8SHFrA4rw5UUAnz4al5KpaL+5jv6OU0y8kFfuvs6/PLvGmvOwa2L6J/PXbRJCG8Ivhf6wggoj9nW1QfHvIBshkmQOY4HXJi0F6KyZQkHyVy8mzfQuglge6i4fshx9NximH78hlg0nJSaG865bI+phOJySLiEFFnUNOB6Bql3CbTllwPinA3GIeN3nWLBaenZwcilGGSFUixl7eTEKGNAl/KJW8LsOpAXGvsbicrEedIkzMI81a5nVxZl3JZzBbsnf9ze/x/OL6aePuFULr2Et+/Edw8lmNmzZuWr/wwxWFEw/jkCnJ3d/ncDud3eakkQV97MlMxiuMTPghR6dQWGWHrh3B1izimKMI/3SzGUpNNhlBROYpc4Uba4xdCsJyciOe+UprisD0gZY6gTXl+E/Z/VQNZVJjTwEu5nTVvAFamjBz+VtqhVKmBpdmXUJXWIZ62XYEQidyzwSZY7vxjlQRPqcGN2PczcoXh+Jxkig287HdmH3FTaijvqRCDiz5SkenuGuXTif0GhGv6R2EVJgs8Lu4d5j/ibBQsUUE0EsQtxQRbsPa/DIZAu6qtIqYUkxWPDvCGc8GHCJEN8rKLPpVuPICIvD6hy4rn9SAUIncMozp+V5lSjYI2UESJBL3jza3CH2XMmzTt5iwk3O/BNEXoh8keSWbn680GsUagRbSlBfGxEKtDgp/UqYgeF6MY8uZoG8eczYpR+V8OTERkxsWDnRi0QtSjZmBNj1b9gaVYiO21gz09LSygmpJSRUEv+nzT79kM2VWSVkhpHvZGTfc/PC7jzwQle54+6O3s0rLT+1uavjSO+BaXjBx0s9lK1IUyMTvUKk1tq+XqiORuUC3w1eFG6rYEdq3Y5leDtRWcHYt9tpjBOf37uxLEZMz1Uo6syCGWHxxREq4LcTijgOKYUv+ELeM/xPXiuuViq9OEhQ3wxFn2OKBASzgjGByEiQSJkomeub+PBjyhgAjyJAgCNMoL9mA4EUGr1hoz4MZk4mel0nYhZft+FIsvNdniywBYCysDwhLjugEBgiKHYHgQqLuks03FpHrgnujHndc+dtDxl+uE/dlJCZnNFPE+53pURFPkR1/ZyJwXb6eRJItKSHd4xBGEp6XsSE6kIcwXte4T7Qk7ddugbGA19On1moN0GN0Mm6veRO43ptdNuZ8T39fx6IXns175+EHwkqzPTmtbPrM40z2pAJbesacxOgxwTdVblqvNRhNWr1+/zVXGpb5K+5vwpxYUko7VvZY/CrkJXohimNyersdtmZpdxfJSFC2j6uUw54jP5OfsYMAY/HUcyoRQiYInRdLYpGvtnYNz9yFAeH7FMxx9Qb50Kjj2x+FhGQZsb+HFE+Dxw6/bPiKhr96osSvFRZxfp8ggr2lMImbBNkSpIr3qVKxU7+I81Js54xow+IW5LR6qh1uBXtI4cHOfuJWMCS218v9KdvjxTviJt+h50ZCcnoVDndUyuZMuT6KnQ4eE8XQd0nUdY912EcPt2zdsolt7iB6vWxlTB5jSU4RngObv1i08MXf/voVgTq2r17ZWrN2lXLMrDkFepPZPMzMI3HGAjbk80ZSCR+b/bGLsldeovEsrgTXTOR4hMInmK5I6i9HyLO5kuMcFVqDaHhMYGjzUEymwmCR4RD/nXgGd6RaK+dhVMuBI0yMYv8lxrgQ3SxiBYThRROxT1NIHjj26eZ3a+WFJNYVhL1e5E0MiXeIXIr8N2BJYptLngRCr+BnMO6OB62wdUpcy++WZBHO2/DIqUAkUQ/hmMaBF1x/dmfmjQyEW25AfjdbWvgZesMQ9BN70zLjQHt2ejc/j+sMKcrf2Y9H1DkWidfZIOei5CgikUM+JLtA8DPEu7Xi3WxkUILTi8nM9UjsXSW2ERrmfsBjFZDzXYo6Qwpzv8m5Mf1AkEbZDz4ab7fINRmV65kYK1wfDcjBJ6LO6A/hPcv8nevPdDLEKEdGcpyiw5aWngLGvKse6l/49BNvPn/HbZxnqSWx0tpWvXL5XWDou7oRSD73oKetetu2rxUAgsb5urqBD10iGMTb1i4vWIQi5MPfwpLgD+DvTmHG4w0O/F09YnOwgMMJ3DwgOL+vvV2OnsE13rY24RQWDcrPYDEdhpLn440GcR9nDwj0OkUws7+zR3438TM6xYogv5vrEQ3j3Xxfe4fo+NAA3t3ZLQbO3+MQCjTjVH4uh/+xr663pV1kKWbdwdfSJtoUGuR3d4prg85+8uP9TFC+jk785pGDwFH/oXfjfQwLOBuxt0Nud9DZh3f2CALyd0P5RtuZoHyoD1svuI3cblZgY4GQ3HdsE0L9/WgXK4FBtNPf0yuUVV8X6uz2yH+3dQAWBQVGZtNhLIz+wnc+z/XnTQz4nQwluQ4BEVCvFvUP4xk8T7nOUfR5xOtHPTpEncMYK76G68ljFeiJP6MbfT6wY7zZUsRY38d9x2OFceQ+V3C7B+LjTQpxH3/nvvPj9zD6TlKqRkxqva0tDY62lvbh7sKJQO9oNPYCO0/uFgCSnJVd4Xe7XX8494w/4wEi7TAnT82pGFsaDYdD+4XdVRqRFGnbb39GHa+/BBwdo6qfXUTubdvI39pFldf+AATupYHKStp8w4UYGi21v/US1fzhNlJr7dTw13uo+ZlHUTkTbf7ZD8m5fhX6UEeVV51DntoG8jY1U+X1F4jn9n7+CW371ZVAMyZq+NufqenRe0mdkoN330it/3oeyESNd19C/WvXUaC9G+87n4K9/eT4YjFV/fyHUPgyqO31F6n6zptIZUylmj/fQa3//DskrQXPvYocSxaBKD20+eqzyd/XT57mVtp01dmYrETdH79D1XdcD8lupcbH/4L3/xHcyk5bb7uauj5+jySjnapuvIj6120QxMltjfT7qG/pl7T1lz8BV7VQ0z/+RrX33SFs2DX3/ZqanniIVNYM2vp/V1P3e+9QGNdvvu48tLlVtH3TT88VYQzdX3yC91wFJc9OLS8+QfUP3E1KyUTbf3MjdX2Ed0t6vO8CctVUU9Dlpk2Xf49Cfdznm9B35xMFldTywlO0/d5bofBnUN0f76DGZx8lNZT9rbddQY5Fn0IRDtOWn11Mnupacm3cRFtuvgSMRUdd771NW391NZTgHKp/4n6qf/D34hnb7voZtfN4eyJUhXe4tm+H4tpPG9FfIY8f/bCKttxyGQjRRB1vvES1f7xNjFvNPb+kjrdeIaUhGWPyI+r69AOM4ciMgxz0AdrNSM3NL4jb4yP/vu/3z9esWbWW0cnR5198+m5ZC6DRzkkvKDqVRcjUE05+T6PTzVVrdSbOMOZoa23pbKivx0NzR47dZUsM+8OYyitInWQjbVY+mYrLxC5y+vxiMuQXQsM3kKGwjPS5uSKo2FhSAQ09TUQImcrGkpZzsmfkkJEj6A06PCOXTCWlwlSnzy8hQ16esO4YcJ8+K5PUtiQyjRlHGrsd70wlMzsm4W9NRi6ZS8uEtUKfh/sKi0S+FhOey6kpeMHDXD5OfjffVz5W5Ehn70RT6RiRI4UzBBiLi8Wikb6whIz5+cI0KeqcmUEqvNtcPp60yXhfWgaZUX9eIWXrhXgGrtXxu/Pz5AB3rnNmFu6zyfeB0Dh8z1QxXrhEqFPT5b7D77qcQhFjqzYZSF9QhnbnizobS8aQLjNTbLfJbdGmpYAIM+X3mY2oV47oc46R1XP9iwqFVUWk98jNQT/jffzuVNyXlCT+1tiswnWYdwsR7RbjViqSwBpwnz4nB/U3YUzGki4d7bbYhN+QJsUu8thwu8V4ZxfI7zbq0OfFwp2E/Y0MhXhGdrYYCzHG3OfcVtRfbTGJXPqmMePloPAR8Nm+jvaWntbmJsDvvIS+kVVavt6SlJKvNRjSWrZtqf7ytZdfG6JOzi15RUVB8e1vffgeNNry3Vewwv5QMMi5nS1avcE4Ui4fE0Ec6YAAPhEcrE7OAnfrFRuZ8X6jYUe7wHC8uW6wp01sUMx4MOjsxECkCxwaGnCgA3Io4uoT4XO8Y3eov1uIWuZEQUebwORKYPtgbyuII1WGGngG3yfePdgvdsjjjGaMYdX2DPG7ApiVc9MEu1tQhyTZ67G3HRMkE1A/JLIDa5KzRdoQ1g9USVkUcnQIm7IqKZ1CPa1iIy+2qITQFrVVTucTRv3U/G6vW6wYqlHniMshMLgK7Qo5OwRm5fbyM5TWZAENQugDTWq2cJ0N9fXI7eaUf360G+8O93UJd1wV2h3qbhWemYyTg71tYsM3Vv7CuE+djPr7PGLjAg04MO+OIt4N5hPm/lJqRKIlsRuIySa+c/15C0vWV8S7uc4+l8Do/LwwpwXnVIbop5AYK4tYHAp2tYC40+Ugbe5zSNYIB/eg3ZqUbOFTw+so6qQMcR9b7DjfDvczb2bHOluYxxu/sx4RHuwTfcD0EnEP7BPWMHbH4WLHMZVabd6D/d39yt13XLHsjdde3Ingufz6xKMvXXD1dedNmn/cLGi7O4F/V29Pi7OjvbNgguxcv1/ZC0a+PruL5eKbZLJNuBIrBCZlZVbeSTsm/L3FdpfR4LD3jfBd0givjX1F0tU93h8dmY+URDSCPej2YtFRHMDkwCPNNhxLBJeCuA3yJgq8YBXwyv1Pyt03cB5hWB+Xrob6ar/XM5A/bsKMXUP+6jes2/DOww98sHXZkvtYEOyUpoNLd1PDS5/+8+m2ijnznmaC72luajTa7TbO02dOSs42WKwmEH6XyZ6UnNj+e6+2YdbCwX0lGqGJSVLLq3DECxre+Mrlfno2CFu9UnB9iQOE2YkLUiLY2gxFzi2sBOy0xRFPnP9S2IMBxiNs84/uY2uWxA7aYveN2FdOCBFtH5PXA/6fvesAr6LM2mdu7zW9Bwi9dxBULChW1HV17W3tZV11d/9Vd9e2dtfeu9h7QUUERKogvSUESO/l9l7mP+fMJAQIECAoaMbnmpDklpk53/ne0953x+qryCxgbSwL7VZMDFysVRvf/UKXJ5IUWkMHJe3Ohi0k/h9p+D3S/lqc+9aopc8uJg/M1pnnMi41xHH7QaSTzywzjemtXPxL+FogUrqRA20VwjItwjOFzsa7Nt9zhTz+yVkkgUdA9zYIEvC4W61p6bZUnU7G7nHctGpq7JlZmejtNRsWzH8Pjf3ljsa+g8Gjp4+4GxuCFmdKNj351b/95bHT/3LbGf3HT5xCHWjRcCjRUFG2wWC1HbFngxe5pzm8rRS2vPAoZwOJYUvcjdFL/e/Us2IHY3Y+WIaPRjw3CABhCm+l7YWUvXs2SluS4jV1WrYu/AJaVi2HQF01Dz5QewNlfYgNgdQBjYhvHcNGg33SFMTBuRAP+XArdss02buek2CwguuHb6Hqq0940ex8M2gxhV0uSB0xAtKPPQkUCD2UJjvCC69kGLiga2a8DM1rVnFvDp132OOGnONPgswzLoR4xC+lHtsb0Dr4ZoQ/vnXLYdubL3GPjaDY9XrEEEIRnUfWCaeCKi2f4Yd0PgKoEX40zvocqmZ+zjMCnT2/q+FZxOuFtGF4jsefDALCGZKuZybjNtJaUZImUiNsDVdXQM0n70Lr2hUQbm7kFmlqFiSmiMJzL2HYVP3VF9wqYsTYJ/fMc7iqrNBZ2enBHqg6PI0NlQRl0vIK0tvgy9v/uf3Jc27/95WZvYv6Tzn/4vyZzz25i6bwDob743tv+4+9+LJWo9Xm+Ns7H1+DAW97qIwe3tln5Jgpe8/Hi4zVKLVUN38exJIKnkba1eCFHbY9pnLA1a3BwCZ11Bjoden1YMCgJuaukyq0e9rmiCuS6K5FFV7gt6Di/bfAi15d6nTUccGIdaTwZoS9Hm5JbtmwASq//w7M770O+X84D7JOPx+9fiokvU2dGj23BldXQc0P8zCYdnTSIaoEX20DGFOcENqyAdxr34b06ReDBoM8vLP42ZTg2bAOqufNBWNGOns0Gvtzb1yPC44C24E8bK12OpkqY7tEDfCMQLS1FWrmzeNGPHovSQO1DbkJPB9MZhzYsBICs76E1JPPwSA5jyud5NkDlRVQNWc2GDCg5oLPDlBip6mp9sstdLhNEgNBoKEJDCQnWroO/JUzwXncGaBKT5eMXpQ49tXmdGj9YTase+Ru8NfVssPjniTcfSk13bxuLfgq7wYDBrfu8koIub3g7F0IGaNHg2fZIrCOPwZ0vftJVePdUO1l9uk7rOOH1hlN5htfev1ydGq95daCms6et4PG02OX/MmPhpHVZ9TYYUqVKgO/1+3UkNYVAMuNWDE80drZX0ISL4AejUBrMXO2gMbepN4KiS6bJ6N0WuZP0WLkHscb2bRmLTTN+wasuYUSC1bYB0Kn273EQ660YlBU1wAb770VNr/xMsTQmIgRjPoyyGsmIjGm2OA6AC0CtYq7ILV442jwu+a7mRDYtAocA0dyPwj11Gy3JqF9ise/fiU0rVwm9bLbrPyZaUpLLbdQ08RS6pAhYCnqD/4tm8C/YTnebAsafQEbePOCOeCtKAed08HUGXq7HSKBEDQvWwyOrFQ8hyqmA6SMj1TtlQxOgT8LlW+B+vnfc/aCPjeN56mJBAk/Axdn8dyshb3AOXgohKrKwLtqCfcX8eA3nq9v42rwbi4GfVoaZ0t4cokKWVTAItpCi4XPi85FOh+pua+N24e6X3kkkMYU0cNbCnuDt3g9LrAVGLA7JO5OhCQqewb4Vv0Ey/92DYTR6VkLCnkRUG2AJrmI0kMlC2XEYgnQo2en9zdlZUHKCOqIrQE/7mbUAkfXbXcxjiAfHfvh8ZGGP1Ku+n7W/Fduu+kB/PGePTzCmsBVAwoeXfLpR/UnXHH1yKPPu/AkYmIN+XxejV6v6xohk1TqpmYv6v/WEpZDjxkn2jXGuBJ3oSk3T8KCSpp39DInezjQih4oDRx9eoG7rAxW33MbjE19E/QDBiEWb+gE49KwSQbEa2phzT+uhXqEC9ZevfCCasFfX8eekfgm6Wd0E6lzkbhwiA24Bb0MVUhN2dnMDFC94EcINlwBox5+GfS9+3J2hvtoEgnZ9hM8Nkc3hpqluAM0npDpR+KgpzZlNDBOc+r0TBFCxE7hqq1gHDGGX4OgFJHLanHHiyKepcVHNOG+qhooefcdGPb3O9G40RkotSBok/LwuqQKSAPiRkq9kgQlFZNo/leQFES0zhR8vxQw5uRylVSHMQoxroXKS8E4dIw0eyr3sdBOSgZM3O7UyUgQhKqdtEPwbCp3NqLDwJ1AKfPXECckkabScyXduBgvFF16JoRxkYa2FoOuaDBXVJM+P5S+8BhEglFw9i/i6TAqBPY6dTo7IeKWJKYIGrZJ0mA7wV2a8aXzwd1Yg4udKrHhylIwDB6Jr6lum7/uJLQRk8SHqjXojWinKsq9f/74Iz+WrlhGjMGle/XwdOC/XR89dN+SfmPHx4pGjzuH+CUfOGf6/b2GjUi1pWdkdsXgKY2nxOAk85iToWD6uRCpr2IDI49LvIeFJ54Evc+9ACzZmZA+YRKkDh+OnnEYB4Wukk2I89RMM+2rqWGajoxJxwMgHuYAqb3kLO0kQjgOa++6GepXrQA70fQhTvfj81KHDYeBl12JX0eiAaQyTbQhJx8NLAVseQWQNmUa+Mu3gmfLFvQyDmYfcG8rh0hdOdjzs/Az13EqT+rYAw6kdFnZkH3iWZB11DHQsmIJbu/17e2ug665EXKnTmOyJzIOLrtT3zt6KfbwsRBYBwyGPMTr5uwcaFj4g4RG0KDI27u3lfE4IC12/7pVoM6QPDN3cuJ7U20i+7hTIGvq6eDdsBo86BCIcZn4avpffg0UnnoG76I0+ELtwCzkkN8bNBgXkWYq8dS0rl3NHPLEYDz0hlsZOjoHDoK0sRP4OgTwnLnBDhfY4BtvY6Vt56CBkDsN33PzJiZ9pftLDHF6XLxhdFJqXDS6bDpHXGxqPXhwZyn76F2m76YhINrBh9/yf+AYMhR3AtxB9DqwDxmJu8QoaFy+mITJeBpOh9fAiTsHLQi6xxpbCsKa/txfs7ukRyQYDNwx9ch7MM7MQNvMQFw//4unHvs7/qp4t7Ovnf0QPX10zZzZdcREptHpDf/6/Juj0EN3XYU7KQ3sqjAwVKr1nF+nrUyUsSAVP4JbSyCAmFhRWyX1vChU0Oe06aBEPF69ZCkY0/CEUxALrl8L7nlfSMIK/YZzAYQUoqW2WwuUPf8w1C5eDHZi98ILRaIJhaecCn3+dCH4yrZKUu9JiWOdyFcp503FHH2fITD64Zdg05P3QM1cxPK449j69Ib6n34Ca3YW3tS+7OlMI8bh89wSjzl5RJ0DhIBb1nKV0p/kCRMeFwQwWCYOHIlwNAnRlkbQyzsb/53dgbtAJijLN0sUd6Bsj21okqtq9re4YEXIOeIICCEWN46ehK8TkN6bhl6oPTqplDC83FNOHI8K9Ij+rZsgiLCHYham3GtqAG2Wp53WhAZkAvWNHCDqedRRCW4M6omQlYpIxHHfjtXRMRFlRgAdFSUA0qdM5Z2ZKLgJUlPGS2Wy8mfgFgo5o0JZL+qtp2ESDY1pYtzRa/qZoEes3jj3a24So9lWcdMaSJt4NKSNGg1lM7/i/hqCPECC0GQntMjbmCr2kODT6LS6RxatODoeiRTIlBxNO2dlumTwdDx7/Z+LL33wse/Hn3bmCbhdTNXsQ29De9chYWHEddTU1Ea1IW2RkmhuIhqWChP2VPQQaLBaPeSdeR60bNrEPSwU7EQxyAzgzdOom6Bx7c9gn3IqQo5CTjsGi9dBxecfcmWSblQQcXz20UdD73POg9afFvDQB1VfiZGWBkb06In0vfqBypHCxQ0V4schdz4MIdyWXZtLecZTg5Chfu06cA4fBcH1y5lVQEOzosSZQmmzmJ+HKCQ4IxPFyuLIRPHBrbK4eDW5fcA8bAIX2hgWSZPcaC1eHqreOVNFxqBHaFL2zddgGjAE8iZM4Z1ChYuEFzh1H+L1pAFnURZBkyackpIQAxk2NXfhz/SFA8AyfLzMtkbBsR+sg4ZB3z/8AReIjjnoY34JsomyllO7ziu9rkzKmuBB+ihz7mdMOhoXCnpo3HmtuGvGAn6pWY7SrNTlKEhN3jQSuD2fDhJfJTUM0jwD3l9y2Fq815oBo0G/YjkGrlbeqS29i/AXWmZAkwJkiQxgz1wBPM53MolL1JaWlLx487Xv7ZXdYHe/QC/fhHj+rm2rV9YWDBmehRBnoDM7J3efclhtD2F7MUmUI3nugJdZYk1DxyNOH8ZdkKLPzwoYTcWbWaWCcCPhZWu/AeAv2wbRbRvxhhZhoGqGhvmzmF+dsCtN+pMaRsEpp4Nr9c9MR0GU3DRBQ97LNGQc4kyJN5IKT2pFFuJ0NKiUAuh/1a3w8+3XM3sxBdXEZEDZBbXBCo0fvgTpZ12Kf5eyneNSUOzK5SxKBpdsbeRJJtsRCMOIEZeoQSjFBh0YkNvZkKV0JmNoP0ImNG4Dwq9NzzyB9x5hHX1WSzqYMZjbnrnppJglU4gnImFcsFbWgCI2X+q9pzRnIoLB48TjwNJvCES2ruGeIM+aFTJjmrgLtQ3vW4kkGyj9PlRTCWnTrwBb/4EQ2raBq6A+vL68c1MRT5DpPYirE2McnmvA86XMTO2CHxAaHcGtAzSrTOwLliNOxD9UQ/bp50HGsdMhsG4JxmIO7rgU5FkFrrImk+3s050dsUgktH7BD8say7c1f//Gq1+QbOVeexr39Es0+iXz351x9xv/vPU/QZ+3sjtqdHzLiA6Cxuqon5pOiDwNeVA0DOqvVjF93nYxLJoxFdRKZp+NNtezokeiuQlaV/7EgSQNIFOGJAuhAL1eEKGMJjWd8S3l3u1HnQLGYWPxRuCCotw08apHpHx3rKUarGMmII6dyCIN3MKKj5b16zGwzuXXjlRtk4ZCdndCLOcuUU7T2BvTXria+HwEGuPfw2Wm1KQlP5/54QONuJNRYQwfG557Bi9LBFRqBXd2UhGv0+2dpeeVcsJK4u5kHn0aYYzLOX2EMWIId3oMzkVbNp5zC3t6CjyJyDaB11eCWPIiFEFO44qcU6fAOOFtAPWAESBaMyGwpUQai1RKdQ0aDZRmP2Lo/cdiLGGEMAamelIjaXXBmv89wgG8uaAPWMdOxl3Livfbi4vSCdq+g8A8aRoEK7aBa/kiqYsW/zbmaVNhhD0YfDg0541XHvvk0Qfv8jY3vbO9IX8/PHwHo6feA+onPqJbitJ0Y+SLSMZIF05UyYTFxGeDECCKhqdQqdqfQFzobSOQCYQD1FsTiyYhgBeJ+8oZy+pwF+gnMQ/T60ckiUjbEVMRXhRyanNXSUwK0GJMEeIYNBzqFi2UAkk0hAAGvlT9VDlSOU2mLShCaJPK7b27ZEapWpqQe9C5nz0uec8uHAQNlIiHh93yd1h801UcvFoLC8FbWQUl77wNYx96AsDbCNGohd8fQpHO4aM8ZCPKLR3bR6U7XHg8V31RP9Bg8C0k0IjD0qywiHFT4vOPJGhGmjWyeEW77L1a0shVCKS/dRyYBg6RKrl0LRSStCctLpL/MeQXQf5pZ6ORP8gBKiUJKPmw+rnnIGPYUEjXmzl1qU3LQKQXhbi3nhv1Us68HLxLZmOIZIeox8vjngrm/Nx9Z7rBYnXc8ub7LyC0m4ZopEvDr10puV2AjzOj4VBg68qfV9E2ckA9GLT9ydQNRP5DwSR7ea6UOsC/ZSOz6GrMRs6ds7QNegKCJuT1qR9eoAargAdXeJRz6pTfJREyJeLJuLuViYtibhdXUHUFfTlDIo31CZ33+8SjYCRyKPKAXABTMn6lHn0SRKatNlZXKd3gnau+siYt81228SwqOxSG9nYDcHFRcEsebeBf/sHZJCIloiDWW1UJq++9E8KlGyCw5HtIev0dRII7Vqol/ntmEhMUuzESaXBeDHmZPFYwmrnIpUSoxoGuuBMdOS7cZHI7axgTTeAiIP1b6vxUpaSCChegGnddhUErjWXivYyFPZB7/lXQ99wLwFdRzvQcRvw7JUK02p9XwOrHHoCVf7kUyl5+AqI1taBx5EhD8fgRHMeeCoZh48F61Img69NfmizbTYamZnNxMbWxo7FnhP3+f3R5TKMLDToT6Svio/pvX3ruqWgo6D9QF0/FEyYXMhglzhcdrnpHJoRK1kHpUw+xWIEKvQNti5RTtuTlQdTt5sCHsiwKpmwLShNMFAfgBaOcO934NjlEIvzRpOfwZFPHvpLOPxIVr2wS/RzTBCqY8TZGxGvy4ozWVzG5EuwcvPOYrtA+lN7WIKbYB7o4ytsHtxRz+nDIX/9PognBWIa4ZlpIZvL7OWAeOkoKXJWaHfvjQKK8k7RqBSmfvcfWAVmtm+j1qNeHafZiO14eefSvLVEicV2q2s+HnxcJb3+NWKw9ViM6wyREoe+t98KIf/wLNIjVvdVVXEWmtCmR3nrrG2Dji0/B8uvPh6oZL+B9owk3HcMc7qNhBxXf4z3btHjh5zUlxUu5rSAY6IdIROgWg1/x7cxVkVAwgAFr72uffflxg9WWsv/4XbooYcSqSfSiusI+vLWFG6qh8oX/wap/Xg+ubVu5GENem2ghcqaezAPKkaZ6iZqZ5jzRQARZUoUNXB4AZi/EAaWCCxgkItxVmgexbQuXPRxlIAiekOQNfxafhyGBsDt2AlmKkYtuCkG2xy6R+XMKjsbuXD9+w4HqgMuuYEUSMhJzdha3M1R+9zUoYwGIbPpZpjdRtFUc2ciZwppeR6NpY33Zf6dEhS6aRZVhZZJZHeJ7DCC332QVK4IkxTDkXXwdjHn8Feh79nms/Eep0qAcp5jRiQU9fljz8D2w8e5b8DlR7hmSZo73zlZw7EWXXT/0mOOYJrJ83Zp1CGm6dMJ7xfCfP/noV6n5BaeNOenUU/DitrcNe1uamxKxWNSekZm9L3CG0n41ixaxXCHPr1PTFcKPqNvDc6PGjEyWk6FqaP6Jp0Da+COgef63XN0kvEsVU5Udt9PGpnYtIBUugCjCD1LZoCAq1trMQwUStEjs9XMJeJMidbXs0SmIE7nJScvldMp8UP8KTfXz/KW1E0gj00K3YWlOTaq65uEFWXaDvSieh3vtz5y98NdUQ9kXnzGeN2ZmwJa33gB1JAg5Z17Iac/2gE6QeGho+2+jQBHaWRz2tyFSIRMoJSSk015t7uKLUpCMkDAaj6DTKYBeN90BOdUV4F61DBoWfMcyplG1nqFOHL1+2Vdf8E4/4M5HQTTad9UEwIOq/SG/z+PIzMqVs1vcz1G3tXTzc9df+XS3QRrcKgqHH3P80F1Gq2IxTzS8K8323kddFUx3TbR5xLPub2jkmUsqu1Pl0F9fz2XuorPPhYHX3MSVO0Fu3eV0InpuUa1j8Sut3SHhfNxuSao9WFcjGaw82Ez0FXvtr2Y9HlLc28hCalwcQY+pw5tBTMJRViEESSZyd5zpsmwMBc6iTDPBsUCX2+ylnY+GqklX1b1uNfS/6T+QNnwU+KqqOVWqRby9+fPPwbO1lLnut3tw2fAp1ZtIdghiDwR2yrpPRHQlK/e15f73+BzcFWk4hOSGlHgf6PnEl08ZHooVMs46D4be+wyMvP9psOblctsBXTMLLurq72dB89cfgEjzuHq7JLLWYZdKxGPBaCi0i71l9i4qQhsd220GH49Gn1TrdHnNVZXljRVlZW0/N9kdabja9l0wgTIa6DVpKofJePh7qf+ZaPSyj5oCw274KxRMOxVals7j7ZHy6UQWRB6dgpqkiAaZnQPWQUNYLIC2XMLRTavXYBCVwReKc7rCjh2Zu27dCFFMTojVV0PjsoXtBkuMXLY+GOwya4Ekf05T/qLYGWdLW65VkBgXqG8lLpMddYnCQ4Y1LPwlSLQWET8o7U4Y8u+HweC0sUqK3mblvPbGpx9kGR0tZa7aOgkVUtzAXZBc5NqHoZbO97ztlXG5OirsSQZHqgJxa0S4vg4fDfygwW9BbZCGPyJeiDVVYdwfgdRpp8Dw+9DoC/NYBIPox5O4a7lWLgH37I+5DVsU1bIwhPSeerPFbs/MzGsb0MaYsrli/dp1UkydfBCNvqg7II0uGg5lU3db2drVPyZwmaXlF7LQa3N1ZSVd8KyiftauZ+AxzkFPTL0b1DcRd7Ww9CLlusmLqc02DjJjzfXQ8tOPUlCq10OkoZ63POsRx4M6Ixe9QDMk7VmQNm4y1M6exdPx1DXYvHY1tKz+GRzjp0DTwu/BROX1vEJZwqaTYREtvqfBAuXPPAjebWUsOMwCydTXMWIUhOprJVoLysDQSCBNTu1CWd1WAZcLaRSUK7vOW9WWbiVj5ewVOQTqJHTXg7F/Pxh2xwOw7Lar0Rs2Mq03FccCpECCizMWisvDJlJXI3t4hUImqj3wiSZJY1XBO++eYwIRg/40aPnifdj01EPMY0ntD/0vvZyr26r8fjwbAFSEo0Y0hDf6AX0ha9oZ4Hrsv+hULbzY43GRZ1pbFqOj87aCferpMu1HEoJer7exfFtJ4bARDrzOSldD3dbZr7301sX3PfwwOmRDdfGmfrtrGOuyh6fcZnN1FeXhYfS0U84ed8r0c9t+l9GrzwDcTgbssb2ARAXU2nbvwKp30RAa+3BwDBrKbbIGSm+Rajbi7kB5KXiWL4LA1s2SmjXlh4N+0OXkg/34M7hSSgMVvPN4W8Ax+XhwDh/JXoIWBGH5ze+/B3H0cBknnCZVGtuzJR0eBB/MdsTmmVAz43nY+tE73O1IrxFsaIacY6dyzBCpqeDnU5ygy8lj4QFmU1OptxNDyZqxCmbalbIWyXhUZkXbyUiIj4auR4dMjyBjZinwTHDwrJAXTASDefvE42HwX27noJmasSiGocrxjlzxEj+nNH8htstJdgFfMj0Inw9s11QV5BQrcc5TelZk8qs9Gz33umPsQ/02JE/vrqiCGHp42hlbPn8TwqWbZf1W6T0VLOUTkHYmQcqrtcUkpB0Q9zRBtKaK08L0viTO0WvEqAltgge5/QeOvvyRJ+9FY9djPNk47+03Kg8Y0uA2EX/m2isepflANFp9W6BAR1NleXlV8caNu5s+og+vUqEH1eMN0nfwjKIUpKlVAk85tWtoYeBIg9NqxOU0NW/oVQSmfkPBNuUUsB1zOhePqKdEkGdVhXiYp//7XHY97nxKzl3rUyWFu5X33M6YnHpGqB+dKB8U5hRQWFJA5czkaRwxGINtT98D6596EFT4N9Sy662uAWt+Hnc9Bko3SpzpnAWJMmU1czEKOowTbBIBqFyYIc+qdaaDpXc/MPXqJ2VKdsn7U+aHaLe13GEodMijcxEFYzHH0FHMadkGx0jMIOZvhOyzL4Oicy/iYhj3zbTXAuQeFplaHORdZrtE0B5TMWhbiLlVBtDgzkrt3ByoUh8Nvge1HNsGDAFzn34Sj2ebwt8esk0kfUpzAjqbGQypDij78nPc0b1g7dsfxKYKfC8taDPwntgywbVoEVR/9REXFZXcFSri/UvlHidKDRPnKHNlcj+NAH6Xq7ly4/qNHfrflXiNbL7W1uaXbr7uiYUfvVfaHZAG3A31n2l0+kvw2+Edf56Sk5fX+UAIMY7ZIFxVCWUfPsYRu698K1Mt059TFqVh8ULOMxOjAOHDlFHjIeWoU5lPMBnwMiui0mSSgiaiiaYMRNC7Yz88BUS+FrCNnQRDbrwNVj14NwTxVwYMNqlXZN3TT4Fz9neQddJpPDKoltkCSQrHV7IO6mbPhNaSYin4RQjjQ4+k0qhh0E23QqS2ilNoRERKTLhk7No+Q8GLMKthzizeCUhLigyXNaTQ4CjoIr9q7dMbrAOHSKxhHcb/cNOH+k/fB9+2LWhA4Xbla7oe4ZZm2Pbxe7wb2nr3Bm1qmiw9o5R6YaJe6HX9PyGAn6t63hzmju+4w3CQTEUnYvol0QRx74PQggYX+LIF0LhgLg/gRPGa0WCIFIPEoGrO9/y61qI+YO4/hBe9ci+AlbOJ1BqA8QhdG1IOXP/U/yBz3ASwYLwVQYxOaWLfulVQjdcxGgwxTCOxCY0Bg1eMm1hcWqPl2IyIW0VZWYQ8vNFqMVMPvLB9xdPvgwMmTn5r8/KfQt1i8Ojlp1NHWiwSCWPQWp5e2LsQV7KWepEDbpcrJRdB8i4TT0aIuz2w5cN3gQQlLPm5iNGl/hjKwbpKS6Bh5c+c5+YqqdEASZ8LlFlFYKSm/2RImu0UKajd02QftSE0Q8YfLoXh6JXWPfUIeCv8zBlPW39reTm0PPoAaPG9afqHZLipbSESkKR3jJnZeHOi4NlWDsbcPBh6899AgcG0b2sJB4ikHE4U3mlTTuadx7dxPZS89zboUlJYHIElK+X0YOW82eCvqYe+p+MCIzULSnHqLJIqHivHqqFx6UIomzULLAW5PN3FrdK4yEi4rHzWt6y+PeyKK9DTpUEUjZsZlKm12dfKgy4D73gYgnXng2vzZlYClMUu2rstCXcrGUIKu50hbh8b1OjAv7VUPh8nK3VQ16EUbKugeu5sVlzpNe1EsJ53IW7MKg4+IR5pH3zp7HXJqTE9Np+bho26DM9N8e3XMk2iyAVrciZ0DUmhxV9dCX3PuwjPOwsD18X8vGRIwfEdgxB8Ts3mkmK9yWRwZuO2jUdjZXmZWqPVEh/NCVdc/Z/Pn3jk8m7J0rTU1kylrxgkVH3x5GPPtlVa8eLozCkp9t1FYqzXhF6VGqEoEKS5RcJ2YfSMyRhpiRolLEcCXHjzicOl5as3wfX1u3iDvXI+uwttyLj9xwPNkHXun2HsQ8+CY+AgCOHWH6yrA4J7Kh4bVEDA5YJAq5dYcTh1SQYSwKA05vUjhDkRRt/3EChwkbnX/SzBA+I+d7vAOnoSaPJ68XQOFb6oQkyleDJO0laVzsnLlV7G56TwF41B05fvgm/RbEmlRm5zpefR82nnomHoMO5yIZeHG9+YIJa5KnUseuZZ8C2EN67kRjSKhRIYxJJQ2JA7HuIijreyBhdukHczvk5KaafhVuykCHsfPZZ42amXn1K41I5Nle2Qx4e7oJeTB9TnRFXgaEM9NH32FgQ3b8SXVnVegJIZG0jJw9/UyoZN8JBiMM4iaTHox+tOWrh6jNt47LC+AfxVVVBw6nQo/ON5fO2Zn0YWY6MAtq2El5afn2F2Otvtbf38eV8hxOHuyIDHPQods7JbPPzaubNLplxwiZiWV1B09ZPP/x/+iN8UvbwOo+ZShDse9PL5O2fq6CTRf+O1UfNKJv7ydobZtj5yKseTZCLxu1ssrPoXLi8B08DhoMwtkEUL9nLjeNuPcBOSbdIUGDFgGDTNnQlNC74Dz+YSHiGMRf1SYER4GeERGR5lhBxHHgNpw0aCEWGIZ90KiDbWsbIe9euH66rBPGwsS2uScBG3HFBcIsa4RN5eleWSvpK9qt5sAg31mZBeLJ6iZ/F33EprO+5kPJcEsyNrET7o0AkkRYnsnyvGMnUGwQli8uJqdEsjJBZ+Dykn/ZEVQMjTx921YBo0DAbd9h9Y/Z9bOFDVUi+MTsOMaPR8kKuw4l68BZuRgL4cdz3qQxJ5ZFAhtSnItOK0yIkRTjBYOAHQ8sUMsB85DUwjx3Hb867ZWRE0xAaso6A0CQNuuRPcGzbC1jdfwAXlwx1TUj8Rk34g0WxzZiZkX3AJZEw6ClxLf8R76Oa4iXZVir+ItIkC5mg4HGisqKjI7tuvvR509HkXnU+hD7e/lG+rvm3S6C7xj7QTMe0hU1Nw4T0PPDP2lNOPpumnjueIAcM2tVZr0RmNqTsW2pRoNBEmyyRmqUQtYvj0XA6MSHoGKOdMBkPDvFTGRm/vQVxHHZQqEi2beDxzl0DEv2/5ZAqW1Xr0IqksCBbavB5aF30Dot4KcZ+HCzsEdbTONNCQNCXBCdxxghVb2uUemfAfDccwYARYjjgeXzLEuwhlKeK4G0QbGiCyZRWo7E6W7KHiFrca042kwlhqJvg3rmUMSt7NiAvQQiOKxOqFgXVo0wpmbW+TgyS8zEMU1D1KTWBobKQDFcYYgjyw/ehTQUc1ATYwQcpyaIwQqazguU/wNYG+sD+4ER9TVoXgAAX52uwcqdax22KoEg0rABHqc6lcD0pHGg9OQ1vwmkhKbRxqhD4lGzjbQhNjtOMZRk6UIWcHfnelpMUaxmsphP3spYkljXRhqXLqLt6AlyCB16CZrx0FqKbCXgx9faUbuIqt4p3ez+dNCiAkzkD9NRgTRINebwVi+EJ56KP9jm9bvXLNa/+45Q6E2zO7C8OX0yCIHrejUSeefFLHa2awWHLqtpRuSMnJ1aHXMm/H1riS0ZOZR43Fkw2B6/swuDeuQS+k4WCTMxwU9SskugjCykm5J5vKL1aq8u3VR3Ve0qYhjXhzFX443DqHjAKzBxddYw16XTsbBBEakZJFhFQEXRLvjdQbruEUnKF3Px7/0+b3wdcKSjzwXDmN8RCIvnd/8OF5eEgWh4JPmS67bWEmt5SwlCTBG0qtSo1VwM1QejRc8mDeJXNxBymTtGBxx0vKqUlmH2DZH5FbdxPysPQOF4KKdbhjGQYMQkPvDa4fvoHGed8CNVZzsYgop1XqvcJB1m9Co6NhGs/CJHhXLwGBRglpFoGwNjXk4XWh3Y4WDitqBwM80US7krhLL1KEOygtmbkIxVZDYtsmcM37iieYiKOT5D6zz70Q/JvWcJMgpW+bf5zLEp9A2St0QNQSQr1VlrFHgRqvv5SoEHiaKSUnL20nYwfiPX3uhqv+621u+q6rJtKluT0yegwuWPSMqlsYKKRZUlLT8QInDVariNupZhfDw4sfb6jgWVXrlGmgsDkgWlECaoeTPbkoKtpz4jqNpKHEEIe0XA36Ljd9dRqWUKou5Mab4wXjyMkQ3roJ1xaxkHkhsG4ZSzyKxHeJnoYGy4nfktQGdTkFzLFC2Q4ep0t2CM7I6EPo4dHTanv1A5vBDIHVCyUvT1hYq2NuRMobM9c8nV04ANq83lKXIxVWmmtBwB3GPPE4UKxfzguCDUurkVVGFEz8ytVWEHk4RKrEJnbqZBSZJ5KwvQVfiwSRY3XbpJoHdTmis+m02LZTxZsMKhbygmHIGK5LBDcs512H05vk6WOSMAVxx9OiJghEdBzc0dgJtCQ5n7gSYVBOL9BRq4XVwVKkLGiG1yjU2AjagWPxGsYgiPdBQ23fIV17bGTGnUOb31vSuyVjl+n8zA4ncYMl5SmncHXJps05ffv3wQA2777ZP4ZuGDGgy+zWe4U0EkYqm5ReULgA3zX+8Pln//mMW/5xHqmGyB/Ai7+vyujVu4goznZbtSPaNWpsQlhBzVltHX1S1yPITV4Snk1GAzLeP1AyRFlaR2vi9D9tuwkMXhla+f2SGgdBBKoTUDGI/jwa3DtvIkvYGKQmqTahAiL3J6ECUeq3IaJQXtg80dWBWIlemxaJUtOuyMFdiWRQbYRItMPJU1QJXkSq3XhsuScTdzPy7ALvnFKxR9gXXk++P2apX15uM+YCiSwpz7R6rLCikgQiQv49zJvKKVESaQDJqbV1n9IwPF0XgmRxhDY0zE+Lnnc3XPQK4suhXbiDHi1ClW1Gq01htNkLuKWgtbXxyT9fdMtVjz/7H4wdezdVVjx8xwlH/a1bDf6JKy4svOCu+3+gFYUf3o0XRddG0hTy+Vp9rpaSlOyc0cRhs0fjoy4/HniWiyRtwGWHtLHc/CQeaHm8s4YXkVsEOI1HvfNymoy2clFWGek6n6V0Pm0KVSAHr+1ATJQ0rjoX1pUHSYS2yXyZ+FWQr0kHhTto71YU9tz9olDzDICkuJLY9/ZgUcrji22fLym2E0FJn6kDld5eu4/b+iUUHZ4vtkvn8CLXGHmAn6vOstRmu6JhB2fjqq/bQO19iCqK5CQBVd786BCtxAU/581Xr/7oofte7rbmMTo2LlpAKclX3A31dVTd6shIpjebHY7M7MF1W7eUintL/ra16ialgQVR/soXsY1WLZnoZmPvUKySOXPowhKBKX9PHrH9PRX79pq0YNrOAWD7922jcbuFZds59Nv/VuxwTdoWX9t12WvbpUSkyn/L77kf169tUqr9fMT292+/Pm1ErGIXr/cOz092GLEUpKF0asiLyMS58V01s6pLiovR1tqNvUOF1UoETPPfm/EFGvs3+3KaXcXwcQxcX1gzb3bTTS+/dXXh0OFDd0yOJBQmu0OAw+LoDjrunqN770XnhzUlVaFQKQ07/5yYg5+44qInMZ4khrGafYzwunag0Tccd/HlryCW5yGQ71554eOln3/8nVyEMiLOylv/47yFQa/H3XMje44DPTYv/+lnhVJhQLviASNqT3/qqkueIiY8Ivu98rGnl6NNrtrX11Xtyx9P/uN5KsTpbPDDjp3awsHN9q1GXTBkuBW33wRiq/heOeR7jp6jk4NweSIWi2T1KTKqNJr2VLfOZFL86Y67NyoUkqy8t6U5A4PW/cnhdf3425FjoxsW/LCQvkdPf2VaXsFZbb8jolVE80Mxat5CY1c9t67n2J8j5Pe5Ny5esEhnNOVq0b+3/Rwhcx4a+HO4CHTEWPDgn85csD+vv09emLD8NYP7PORpahT7j5/YPzWvIK9NCLbtyO0/YGg0FHaj0Zfioti5MtZz9By7PVpqqiuVanVi8JFTxikUih2wO3VJuhvqa8vWrt4y89knZqAtlh50g6fjufVbFmEAGzQ7nYPv/nru42TwpGuPAYYtf/DQwWjgpHAbEZPJ0kQsnqNQqpQ9EWLPsaeDVN/payIea0YobEQfybA54Ha1/PjBu/OOueDiE0kRfsmnH338+ZOPfoq/Wra/77Vf3peChebqqlb8ENxkLgjCUjEpUupoMOMto9GWVdTvyKpNG1bHolGh17ARw3pua8+xu4MmlqqLN20deMTkcRgLqrbj+WQIIfvbiXj8aEI1gyYfVXHSNTfMP6C8UFcKT50d6OXNt8744PuiUWN3OzEejxHVrhirKS2psWdkplucKak9t/d378/bN3y0j2hNyaaS9IJemQhlFIjP7btTmUFv3/reff854/KHn/hxJzs8eEHrTl7e99LN1z9QvHTxT5SKpHRRG9YijXv6VqVWm/AkSAUwgttUArF/fdvf9Ry/NzuX2yD0FkgKikRTZXlFJBjw2UjcWKMxIDR2kLGT9iqpa9NTqG8m6PO6G8rLtn38yP2vLPvq8xUH+jEUB/JkNOAv/3fpeXfdPG7Yv1Z99+0c+lljRXn5u/f86zVakW27iCMza5TBbEnxu1qroqGQn3KqB8ZR2XMcdge1cqC5+ZbOg2httRgJRypp97elpg0hg29zlh8+cM/rxUsW8WBHa21Nxc1jh/77vjNPvnvRxx88TpJMB/wxDuTJlLXBL1zaXT7zC+qjOdWWnm47++935Ki02h1UuwmbZfftPyaZiEdb62o3YdDrBND2GMLv5BBkHvtoQx0obU5V7uBhk4kpLk7dmHLLAnn4M/7692w0/AwJ8kQb0caeRNiiwK/J7vgciu46odf/eeuX1Dqs1RscOpPpVJqIaovAQz6fh/o7+Q2VKk3ewMFHYcBr7jGD3xGiYeqSJNhPOht0hUVyT1McAi1NrihC4LYmbLSdU/RmcxFlaGa/9tK7smNNdtfn6DaDf2bN5tLnbrjq/nkzXv/8py8+/W7d/LlcoIqEQoE7TjjqXsJhPbf9dx6rog+k3jFFMkbDn2gcEVjz+otfb128YGUcf1FTUly8+NOPZiFWn4229PiSzz7+oNuRVXe+mKu+7iOMpEvwW93ks//Ut/+4iaN0RqP5kUUrJsci4fTt6aZEAretRNdkMHuO38RBTAohaQ6CuqZVNGBiT4MJf7rk2JhBr46rNbBt3epvZ9z5j/eBByNgPXp2zyFt8PgBCbaslNNFJYOPmvKn4cdOnYrQ7DS1VidSLzO1d67/cd4CDELWXP3UCzd2Tey45zi8jR3tV2sG/wcPQ9LTAtbr7uWZWGXYB4phYzL0yQQUf/r+uk8evv/V5ypb1ikjYZmzvvtN46CV/dH4W9Do7zvirD9WDZgwKX/TkkXlAyYckT/m5NOO7zNqTGa/cRNaeoz99wBl5AkuoiEZPBGEoA/UKU5omT8Pih/5Fw/L29LTaG51VtDrWZ9cOh+E7DwQMrJ5sL3bg+f9LTx19UCjJz5vovGoGDTpyKGXP/zEG0ab3SlDG2YD3fOkVM9x+IJ2NjHmkVQqNKDIzABBL0Cy2g2bH7wNtn71JWh4/jcBfc/+U2Nqr34jE15PjUB8ADRbu1duf4DrL7/40DL4nYzf0HvEqJtPuvqGaQar1UTqIpaUFMPUS688S1AolD1G8lux9SQzETDHPCmbfPomuL/7GPSTTgHBZIXoz3PAv7WEh+iJ5pzU2UFngowjT/jIYLWfneS51kSXIM0NN11/aECa3cCcIBr9U09ddQk1/1CxYf2dn317NRl7Ih6PxSKRCAa5ph6LOcw8Oc2o0qA8je0RuZRKA+GWFkmqsv8QSBjssGHJBhCXrAOVjhil9SyAIfgkhjIaVg81b4WYdukf+k094QJRoZgBBwnt/uKtu2j0RDYyu+3fQZ/3OPq6Zu7s+aU/L9t8zj//fQ3IzRbe5qZGrcFgoE65HsM6hAPSaAwSNetBSMlCj45+zJIKgc/egcDK+aC9+01InXAkDJl2JPgrykBlskjD7mIHVTZihXMaQGytAn9t+f3GzMI5yUi47mAYveLXvl6exgaOTAZOnFww/ebbhrQZOw2Eu+rrtkTDEX+PVR3C3p14L5V6CKxdBgm3R+LWDLSCbdxkyD7vKqZPjDQ1gb+ujnPv8UQSYrhA4iTeHEtIj2gckqT1FY5BxaLFOSG/917S2Doo6/PXvmSfP/7I656mxgadydRHqzdMbg8u8MgfPHSimExEqVLbY1yH4CEoIRn0Q2TTcjAceToo09OZElCIBEHIKQTF+FNBgbCl9MXHoKK4Elq8cWhs9EJj066PhgYPtIZEaNxaAQ2rV16m0KhPZyYFWUhjd49DHtLsfKz6ftZ7GxcvFC6854Gzxpx06rE7xj7JZGtdbbkzK6c3/tPaY2GH0EH0MDotqLUGxN81ALm90P6NoLY4mICJWIQTP3wEDV/OAGVVDQwc3Z8Zm/dkpMSjH4/aId5QBr6a8seMGbnfx0OhQHd+7F/d4BHTu4kCxGSzUeryWOK+aaqqrO0zcvQIvACKgiHDjvS2NFfQLmBNTUvvsbRDxLkbTBBBmBIuXQ/GSaeBUouBq6iE+lkzmSMnc9ofoSUYheIla5i8Vtnk30mmZ3chgRJCCI1cc+f36n/Kaffo9Ia/JvebdvEQhDSy0SdJaIExfVPj1g0L57+dTG5PwkYCAU88FnP1mNkhhN0VWkhGEX831eENCkrdkBo9uH6YBa3zvgaFTgXGoaPBkpPLhLHcQ6MU9vogOnW91QKxlgbwbyv+i1KrPUaGuJ0+DjsP33YUL1lUOvHMswFx+yTG7qLYfjapeflDw35/w5YVy1f2GTVmZI/B/Vp2LqUQwZQG/jmfMKdm6nk3QNJTx5TixKXZ66//Zl53EhQMLJ0HZlUUtDn7Jt5O9C9xqxq8m9cL6tSMRx05+ZMSkUhA7AZGukPG4N+4/bYZeEIjR590ypEYvBp3Xr14ofWZvfuYo+FQgCqznRO39hwHFcaoVKwwEi3dBIIlRWJ5jnggqTFKNOhRH2cpQ3UN0PLhDCh+5Rk0fBHUrftBn0hZmxYXeOILhxvPSvuHSqW+UyJ3PcBz+CUrrXs7EMuPQ2M/8oYXXz+naPTYUd7mpoZwwB9Iyy9sF0DeuvLnpY6s7Gx7RmZujwn+olEqgN4JicZ68Hz7JlhOugQ0mRksTCdWbWF+SOWQyRArXQ01T/4bfLV1oLY5QU36XYnEflimwISuUY8bMiZMjln6DJkixmOLdl44N1x39eGH4Ttg+Z8ioeCjRqu1gf5du2Xz6lXfz/qs498UDhsxUmc06dDwV7UNlfQcBzkbQzlxSwZ4v/sAQqVrwXb2daDUCyD6mkHQWaFx9kyo+ngGqElVxZ4KXtLSIgVurQ4i4ej2fPu+PKJxiBN9t1oL5YuXqF01VU+TusmBEu0eciRJFMCW/LSEDb7vmPFH4WPMTlG8RqXR6J05uULA43WRimEPG8LBCkxVoDBaIFpdAXFfKSisqaAwW0FlQq8dikvK3xE32KdfAATaBbUCAps2QFOjD5QGA3jquiHPgJ4+4nKDdtWK4XjPb0UDeERMJn47Bk/HU1df+ur1z706sP/4iePwn7qdf6/Wak22tPThTZUVm4iaTW808fwsSZD3GOqB2nlSavrSGSHWXA9xjwcSrmZIlm8A8ymXof3FIOl3sc6rKIYBYiHQZ+eBqNCDb8Fc8H/1OhTmO0Fjtcqc8gdu8GKuHaL+JnCXrr/P1m/4t/FwcP3+evpDCsN3wPICevFpRaPHnTBw4qTsYy689BT8t/arZ594f/CkowcUDB02VEoaiCJVZOu2lm4gSr+UnNxeyXg8TnLkPZa7D56c+eoTPIIn6kwguhohgR480VAFfoQwzpPOBYGFEOIIU8wQLN4AFW8+CwWXXAuGQWOg7IWHoHbhYkh4mxnLa+2OLuXcu2ykSiULWYsYGPeffuZ39szME+JhifTiphuuO3wxfAdYI8aj0W82LV5wd1Xxxmfwez67YVOOKzU7naHti19K5WT06tM/vaCwb+XG9eta6+sqMQ4IdKAJ6Tnadao6fKVvSTWQSJ5JY9aaDkmvB8TKUoi2tIDn+w9AnZkLzml/BIWCJH5U0CbkoNBqQOl0ctZGaTZArKYcQptWgU6nxrjWybKsSqXQbQ8F5+bNoExGwVuybqpCATfuL3vjIenhd/L2xlvefP/rvmPGHbn3+CoeRE9vqNlcvA69vEqj09ujoWAwNTe/IBoJhyjdSe0KgqJdWZfGDlnF97c6fUUzpORsSdRNFEgsLYHOXJLNTLbUobEbIOFuhlBjE5hyC0DYtABg7Cn4twKodCpJ14mCR4UGhEQIL1gcVLZM0NkNGJACeBbNg5pn72IZUgpSBVEE8WAtWnyPsM8HjtGTWtL6DZmQiIRKb7z+2sMfw+/k7QNo9P+97MH/RQZMnDTM73a5HZnZmTQcPv/dGV/FY9H4sRddNl0KaFWM4bP69B1IPfbeluZKvcki+FytTb6W5oa0/MK+VZs2bMwdMGhgxfq1G7OK+vbCOKDS7ExxODKzcn5z1q5UI/5uAfePs8B5wnRwL/4OzIOGQ6BkPaicGaBE46eeF1PRQFDUVYEyPQNE+3TE50pWBCc5GrXNDg2zvoaad16EAfc+CxqzCZpnfwmCyQaRmmrY8vYr4G9uAp3DgQZ5kLm1KID1esE1f6HTkJ79sMXunP6bCFo7MfpZaPSkzEvtw54Hf1j6ABn80CnHRpOJRHSX6yJPT1mcKXlSHJZMmh3OVBoyyexdlKZUKVXphb31aq1Og19TBYXw2wt2WXgsAQoMZ4yDRrEqnzYrn9t3Db0HMBxRp6OnRixMwtCWtKkA8QAIBg3LeCaDXpYRFeMJUKMxmwdj2IRGTmJwvi9eA295JavwOU1qyMzqI2VsDo5v3/G0snCheVwQLdt4OjiOJDXut39TkKazY9HHH7x6xFl/vHTnn0eCAf93r770Ff5u0sH12HhjNQZJOj4ROfihEAWMNDSB7ynE9jb+RvAD17vOAkLEz7qtoDVBksYKTKkAxBTAiwH/Lh4BweiUFM+jARBVOkgEglD2ylOQMnYSOI8+HuLBECjRy6vteoh7AGrfewkqXnscVLhIqKhEI3rJpPiLEaJT2wEtxIDbCzlHHd+cOXDIyGsvv7jqN+Xhdz7evONvT7rqaw1jTzl9rK+11R3y+YKDjzz6CGovTc3L+xzh+Ii2vyUOy+bqympndk42YnrDdryfIMoQgu4KGf/HKzds2IjPzzHa7I7dZjOkqw6xxlrONQsWMphAF9X29sfYRdaETcZiEK+rAIXZBirSuk12lKYUtn8+hQoSwSB45s8Fc9+BoM3JBTHswd+omBYjmaT8eZQlKpNxEVzffgr6gl5g6oNeWiTN3CDCoCZWORQsdnDN/ASCtdVgHzEOWpYvhIqZn0EsibsDBrGiKyhrzf7Suxdef38M6n5elmLLzr4Hf3LJb9rDy8HsAPwymiCOSqMJ3zVzzkspObuK/rgb6qs/fPDe/515yz+uQ6PnFoWAx+164aZrXrvgrv+egbieuzSpR+efxx5x2bXPvnJjr2EjjugQCRP/GyhwO0+4G9i7kOJ1YvEXuL32AzE1H21MlKqR+6OPuqddRJ5rT0ZFhMcIPZZ9CuKwY0FJpxHygMKSxgaX9DagRzdzL3oMIUqyegsk37wblMddBOLYE0CIY3Tp9UDy7XsBhh8DwoTTQKnABeQJguvBq0A36SSwTL8CX9PLs6m0e7GYekMdVN93PfhLN4I+JRVioTBoLFZWLxcTiV/XAGj42+sGY5/BYB86/szr/nzpp79pg985b99//BF/Pf2mWy7I6NW7oL5sW0Xp8qWlUy64ZBp6daMoiiRka2xz0ej1g4lY7DW8aOdgLNCxlY/wiZboQ5Z99dmc/MHDeuUMG1nkWb0CmmbPhNwLrwJNigNoql6B23+koRFcX78FlhPOBW12jmRYiJHxDdq1W3fZHfaUC29bLKTyHY8hDEFPrDNA68z3QJeZC8Yh4/FPIpw5Idxd++Fb3CKbdc6l4C9eD5WfvQe5088H09BRoHA3QlypRW+cpGAeFBiANn3xNpgGjgDD8Am4gHy4cHWgDAVBNCDkSSph29P/AlCbIHPKCRBpbYGKj94E14a1nFdPJmIYA2tkhJX89W86nnciEkFHFYX+p52+NavfgHFXX3phy28S0nSWt0ejf3nLimVbTXZHH3djA+n/bEO0EjnmwkvPwh3ARJz1P7zz5jfZ/fpnDpgwabxaq+WKRf22rVvmvzdj0bQrrz3RkpKaTsWsdT/MXfjaP2656y+vvHVznkpTFPO5wbd5LYgxWTmaNK8RS6rsJrCfej5P8bR+8irYJp3IFUpBpwaVLQUDPwS9shS9RCaq2NX4qa+bCj64SJS4iyQjYUi2IKRQG8Dz3YegLhwItuOmSxibJv6jifZqaLCqDD01vp7JDGJzHTS88xLoAy1gy7gXYqkFYEgGofHle1j12nHZHZB1421A3b3BufOh9v3nIPui60ExeBIQ05d79gxomT2TjTm8Yh5CBh+u2yjYstJ5UYmgltcj/e/QYFQR9BqI+rwQLFnTO5qX82/80Y2/Cw/f0dPjFzWeU1T+9+DeI0efXzB4aFblxvV1pT8vexc9fva5d9x1fcGQob0aKyvq57z+ykelK5Z9nVXU74y+Y8YNbqwo825ctOAzfI1537z4zJ+Pv/Typ9W2TI1Sb0Fs7AbRj05EqweV2QlJv5vz1TTXGW1uBZVGCbFVc0Ez4ljGvdrcAlAZiFAoBgp7qqQ8TflqFe0CAYYh/BoYGCowHvCVbAJtaiaIa+eBomAggDUDjQ1NLSOTcbcS31MMByDhb0XoYQGl0UEMR8wC0PjCfdA86yNICirQWG1gzMoFjRiCYMU2UCqVoBh5NAj9R4PY0gCBeV/gYqkAfWYmaPD9kuEghOpq8bNiEKpWQ5zgk0oJPEi9V5n5X9fLU9gUam0Fy8BRYtrwsScitPnud2Pwu1kENAtLsKVFHiekW0gyPQX4oCa1FaRmQiRR+D2NEPpeKK5sVpnNcHm2Pev0W+98cOJRx5yujMXNaEtgGjwcYpXboPyDtyDn9D+CoU9f9M5hiZcl5MdtNsYe3bfga9AOnQThNQtAWzQCjSogTfTo0Ys3VoA6rx+ENv6E8OII8C6aBfZJU8G7fiXos/NBlYFxgTKBBu5gz08bgxiKwraXnwTLiDGQecKp4C/ZCPFAEBeKFRpmfwFbP34XRJUGjVYPMZ8PF2eIK6kqxNwI3yDpcYFKkaTZI3yOGReFBf/Oj68RwAWoBLXZwuN1YnvwLR66hr6T0ccR1tDONfD00zanFvQedu2fLw3/piHNXuAOMR54OsIf/PKT/Oj4dyTFU0bWpTGZwL95Azw1f3HtDUdN/K9tzcKBjlDLSMeoiQhbjoPothKIt9RIHOcKNRqJH7074mRrNkTduAMgRLCd+Ef27CrdZFCYUyFSvAYUOlp7CUgiJCEPqjKgl9bpwXrkiYjJdWDHryJiZTJAlcUOCoRN0SYvB82iGINEawPuCF5QOfHfVSVQ99L/QGl3QqipEXILM7g9l3rPhWy7NNWP72Y2qNnDA+RBHBdjIByTgmHK/mRaJOMWpdSnKAIcdvVm+uwKBUTcbkhWbe4LBfm340/u/N16+H1NAeoysqDyk/dg6+P/5e0dLUAbVyrW6gWxrxIDxX7/fhxMI8ci1qbADY0EMbMQT0Dr6p8h3FgPmcdOQzeCBkXpP6WOCz0QcoGo0EpGhhBFVGLwh7sCMLTxo0FbuAVAiPjYUBUaDFSXL4RISzNkHHsSPhe9stGOC0EPBKWTHi+U3HENuFYt4wqnkkr6/J7iDoZAJq/TqThXLrUYiBCNxuA3pyLKvW9JCLa6IGPcZDF37IQxGMCu+F16+C7nutG4dSlpUPHBDFj34F3sYQnDekMho0GhticFJURrmyF+z9+h6NJrwZCdC94Nq8DQfxDYJxwJ3jd+Bj/+O3vqGRhwKpl2LopBJdE+a/MLEb8TNzpxLhpAIGOkglDUz/nwWH01xF2toKO/U+MtMdjA+/NP4N28DrJOPQ9iTbXQPPNTsPQdiB5chKov3oOqJUtB7UiFWJQSN+HOByPQEBItYtvOz4dC8RslbCZoExOgevlPgiU3+8nnX31j0tWXXSz2GPzOto4QQOtMBa3DDOUz3oTSJx8Ac4oDVEYT6YRCTFRnZaXZrFqELMkcO3rdVqh79SHQ2u3gLd0M6SefBfaJR0LBdXdiQBnE52kgHsTAUmcD36z/QczdDFn/fAKNkgJTE7i+fA+ipevAedlt3JwlYOAa/O5TxPwzIev2Z0BhcYJaSEDBDbdzsUlhNUJozs9Q//pj4MVAlCgrCHsXDu4j8bzsJUVITl+NsYPJrEXvnoBAMMoe/zdp83g9aFjEW7x2osGR+nf80QM9Bt/R2JMJ0KWmg2v9Gqid/Q3Uff0ZYmkTJIg+Liy16ATD0YLGRq8G5I5a8iTJGAZ/7kaEJjZo+vgL8CvMkHPauQiBNFD+5rugScuA7HMuB9sJZ7HRJqj1QGMENWLnOAaSIUHLN4fG4AA9tnHUZO5vUaZkgQcD16a530DWKedwIalx9gwoeelxiIUwAK11s9tW4M4jNHi6zLzFlBZ0vgi9CO+CcHjEo/t14AoPLV0NMb3j9mdfeeObay+/eE2PwcswRosQJlC2Bdb87TrE3w1gyKPEjZwrl9LnaB+KbJvNsBNkkIawqJUhEbOA+9sPILp6PmgMRmhauxZMuTmQM3QwaIZORPyNNr1yAXgXfQuW6ZdBxlmXUBMjJLwhcH/yIqjMVjCecD6ICKmUjVXg+fgVqJv1NcTXLeGKpruiHEw6A2hSs/e7QUuUToRbcqhyrOAg9lcw+Y47iygetPcwqjHYL9tkChf1ehR/clxP0Cobq8psgbI3XoSyl59hY9+5XE6G4gmH7uudnfLP3XpTMvo4wgy/n72L2mwGESGNqNGAru8IbtGNbl0L4cptYBh5BKSg11ejkbsWzwX3nE9BodOBfsAoEHBniVdthlhDNX4uG8QCfjZwldEAar0OjVXcPyMhbEstAUYjZI4eE29cs1oZ9nuFX1yDgiqjsThfI0Gl4Ou/wwjCDvl+cddMzD7eW8rNa3N6Qf6UE6+57opLn/99GzxeaH16BgRra2DBBWdgwKrhi9RZ0OcOBN/UK9QX7gn2CrtkPSg3HGRMz5sFemhKOyZ8PlCplOxho2iEhOkpgk2icUuBswaUuEsI7RXZtvstHsDCph7yAOhwceceP2XTki9nVlgCwROtqU5I/GL9MAIH6oIY55iDFrJEhirrt8q7KX1WLjoI0te230nXXtjB+ju2ze28JgSGnQmOpQaeeqo7e8iwUddcfvG23xWkoQutNpk51Ui56vL33oK6778FlVa3W89JGRG7xZCRm25D4xD3cU05d4QUbHypkIhKN11FlBNym+72rV7s/p2ePGtQD3qHE/zRWEtpfc3D500Y1h9EoUCj+aW8vAiRYATMA4aB3pnKOfNoMAgxdAi0+yipQU+McypXSCaknRa/UoDOCwQfSapQy96bFgaPPAjSalDIX7nySv/R79GpRAL4HuUbbKHc7GfwqdN+Vwav1Goh1FAPns2boPzdNyBYXQkxjwd06Rl7wJQixKIJp8cTlnq+u8X+pL1bDId+sXOPBkMgWJUEvxL1ra6FxW7PGxZ/4g6bw6rkNOXBhPMMqUJgdDogbcAQ8Pj8FUt/Wr7IZrOarTa7VedI02t1Gr1BbzDpdFqzEkSVSkhqlGJSnYhEVLQoeHHgwkhEoxCPhHGhhPE1I0C0RJwUiEVxrSR23Jq5U1oJwap6CIkLTnzq+RfPu+HqK99R/V68uyE3C7a+9gJsefFJ/D6fcbEG8fZeXKoqEY2l+vzhwzqrEfWFwJCkgWht8zezPotOO2H6CwMMtmPGDO07mTxl8iB2P5JHDrv8YCws4q7N4vXr5nw788uH8FfUvWrDh17OAqDB63EN2ExOZ4rNYrHazFarWa/Xaw0Go95ms1kMKUaTSqHUqRUCRTV6GnMREgldIhah+4TGH+ddI+IPSIsiGgHRooCmzSVg71349POvvrHkd2DwAhiycqBx3g/gXroA7H2KOK3XlSMQiTrT0yxGvOwIaZKH52LHBZ0IacCRnw5l0UhtfXUj/Pj2o3V/uvm/D+cXpvVDz5mm7gZGr91mZXAxRR06sPftC8Fg0FtTXfXpU888X7Lzn9Hb33Dd1cr6upC2vq6O+praHlp5QVhoUdC/jUYj7g42XhQardaIX012h8NksVqNBtorHClGK/5crVYZVQqFTojF9fhH9nAg8Mhv2uAJC+qysqFh6SJY+X9/4T52jc0BYrBr+p/ecDg3EUvqiEnrsE1L40KN+P3QPw9hlCpW2eSNQZYzDWoaW2Z9vXTdBxmgud6RajsomULy7hGvH5x5WZCbkg51jY1lc+d8v+iMM//Q6d/jQiCF9mAymQh2hJAq0njtkDWghREIBLS1NTW0GNTygjDKD7381YCLwazVaA06vc6cnp6RW1RU5PmNGbwoRfe4TVO3o9pqAfeSBbD57ttAr8BAMSNdvqtdO+1gMpZntxm0SpqjPGwNPgEJNAu9yYDGF6yiYLmmqhG+euKO6Ck33fv4tCGDR1ks+gkHg6WEXjGUDIOjdxEo9XqoKi/7EY36gPn3aGHQ7aHHnshUo5GIgh4+n1fV1NhoXr9urfW3Y/ByWo+yMImgH8qefwxibjdTUmgU1FOets+TOtqoKsdo0CiJDEg8TC2eagSCRgEqrSYUi3oacJuHlioX1GxdB0/efMXW17+Z+/QIW84AnydgMxr1IHbXicoTSdpUO8KpfAj4/J4538+eMWbsuF90vcsP4tmmluGm34zBE+2ELi0dtrz2PFS+PwMSfi8PM6usNqAuR9EV2Nf7BV5fOKMq7ILDmaMpQWOCGCzqoslANBxqFBJRDNoLmR+ytb4KVhZv+1IvKo41h4XLHCm27jN4sjCPB9L79QXE1LBlc+n6mprqVb/29Ti8DR5vjsbuYK9OFcvy116Aipee4uKN2oa4lMiB2m6gct/G0ghD2q2GnPx0e3uR5LA0+GgE1EYjKHVq37atWxvGj58AapMOGmqbwFNWDVcdc5TvhbnzH7lx+tQJejE5QCF0H+VIyKsCW98+QJ2mm0s2fYlQJNZj8PuZZiRMqLZYYdOj93NOnby46+fFrAtKrb4c8xxAZoUMPB5L5ri9oW71er/0Qflrk9oIFkHpWb5sqfuCCy9iaZrMoSMg6nHh70Nw8yknbJqzoeTJUWlp92tVaptSrTzgc6bmO43RANbcfHA1NdZVV1d/uadr3WPwnWRcVCaT5NERvlBbQMtPi6BxwRzwl23l2VG1zQEJBZ5SvHtK5ol4wu7xhOBw7qalaqPSrgB9Umh95rkXEiz1jrDWlJ4Bbm8YfI1NoNb5YG1p+TuCLzZxaF72hRCKARxQOwP1snggLSUTNGYLlKxa9dOqlSu2wuV//tWvh4qw76HowdVo3DTJ0xZoqkxG8KxbC5XvvA76zGxwrVoOtd98Dvr0TM6t7+gmDnxbTiSSlhSHyZbqMB+2OXhKk8QDOrDkp0JzOFQTQ0fQVp6n6mckHIZYLA6iIgZTMgu88+rKH542ZfjYZCDST61S7ve8Hz0tbNOAY9BASsMnGhsbZyGciRwKl0RFtBCH2kFQpezdN6F52VKewGcIrjeAf1spuFb8xIUjGtBQp2VBlLx5vPsN0h0KpelDKovPG4bD9kDLC7u8kO/Mg4QuXquUG+Q4CMeYhnQkIhhYUkOb1WKEP2WOWvfud0ueG52Zc79Op9UrOB27jz1E+B5RXxDMGADnZmRCS0tTxScff/jVlGOOPSQuiWr1bdcfcveJCDuJ5oJIMxVyGytDGoQtlqK+Hdp4Raap6P5YmG6zmJLiNBuVe1GPPuQxPF4+s80CtaFwfcfuSNJL6nfMFKhZNI8hIi2CeDwOFStb3+yVlnr08NTU6QkSBN5HL0+LJCjGcFcpABU6qcbNpSvRu1cfKtdDFaivPySzLwq8Idq0zF0iGu6pPsgHZWiSopii06m1ZPDd1Tj2y19GETSCFrQmQzRU56rquHAj0TjYnDYYOHU0JITtsHbqtAmui+58/KGBhVlDMXTthZ6+6wuehsWjUTCadZCGMDMeT4YQu38wYMDAQ+aaqBRa7aF7w34lSjeeghMht7q6FUgf+XANWqkpjLoINQOFSDAUKFert7cDkzRCXNCDS2EBY8TFqcO2PIbHH/xp1vL1r/Yx2e+0283aLm9wBKE8XrBlZ0OhE+OGhobyJYsXzT7v/Av3Cf8fVINXKhXQc+wSsIJer8nvW5BBnv4wzcELTOdN5E8Gmym8buaa8smTdhRRIe5JdVIJhkhImq8VJfKlGXfdlLzg30+8Mqx33rF98zOmUGDb6YDMru+IBq8BA8JOpVYPxZs2fItwxr2vn1sagDk4wymqwzYDcZChQCyWzGpo9B2++J1K++Ew6EgbKZZ0by4pdu1M0aEWkhBT6iEk6EATj6KXV2D8L2VyPrj/lvrrHnrpYZteV5QMx3PUXRgWod50cqDp2XkQCQYRLdd9sX87rIKnsTiF2s0uX6VUKnssvLMLE49nRCKHr+4xTf1EQ1FQmhBmRGONz774cjK5s3S7mAB1aiZsXLsWmjeuBY3JvMOvm1ze2fPWFL8+oVev2wUhIST3sPjbRgnNObmgs6dAaenmVT/O/2HF2X88d/+uPwbSsVj3X3+Vyajpse6dPZUoqtR6ZWq63XTYBqzkGWMBNRizUsEjJptI3bqzmEiDRt9U1wxlW2pAb7Xu8LspGfnxefUVz55/5jHH+avqxxuMxt1ibC42GRVgGdSP+5pamprmIpzZ775qhTzk3d07rIqIeXqOHQ93MKhXiILd5z58c/ACC4D5INOQBgprvIGzTZ3k1Akva9UKMJn1aPDGHbJiWr0GrhxwZN3T7319/xHZec+ZDcEsJn/qBD7FwxHQG3SQlZkLXo+nbv78eZ+MnzDxgGAlefl4vHuzcioqOvQcu3grq9NqtOrUKjh8U/Do4XUA9lQbNITDVYl4olNl7LDfD/njxkE86IU4Cz2odlw04Si4/IFvasOBj47slXkDEd4zZUgHpgYKjEPuBOgRzqiNZqjeunVjdVXVhgPPlin40Z1Yvido3cWz8P+y9BqVRUP0eodrDp5MMq4Ag9UCkRZ/hTS3uuu5xCNRMKWnw3mXTwdFIg6JTohWL77m7NgxF93y1NghReOVwfBYo9ko8eTI3p3pCrUqSCsqInWSWEnxxq8QzsQP/F6I0N0xpioajfdY+c5eLxLLqW/0KQ7fDA3R6iW5E9I0lBR+IuUqtXL3dCSxOPgFA+Q4xE4bTMm7rvn6lS0nnHfLowNMtmfSU50prKYnU4xEfQEw2mxQkJIOfo+7YeZXX3504rSTD81kBHmxnmNHr6LTqfNzsx1w2DIwMhFRDCBhAp3J4HWVbqkS97BTUStSlTsJKaoYdPZn3HaA2L3e7f4y12Y5dmKftCvDwQhQGzERLIU9GAP06Qcasw3WLl644FBqJdjF4GOxRI+V73TEYsmClhb/YR2wUh+8EuMzQyLhMeh17j3RZGuolybshdJVGyGp0u52oT9/659DVz/y0mMDCrInRN2hISazgVs9FAiXMrJyWAC6vKzso5GjRh+y10alVvcErbsYQEyZ2UYdcbjidwpQlRo9ROKJ1k8+/CBw9FFH7/4J1Duk0IBfYQSF390ul7lrhkMJ795zU8lfH339fyNTM5+w2szmqD8A6pRUMKRlQXX5tk3z5s354cw/nH3oGrzQ01mw473Hm282a1NzMuwQP4wD+kRIDcYUysGLTY89+Vx8z+m9OEQFLaRl5EK2qIUwd9rsJnOC0KbO7fkwY/Kokxxq9R+SegBjnyIQVGqoq62dh3Cm9VC+LqpIpCdo3fnwe8NpQU/0sJ50Cnv9kFpkAKNTrNPpNJCM79mzCaICBKUGTNY0sKp3n52iGGfDdy/7B0294oExzoy+RXmZQ4dPzoN4NOopK9v21egxYw/p66JSKnpc/E43VJeWYnaaDbqDmpLcm1TxAcchZhXYs1OgKRarjZGuk7jn3UoJCWgVDLB2cy3ofA2I5TV7wMEqePrGS1c8PuPzV6yZaf/Wmi2OhsambT/O/2Hp/rYS/GIGnxR78vA7ePdwOFOnN+vknvhutnCp1TgRjkAyFmXBA4ICklRkN74Vf/YkaMwmiASjFYTnhS58PJ1ODbVbysG/rQRUJutug1epCqqELa2t77oWLdFeOXL8dV6PZx3CGc+hfn9VvzlFtwM84kkx3+MJ6zzUVtBdl0aQOwAjMYh4vWDOSAd7Xi+oLy7mLIfaaOhWQlOi14v6/WAaJCYEQVHXVU0nKiD9P3vX/hTVeYbfc9k9uwu7y3Jd7lgVZVBovSCOrYlJJnZMjAGB0P+gUcH+1stk0hE70/anjCCSNtNkmqqJAglyt4BFE6FEKoIiURBQiCzLctv7ObvnnH7fWVGT1nCvLD3fDMPM7s6Zc3af7/2e9/a8AeERAFNmaSPOxune2ffi2O9qm4veP1VkVTGq3t+88+6K/33pEINGRvnTgCf5uHhjCENjtbFFUxqMdEKy4FjfkSIZUG5PAW3iZtCGGyEoKlKc7mwlNFqNVD+ONdGXBvA8iB4taPQa59DIpGWuCTTe7QLNumRwDw9I6m1YyW22c+EPv/6l+1e//+NH4CdJC3ran5uUl2HZ7GyUyTT9eBj1gheu9kNAw03yyKEDXVQkxO9IB31CIkxPTk+Unz1dr9EGKrb9aOfB/uZLJB5fSalUElgXHaFBnB3fflSiYJ+anBgl5+GnERQFgeuTYaqjxVe/MttmQe+f+NNfONLrH6XUcljyu84bRRh9gzmIBVMaHLrD49Cx1BxuPDduTYfIzamgDAiEm9c72k5//FHp5OQkFiZSUjk/ozak787sa2wADZYnwfQGKzEsIkSEW9ApZJ15IK01lRWmva/unTvgMfePiAFKZwDB5ZCEaVfTokkZ8U+oAI7BB6hiYqMNgA3tvGtp8FeJrsHa7ECqlRC8Yzco4zeCKjgMTEND/VWn/9pw/V/tF9CnmgpPlnCYt/8i/3BBRvZbsPXN/RnWGy3I0NNok+gX1c8reFipmcPD0JPvFRa7JfWBuZ4OvAco5LBq8EnU/gXQuiDw45LR/wS82+2Rkf40pXGzxrtO8/yYOulTF+ZdTmkWqh7Rl1hEX3SJG8Bpd0xXlZ6vr62trsZALyp+f8Q3J8AHIgT8zvwjbx8TD2bzKUnbs3obGwlBtEh6kAulN3jETVA0BfpIQVILnu/mIdCG0UQnAGcaBtb8EEhGtXoAL5cWfMvCk0E6tcEw14kfeKAW4scelgWv3QEQagDt+o0Qk7oFSKXK29F2rbWs7NOaifHxSgT0nmddBoG+C4G+gMzJpTb9dF/mg+YGCFQToECWVliAbKDHRYLeaACXKH4jDSWeb64FUyssacgoQeBYIFXqVWPl6e8rKvq/i9AIvIEmaf2swZmZiXGCKMlS4Dh3ZMpmCE/dBqDWwdDgYE/puU/qeu/eqUGfvjqbzBy+3InCk7eO5h8pgJxcSH99f8b9pnoCUyPM+8X5RG+wn4k2q4JhwCGIpoXOdhXQaaUyxoLb9FBSP1gtXF6uh39qTTidxgCFQuu0ueFZHishTbQWwOtwSJRDFxsNCTt3QVDcGpiwWS2XKs7UNzU2YIe0cV51JQhkM/TGm5HFp6a9kHWntpYQzFZgtFoptj7Hy6BNaAMmXuDdDPtwodliASsexK4FxWAvcOPm1QN4jVpu4n4UXQMF5w6Niw7Gw7D+61xWSYQU0RfObkNgiEWgSoTgxCSgGY27vbXly/LSc9U2m7UGAb1vofdxoqi462je4QLFW7lU6oE3MsfaLgPDUIjTBz6SGJw9ZyqEa0AXYeC6x2zDi+kJJdCzqn+wEdjRhwB4ENwqoDW00yU7rTOL5fjQMYuD+lYAfmYKNHoJH/N4YfoSu2MnUGot9H7d03mhvKyur68XO6VfLV70n0CgP3XraN6hY0J2jpi2+5XMu9VVBD9qlcbQz5YMw5El3PyhSCTdExNjA4vpWcYBPDrUCGxMArhHhuY8/XBFA56QKfxjoDAKKl7FUE8MGenLtuLmZqzxwoQZIWbbNghZsw7Gx8eHG8+VNl253Pw5+uTl+Stsff8qKi7pyjv89jHPm5neLXtezelrqCNoFtEMnVbi6M9+DgFEnCWmFc6LtbVDr+3bv/DvxOMFWqsHddxacA0Prg5KQ8iIn4nQ4IKouNAQrEXjI8OYvrit06CLCQNDUipQEfE43e78quXLK2dPf1zG8zzWXhlYnjvClr74pkRvsnOotAMHssbb/oHvFJQGLTyj+RR4bN0ZJa6DtbxXVOJarMyFd3oSqJBIUCHQs0P3gFT5dykK7fXKLX6PnT2XJ+rOPbOUPBIR2PG4yvBNyaDdmgZMcAjf29N9/ULF5xcH+u/h6EsbAvuyk1rkyN7GIUsxKwe2pr2c2VNdSbIDFmlgxHerLKXWPqcbNIYgMCaARfJFlqDEBZc8SErxPP9kirC/Al6vl4vHfK4eYjAMFRGkxFaSB0IfBeEpWyBs3Xowm0wPPvvwg4v/bG2pAF+Y8X9aBltYdOpmft6hAj4j05v+xoGcB80NpIrkgdHrfKAXfSeCD/AOUIeGgksURvFr5FL0O3BuCFibDJzZhGgO56uk9FfAe1hZecwHeEILLjaBCgyCuF07QB2DeCvrtf29uqaxvOwcFgXFWdKh53X8IEt/U0pOURT14r7Xswcba8FumQJGp0MUjH9s4TnWA2pKCR6OG/E1fSzeGou8B5TBoaAyRvs9l6dtVoeMdrTO3uhkM9PSzMkv7Y9TaHXQ093d9smZv1WOjY3VIqDfWAn3iLO1eYd/XiB4eXF7+ksHeyorKa952GfpH2nEYHk9ykiAm2XvS9qMS1LU76MxAes3gXOo3687KOiN86ikW83rt3v2cP2m0XZSycR/UFx4vqurE/N03JS8ouqnH4UsCwRB8P44MyPX3NpEKkQelMjS43Jkr1MF+vgI8daY/T7HcUvHtxF/Jxg1qNcmgetej9+GKOmwxCQZ7T46QDS2f1hZV/Bur8ViOYOAPrpS/TME+m4E+uM0Wj95YW/O3ZoL4DZ/A7Q6ANwOK8Sv8TqsVusgSVJL9wC4rS9AK9X3OFxO5O+o/TRKw8kcfuYnvfrFlavo/6WVMmJxFnrztURvBBHSX3ntYE9VBcVOT4FaqwFQKO3I3I8SeIrGErYpCqxdmpOrDDOC12HzT8CvplrnJQCR1Z/ut/Dkqe78I4eOebwez8tZ2bmWliZKcEyBSqOacjld9iUfZ4SdV2MUsFFxYL19A/wxhyN3f/g3EZPi9PXVlcevXGsv1f1wl2h2UjA5aRtvqK9zCQIBS/onUsAhpxjX11CBWhC8nAx4eT0PS19yp+qz8uMtnZ3nU7JykHOpspws+fPy6K9gCWulCgLWbHg0fEwGvLyejyN7u6ayouBaR0e1QBAjIr+8GXRcX4OneYt+prD8bwEGANiuWKEVQOIkAAAAAElFTkSuQmCC";

const weekly = {
  "directory_items": [{
    "id": 61887,
    "likes_received": 52,
    "likes_given": 6,
    "topics_entered": 246,
    "topic_count": 2,
    "post_count": 129,
    "posts_read": 1062,
    "days_visited": 8,
    "user": {
      "id": 61887,
      "username": "randelldawson",
      "name": "Randell Dawson",
      "avatar_template": "/forum/user_avatar/www.freecodecamp.org/randelldawson/{size}/56290_1.png",
      "title": null } },

  {
    "id": 90178,
    "likes_received": 29,
    "likes_given": 0,
    "topics_entered": 2,
    "topic_count": 0,
    "post_count": 0,
    "posts_read": 21,
    "days_visited": 4,
    "user": {
      "id": 90178,
      "username": "cndragn",
      "name": "Candice",
      "avatar_template": "/forum/user_avatar/www.freecodecamp.org/cndragn/{size}/48701_1.png",
      "title": null } },

  {
    "id": 6,
    "likes_received": 25,
    "likes_given": 8,
    "topics_entered": 18,
    "topic_count": 3,
    "post_count": 2,
    "posts_read": 71,
    "days_visited": 8,
    "user": {
      "id": 6,
      "username": "QuincyLarson",
      "name": "Quincy Larson",
      "avatar_template": "/forum/user_avatar/www.freecodecamp.org/quincylarson/{size}/26_1.png",
      "title": "" } },

  {
    "id": 86405,
    "likes_received": 19,
    "likes_given": 0,
    "topics_entered": 152,
    "topic_count": 1,
    "post_count": 72,
    "posts_read": 426,
    "days_visited": 7,
    "user": {
      "id": 86405,
      "username": "shimphillip",
      "name": "Phil",
      "avatar_template": "/forum/user_avatar/www.freecodecamp.org/shimphillip/{size}/79619_1.png",
      "title": "" } },

  {
    "id": 163793,
    "likes_received": 18,
    "likes_given": 2,
    "topics_entered": 113,
    "topic_count": 0,
    "post_count": 58,
    "posts_read": 386,
    "days_visited": 6,
    "user": {
      "id": 163793,
      "username": "clevious",
      "name": "K. Sid Ahmed",
      "avatar_template": "/forum/user_avatar/www.freecodecamp.org/clevious/{size}/101718_1.png",
      "title": "" } },

  {
    "id": 75682,
    "likes_received": 18,
    "likes_given": 0,
    "topics_entered": 2,
    "topic_count": 0,
    "post_count": 0,
    "posts_read": 8,
    "days_visited": 3,
    "user": {
      "id": 75682,
      "username": "kevinSmith",
      "name": "Kevin Smith",
      "avatar_template": "/forum/user_avatar/www.freecodecamp.org/kevinsmith/{size}/72934_1.png",
      "title": "passes a Turing test 58.3% of the time" } },

  {
    "id": 2193,
    "likes_received": 16,
    "likes_given": 10,
    "topics_entered": 129,
    "topic_count": 0,
    "post_count": 38,
    "posts_read": 397,
    "days_visited": 8,
    "user": {
      "id": 2193,
      "username": "DanCouper",
      "name": "Dan Couper",
      "avatar_template": "/forum/user_avatar/www.freecodecamp.org/dancouper/{size}/1479_1.png",
      "title": null } },

  {
    "id": 149310,
    "likes_received": 15,
    "likes_given": 3,
    "topics_entered": 245,
    "topic_count": 1,
    "post_count": 40,
    "posts_read": 1040,
    "days_visited": 8,
    "user": {
      "id": 149310,
      "username": "aditya_p",
      "name": "Aditya",
      "avatar_template": "/forum/user_avatar/www.freecodecamp.org/aditya_p/{size}/99447_1.png",
      "title": "" } },

  {
    "id": 87405,
    "likes_received": 14,
    "likes_given": 1,
    "topics_entered": 2,
    "topic_count": 0,
    "post_count": 0,
    "posts_read": 16,
    "days_visited": 2,
    "user": {
      "id": 87405,
      "username": "Pagey",
      "name": "Dan",
      "avatar_template": "/forum/user_avatar/www.freecodecamp.org/pagey/{size}/86707_1.png",
      "title": null } },

  {
    "id": 746,
    "likes_received": 10,
    "likes_given": 4,
    "topics_entered": 147,
    "topic_count": 0,
    "post_count": 51,
    "posts_read": 332,
    "days_visited": 8,
    "user": {
      "id": 746,
      "username": "ArielLeslie",
      "name": "Ariel",
      "avatar_template": "/forum/user_avatar/www.freecodecamp.org/arielleslie/{size}/44939_1.png",
      "title": "(totally not a bot)" } },

  {
    "id": 163951,
    "likes_received": 10,
    "likes_given": 0,
    "topics_entered": 73,
    "topic_count": 0,
    "post_count": 35,
    "posts_read": 413,
    "days_visited": 5,
    "user": {
      "id": 163951,
      "username": "lasjorg",
      "name": "Lasjorg",
      "avatar_template": "/forum/user_avatar/www.freecodecamp.org/lasjorg/{size}/96853_1.png",
      "title": null } },

  {
    "id": 168749,
    "likes_received": 9,
    "likes_given": 3,
    "topics_entered": 127,
    "topic_count": 1,
    "post_count": 32,
    "posts_read": 784,
    "days_visited": 6,
    "user": {
      "id": 168749,
      "username": "ezfuse",
      "name": "Geno",
      "avatar_template": "/forum/user_avatar/www.freecodecamp.org/ezfuse/{size}/102040_1.png",
      "title": null } },

  {
    "id": 120,
    "likes_received": 8,
    "likes_given": 4,
    "topics_entered": 65,
    "topic_count": 2,
    "post_count": 17,
    "posts_read": 178,
    "days_visited": 7,
    "user": {
      "id": 120,
      "username": "JacksonBates",
      "name": "Jackson Bates",
      "avatar_template": "/forum/user_avatar/www.freecodecamp.org/jacksonbates/{size}/66729_1.png",
      "title": "Code Eye for the Newb Guy" } },

  {
    "id": 139189,
    "likes_received": 8,
    "likes_given": 9,
    "topics_entered": 36,
    "topic_count": 0,
    "post_count": 13,
    "posts_read": 142,
    "days_visited": 8,
    "user": {
      "id": 139189,
      "username": "snowmonkey",
      "name": "Tobias Parent",
      "avatar_template": "/forum/user_avatar/www.freecodecamp.org/snowmonkey/{size}/79328_1.png",
      "title": null } },

  {
    "id": 142107,
    "likes_received": 8,
    "likes_given": 6,
    "topics_entered": 52,
    "topic_count": 0,
    "post_count": 21,
    "posts_read": 180,
    "days_visited": 6,
    "user": {
      "id": 142107,
      "username": "vipatron",
      "name": "Vip",
      "avatar_template": "/forum/user_avatar/www.freecodecamp.org/vipatron/{size}/81984_1.png",
      "title": null } },

  {
    "id": 170444,
    "likes_received": 7,
    "likes_given": 0,
    "topics_entered": 59,
    "topic_count": 0,
    "post_count": 7,
    "posts_read": 258,
    "days_visited": 3,
    "user": {
      "id": 170444,
      "username": "Exolent",
      "name": "",
      "avatar_template": "/forum/user_avatar/www.freecodecamp.org/exolent/{size}/101696_1.png",
      "title": null } },

  {
    "id": 167047,
    "likes_received": 6,
    "likes_given": 7,
    "topics_entered": 48,
    "topic_count": 0,
    "post_count": 22,
    "posts_read": 208,
    "days_visited": 8,
    "user": {
      "id": 167047,
      "username": "yoelvis",
      "name": "Yoelvis Jiménez",
      "avatar_template": "/forum/user_avatar/www.freecodecamp.org/yoelvis/{size}/99065_1.png",
      "title": null } },

  {
    "id": 143165,
    "likes_received": 6,
    "likes_given": 5,
    "topics_entered": 35,
    "topic_count": 0,
    "post_count": 15,
    "posts_read": 174,
    "days_visited": 7,
    "user": {
      "id": 143165,
      "username": "SpaniardDev",
      "name": "Yago",
      "avatar_template": "/forum/user_avatar/www.freecodecamp.org/spaniarddev/{size}/89850_1.png",
      "title": null } },

  {
    "id": 148503,
    "likes_received": 6,
    "likes_given": 2,
    "topics_entered": 12,
    "topic_count": 1,
    "post_count": 10,
    "posts_read": 74,
    "days_visited": 7,
    "user": {
      "id": 148503,
      "username": "HooriaHic",
      "name": "Hic",
      "avatar_template": "/forum/user_avatar/www.freecodecamp.org/hooriahic/{size}/94417_1.png",
      "title": null } },

  {
    "id": 157144,
    "likes_received": 5,
    "likes_given": 0,
    "topics_entered": 5,
    "topic_count": 0,
    "post_count": 1,
    "posts_read": 22,
    "days_visited": 4,
    "user": {
      "id": 157144,
      "username": "Irys13",
      "name": "Eva",
      "avatar_template": "/forum/user_avatar/www.freecodecamp.org/irys13/{size}/91344_1.png",
      "title": null } },

  {
    "id": 147554,
    "likes_received": 5,
    "likes_given": 4,
    "topics_entered": 89,
    "topic_count": 0,
    "post_count": 15,
    "posts_read": 316,
    "days_visited": 8,
    "user": {
      "id": 147554,
      "username": "alhazen1",
      "name": "D Wilder",
      "avatar_template": "/forum/user_avatar/www.freecodecamp.org/alhazen1/{size}/85244_1.png",
      "title": null } },

  {
    "id": 22,
    "likes_received": 5,
    "likes_given": 1,
    "topics_entered": 12,
    "topic_count": 0,
    "post_count": 1,
    "posts_read": 38,
    "days_visited": 5,
    "user": {
      "id": 22,
      "username": "raisedadead",
      "name": "Mrugesh Mohapatra",
      "avatar_template": "/forum/user_avatar/www.freecodecamp.org/raisedadead/{size}/61322_1.png",
      "title": "" } },

  {
    "id": 168662,
    "likes_received": 5,
    "likes_given": 7,
    "topics_entered": 39,
    "topic_count": 2,
    "post_count": 8,
    "posts_read": 171,
    "days_visited": 7,
    "user": {
      "id": 168662,
      "username": "zebedeeLewis",
      "name": "Zebedee",
      "avatar_template": "/forum/user_avatar/www.freecodecamp.org/zebedeelewis/{size}/101076_1.png",
      "title": null } },

  {
    "id": 63116,
    "likes_received": 4,
    "likes_given": 3,
    "topics_entered": 16,
    "topic_count": 0,
    "post_count": 7,
    "posts_read": 130,
    "days_visited": 5,
    "user": {
      "id": 63116,
      "username": "Ducky",
      "name": "Chris",
      "avatar_template": "/forum/user_avatar/www.freecodecamp.org/ducky/{size}/73986_1.png",
      "title": null } },

  {
    "id": 143943,
    "likes_received": 4,
    "likes_given": 3,
    "topics_entered": 19,
    "topic_count": 1,
    "post_count": 4,
    "posts_read": 65,
    "days_visited": 6,
    "user": {
      "id": 143943,
      "username": "Lucasl",
      "name": "Lucas Lombardo",
      "avatar_template": "/forum/user_avatar/www.freecodecamp.org/lucasl/{size}/93365_1.png",
      "title": null } },

  {
    "id": 4257,
    "likes_received": 4,
    "likes_given": 0,
    "topics_entered": 21,
    "topic_count": 1,
    "post_count": 7,
    "posts_read": 51,
    "days_visited": 3,
    "user": {
      "id": 4257,
      "username": "Hassanbhb",
      "name": "Hassan Ben Haj Bouih",
      "avatar_template": "/forum/user_avatar/www.freecodecamp.org/hassanbhb/{size}/46791_1.png",
      "title": null } },

  {
    "id": 170643,
    "likes_received": 4,
    "likes_given": 9,
    "topics_entered": 34,
    "topic_count": 2,
    "post_count": 6,
    "posts_read": 175,
    "days_visited": 5,
    "user": {
      "id": 170643,
      "username": "mehdielazzouzi",
      "name": "mehdielazzouzi",
      "avatar_template": "/forum/user_avatar/www.freecodecamp.org/mehdielazzouzi/{size}/101845_1.png",
      "title": null } },

  {
    "id": 5653,
    "likes_received": 4,
    "likes_given": 0,
    "topics_entered": 3,
    "topic_count": 1,
    "post_count": 0,
    "posts_read": 31,
    "days_visited": 1,
    "user": {
      "id": 5653,
      "username": "taronjamal",
      "name": "",
      "avatar_template": "/forum/letter_avatar_proxy/v2/letter/t/85f322/{size}.png",
      "title": "" } },

  {
    "id": 16065,
    "likes_received": 4,
    "likes_given": 0,
    "topics_entered": 17,
    "topic_count": 0,
    "post_count": 3,
    "posts_read": 122,
    "days_visited": 7,
    "user": {
      "id": 16065,
      "username": "astv99",
      "name": "Steve",
      "avatar_template": "/forum/user_avatar/www.freecodecamp.org/astv99/{size}/66820_1.png",
      "title": null } },

  {
    "id": 112225,
    "likes_received": 4,
    "likes_given": 0,
    "topics_entered": 19,
    "topic_count": 1,
    "post_count": 6,
    "posts_read": 59,
    "days_visited": 8,
    "user": {
      "id": 112225,
      "username": "LukeAyres",
      "name": "Luke Ayres",
      "avatar_template": "/forum/letter_avatar_proxy/v2/letter/l/5f9b8f/{size}.png",
      "title": null } },

  {
    "id": 66726,
    "likes_received": 4,
    "likes_given": 6,
    "topics_entered": 16,
    "topic_count": 1,
    "post_count": 2,
    "posts_read": 79,
    "days_visited": 5,
    "user": {
      "id": 66726,
      "username": "faissalabsml",
      "name": "Faissal Absml",
      "avatar_template": "/forum/user_avatar/www.freecodecamp.org/faissalabsml/{size}/95568_1.png",
      "title": null } },

  {
    "id": 86803,
    "likes_received": 4,
    "likes_given": 3,
    "topics_entered": 47,
    "topic_count": 0,
    "post_count": 7,
    "posts_read": 208,
    "days_visited": 7,
    "user": {
      "id": 86803,
      "username": "moT01",
      "name": "Tom M",
      "avatar_template": "/forum/user_avatar/www.freecodecamp.org/mot01/{size}/67262_1.png",
      "title": null } },

  {
    "id": 148076,
    "likes_received": 4,
    "likes_given": 14,
    "topics_entered": 84,
    "topic_count": 0,
    "post_count": 11,
    "posts_read": 371,
    "days_visited": 8,
    "user": {
      "id": 148076,
      "username": "ridafatima15h1",
      "name": "rida",
      "avatar_template": "/forum/user_avatar/www.freecodecamp.org/ridafatima15h1/{size}/101179_1.png",
      "title": "" } },

  {
    "id": 84340,
    "likes_received": 3,
    "likes_given": 0,
    "topics_entered": 0,
    "topic_count": 0,
    "post_count": 0,
    "posts_read": 0,
    "days_visited": 1,
    "user": {
      "id": 84340,
      "username": "owel",
      "name": "Owel",
      "avatar_template": "/forum/user_avatar/www.freecodecamp.org/owel/{size}/48425_1.png",
      "title": null } },

  {
    "id": 167785,
    "likes_received": 3,
    "likes_given": 1,
    "topics_entered": 56,
    "topic_count": 1,
    "post_count": 4,
    "posts_read": 257,
    "days_visited": 8,
    "user": {
      "id": 167785,
      "username": "kickflips",
      "name": "Sam",
      "avatar_template": "/forum/letter_avatar_proxy/v2/letter/k/7ba0ec/{size}.png",
      "title": null } },

  {
    "id": 34727,
    "likes_received": 3,
    "likes_given": 1,
    "topics_entered": 16,
    "topic_count": 0,
    "post_count": 2,
    "posts_read": 54,
    "days_visited": 2,
    "user": {
      "id": 34727,
      "username": "EddieCornelious",
      "name": "Eddie Cornelious",
      "avatar_template": "/forum/user_avatar/www.freecodecamp.org/eddiecornelious/{size}/79862_1.png",
      "title": null } },

  {
    "id": 106001,
    "likes_received": 3,
    "likes_given": 0,
    "topics_entered": 15,
    "topic_count": 0,
    "post_count": 3,
    "posts_read": 54,
    "days_visited": 5,
    "user": {
      "id": 106001,
      "username": "bradtaniguchi",
      "name": "Brad",
      "avatar_template": "/forum/user_avatar/www.freecodecamp.org/bradtaniguchi/{size}/57458_1.png",
      "title": null } },

  {
    "id": 129741,
    "likes_received": 3,
    "likes_given": 2,
    "topics_entered": 12,
    "topic_count": 3,
    "post_count": 3,
    "posts_read": 40,
    "days_visited": 4,
    "user": {
      "id": 129741,
      "username": "dk34",
      "name": "leaflove",
      "avatar_template": "/forum/letter_avatar_proxy/v2/letter/d/ac91a4/{size}.png",
      "title": null } },

  {
    "id": 148939,
    "likes_received": 3,
    "likes_given": 2,
    "topics_entered": 4,
    "topic_count": 0,
    "post_count": 3,
    "posts_read": 19,
    "days_visited": 3,
    "user": {
      "id": 148939,
      "username": "zivaev",
      "name": "Zafar Ivaev",
      "avatar_template": "/forum/user_avatar/www.freecodecamp.org/zivaev/{size}/85800_1.png",
      "title": null } },

  {
    "id": 139468,
    "likes_received": 3,
    "likes_given": 5,
    "topics_entered": 70,
    "topic_count": 0,
    "post_count": 5,
    "posts_read": 251,
    "days_visited": 7,
    "user": {
      "id": 139468,
      "username": "Layer",
      "name": "Crick Cracker",
      "avatar_template": "/forum/user_avatar/www.freecodecamp.org/layer/{size}/82898_1.png",
      "title": "" } },

  {
    "id": 146027,
    "likes_received": 3,
    "likes_given": 5,
    "topics_entered": 21,
    "topic_count": 1,
    "post_count": 6,
    "posts_read": 82,
    "days_visited": 7,
    "user": {
      "id": 146027,
      "username": "mslilafowler",
      "name": "Asma",
      "avatar_template": "/forum/letter_avatar_proxy/v2/letter/m/8dc957/{size}.png",
      "title": null } },

  {
    "id": 16648,
    "likes_received": 3,
    "likes_given": 0,
    "topics_entered": 0,
    "topic_count": 0,
    "post_count": 0,
    "posts_read": 0,
    "days_visited": 0,
    "user": {
      "id": 16648,
      "username": "IsaacAbrahamson",
      "name": "Isaac Abrahamson",
      "avatar_template": "/forum/user_avatar/www.freecodecamp.org/isaacabrahamson/{size}/30882_1.png",
      "title": "" } },

  {
    "id": 160777,
    "likes_received": 3,
    "likes_given": 5,
    "topics_entered": 21,
    "topic_count": 2,
    "post_count": 8,
    "posts_read": 81,
    "days_visited": 8,
    "user": {
      "id": 160777,
      "username": "michaelnicol",
      "name": "Michael Nicol",
      "avatar_template": "/forum/user_avatar/www.freecodecamp.org/michaelnicol/{size}/102004_1.png",
      "title": null } },

  {
    "id": 3509,
    "likes_received": 3,
    "likes_given": 1,
    "topics_entered": 20,
    "topic_count": 0,
    "post_count": 14,
    "posts_read": 100,
    "days_visited": 4,
    "user": {
      "id": 3509,
      "username": "KoniKodes",
      "name": "Toni Shortsleeve",
      "avatar_template": "/forum/user_avatar/www.freecodecamp.org/konikodes/{size}/2470_1.png",
      "title": null } },

  {
    "id": 165230,
    "likes_received": 3,
    "likes_given": 3,
    "topics_entered": 36,
    "topic_count": 7,
    "post_count": 6,
    "posts_read": 168,
    "days_visited": 4,
    "user": {
      "id": 165230,
      "username": "ian",
      "name": "OKC",
      "avatar_template": "/forum/letter_avatar_proxy/v2/letter/i/bb73d2/{size}.png",
      "title": null } },

  {
    "id": 151872,
    "likes_received": 3,
    "likes_given": 0,
    "topics_entered": 52,
    "topic_count": 0,
    "post_count": 6,
    "posts_read": 119,
    "days_visited": 6,
    "user": {
      "id": 151872,
      "username": "E-Designs",
      "name": "E Designs",
      "avatar_template": "/forum/user_avatar/www.freecodecamp.org/e-designs/{size}/87999_1.png",
      "title": null } },

  {
    "id": 86577,
    "likes_received": 2,
    "likes_given": 0,
    "topics_entered": 0,
    "topic_count": 0,
    "post_count": 0,
    "posts_read": 0,
    "days_visited": 0,
    "user": {
      "id": 86577,
      "username": "megantaylor",
      "name": "Megan Taylor",
      "avatar_template": "/forum/user_avatar/www.freecodecamp.org/megantaylor/{size}/90127_1.png",
      "title": null } },

  {
    "id": 29355,
    "likes_received": 2,
    "likes_given": 0,
    "topics_entered": 0,
    "topic_count": 0,
    "post_count": 0,
    "posts_read": 0,
    "days_visited": 0,
    "user": {
      "id": 29355,
      "username": "Magwit",
      "name": "Magwit",
      "avatar_template": "/forum/user_avatar/www.freecodecamp.org/magwit/{size}/16569_1.png",
      "title": null } },

  {
    "id": 61536,
    "likes_received": 2,
    "likes_given": 0,
    "topics_entered": 8,
    "topic_count": 1,
    "post_count": 0,
    "posts_read": 30,
    "days_visited": 2,
    "user": {
      "id": 61536,
      "username": "unhalium",
      "name": "Halim",
      "avatar_template": "/forum/user_avatar/www.freecodecamp.org/unhalium/{size}/101755_1.png",
      "title": null } },

  {
    "id": 92796,
    "likes_received": 2,
    "likes_given": 1,
    "topics_entered": 5,
    "topic_count": 0,
    "post_count": 1,
    "posts_read": 27,
    "days_visited": 2,
    "user": {
      "id": 92796,
      "username": "oddjob23",
      "name": "",
      "avatar_template": "/forum/user_avatar/www.freecodecamp.org/oddjob23/{size}/50163_1.png",
      "title": null } }],


  "total_rows_directory_items": 164202,
  "load_more_directory_items": "/forum/directory_items?page=1&period=weekly" };


const all_time = {
  "directory_items": [{
    "id": 61887,
    "time_read": 4316407,
    "likes_received": 3428,
    "likes_given": 505,
    "topics_entered": 21435,
    "topic_count": 52,
    "post_count": 10084,
    "posts_read": 93972,
    "days_visited": 605,
    "user": {
      "id": 61887,
      "username": "randelldawson",
      "name": "Randell Dawson",
      "avatar_template": "/forum/user_avatar/www.freecodecamp.org/randelldawson/{size}/56290_1.png",
      "title": null } },

  {
    "id": 6,
    "time_read": 309404,
    "likes_received": 2938,
    "likes_given": 1015,
    "topics_entered": 2167,
    "topic_count": 133,
    "post_count": 693,
    "posts_read": 30166,
    "days_visited": 601,
    "user": {
      "id": 6,
      "username": "QuincyLarson",
      "name": "Quincy Larson",
      "avatar_template": "/forum/user_avatar/www.freecodecamp.org/quincylarson/{size}/26_1.png",
      "title": "" } },

  {
    "id": 746,
    "time_read": 1161892,
    "likes_received": 2888,
    "likes_given": 614,
    "topics_entered": 11387,
    "topic_count": 16,
    "post_count": 5074,
    "posts_read": 33885,
    "days_visited": 745,
    "user": {
      "id": 746,
      "username": "ArielLeslie",
      "name": "Ariel",
      "avatar_template": "/forum/user_avatar/www.freecodecamp.org/arielleslie/{size}/44939_1.png",
      "title": "(totally not a bot)" } },

  {
    "id": 286,
    "time_read": 1363307,
    "likes_received": 2448,
    "likes_given": 1514,
    "topics_entered": 8991,
    "topic_count": 25,
    "post_count": 3162,
    "posts_read": 53825,
    "days_visited": 742,
    "user": {
      "id": 286,
      "username": "PortableStick",
      "name": "",
      "avatar_template": "/forum/user_avatar/www.freecodecamp.org/portablestick/{size}/65591_1.png",
      "title": "🍸" } },

  {
    "id": 75682,
    "time_read": 1055155,
    "likes_received": 1950,
    "likes_given": 406,
    "topics_entered": 4937,
    "topic_count": 64,
    "post_count": 2754,
    "posts_read": 22434,
    "days_visited": 574,
    "user": {
      "id": 75682,
      "username": "kevinSmith",
      "name": "Kevin Smith",
      "avatar_template": "/forum/user_avatar/www.freecodecamp.org/kevinsmith/{size}/72934_1.png",
      "title": "passes a Turing test 58.3% of the time" } },

  {
    "id": 120,
    "time_read": 1413514,
    "likes_received": 1853,
    "likes_given": 688,
    "topics_entered": 7741,
    "topic_count": 79,
    "post_count": 2006,
    "posts_read": 67632,
    "days_visited": 604,
    "user": {
      "id": 120,
      "username": "JacksonBates",
      "name": "Jackson Bates",
      "avatar_template": "/forum/user_avatar/www.freecodecamp.org/jacksonbates/{size}/66729_1.png",
      "title": "Code Eye for the Newb Guy" } },

  {
    "id": 84340,
    "time_read": 1638390,
    "likes_received": 1734,
    "likes_given": 515,
    "topics_entered": 7692,
    "topic_count": 50,
    "post_count": 1588,
    "posts_read": 53575,
    "days_visited": 527,
    "user": {
      "id": 84340,
      "username": "owel",
      "name": "Owel",
      "avatar_template": "/forum/user_avatar/www.freecodecamp.org/owel/{size}/48425_1.png",
      "title": null } },

  {
    "id": 4051,
    "time_read": 1750036,
    "likes_received": 1366,
    "likes_given": 490,
    "topics_entered": 15166,
    "topic_count": 68,
    "post_count": 2921,
    "posts_read": 100735,
    "days_visited": 806,
    "user": {
      "id": 4051,
      "username": "kevcomedia",
      "name": "Kev",
      "avatar_template": "/forum/user_avatar/www.freecodecamp.org/kevcomedia/{size}/71012_1.png",
      "title": "I'm a cat" } },

  {
    "id": 2193,
    "time_read": 1224049,
    "likes_received": 1066,
    "likes_given": 320,
    "topics_entered": 7052,
    "topic_count": 2,
    "post_count": 1516,
    "posts_read": 30476,
    "days_visited": 450,
    "user": {
      "id": 2193,
      "username": "DanCouper",
      "name": "Dan Couper",
      "avatar_template": "/forum/user_avatar/www.freecodecamp.org/dancouper/{size}/1479_1.png",
      "title": null } },

  {
    "id": 3541,
    "time_read": 1886817,
    "likes_received": 999,
    "likes_given": 763,
    "topics_entered": 13549,
    "topic_count": 14,
    "post_count": 1930,
    "posts_read": 82389,
    "days_visited": 812,
    "user": {
      "id": 3541,
      "username": "BenGitter",
      "name": "Benjamin van Zwienen",
      "avatar_template": "/forum/user_avatar/www.freecodecamp.org/bengitter/{size}/12192_1.png",
      "title": null } },

  {
    "id": 16648,
    "time_read": 644961,
    "likes_received": 911,
    "likes_given": 912,
    "topics_entered": 4506,
    "topic_count": 49,
    "post_count": 1179,
    "posts_read": 55717,
    "days_visited": 684,
    "user": {
      "id": 16648,
      "username": "IsaacAbrahamson",
      "name": "Isaac Abrahamson",
      "avatar_template": "/forum/user_avatar/www.freecodecamp.org/isaacabrahamson/{size}/30882_1.png",
      "title": "" } },

  {
    "id": 356,
    "time_read": 33502,
    "likes_received": 762,
    "likes_given": 54,
    "topics_entered": 82,
    "topic_count": 3,
    "post_count": 45,
    "posts_read": 860,
    "days_visited": 79,
    "user": {
      "id": 356,
      "username": "TinyRick88",
      "name": "Mooli88",
      "avatar_template": "/forum/user_avatar/www.freecodecamp.org/tinyrick88/{size}/1865_1.png",
      "title": null } },

  {
    "id": 3694,
    "time_read": 1120289,
    "likes_received": 724,
    "likes_given": 67,
    "topics_entered": 7135,
    "topic_count": 8,
    "post_count": 960,
    "posts_read": 48062,
    "days_visited": 534,
    "user": {
      "id": 3694,
      "username": "jenovs",
      "name": "",
      "avatar_template": "/forum/user_avatar/www.freecodecamp.org/jenovs/{size}/60963_1.png",
      "title": null } },

  {
    "id": 152907,
    "time_read": 529135,
    "likes_received": 661,
    "likes_given": 429,
    "topics_entered": 2922,
    "topic_count": 22,
    "post_count": 1945,
    "posts_read": 10353,
    "days_visited": 89,
    "user": {
      "id": 152907,
      "username": "hbar1st",
      "name": "Hbar1st",
      "avatar_template": "/forum/user_avatar/www.freecodecamp.org/hbar1st/{size}/88689_1.png",
      "title": null } },

  {
    "id": 90178,
    "time_read": 452940,
    "likes_received": 563,
    "likes_given": 429,
    "topics_entered": 1777,
    "topic_count": 12,
    "post_count": 533,
    "posts_read": 20632,
    "days_visited": 127,
    "user": {
      "id": 90178,
      "username": "cndragn",
      "name": "Candice",
      "avatar_template": "/forum/user_avatar/www.freecodecamp.org/cndragn/{size}/48701_1.png",
      "title": null } },

  {
    "id": 19039,
    "time_read": 75902,
    "likes_received": 502,
    "likes_given": 43,
    "topics_entered": 652,
    "topic_count": 6,
    "post_count": 88,
    "posts_read": 4610,
    "days_visited": 262,
    "user": {
      "id": 19039,
      "username": "elisecode247",
      "name": "elisecode247",
      "avatar_template": "/forum/user_avatar/www.freecodecamp.org/elisecode247/{size}/28927_1.png",
      "title": null } },

  {
    "id": 47232,
    "time_read": 325417,
    "likes_received": 452,
    "likes_given": 311,
    "topics_entered": 1628,
    "topic_count": 34,
    "post_count": 533,
    "posts_read": 10525,
    "days_visited": 98,
    "user": {
      "id": 47232,
      "username": "timotheap",
      "name": "Timotheap",
      "avatar_template": "avatar.png",
      "title": null 
    }},

  {
    "id": 77239,
    "time_read": 983258,
    "likes_received": 425,
    "likes_given": 144,
    "topics_entered": 5208,
    "topic_count": 18,
    "post_count": 601,
    "posts_read": 29747,
    "days_visited": 561,
    "user": {
      "id": 77239,
      "username": "lionel-rowe",
      "name": "Lionel Rowe",
      "avatar_template": "/forum/user_avatar/www.freecodecamp.org/lionel-rowe/{size}/41877_1.png",
      "title": null } },

  {
    "id": 216,
    "time_read": 452334,
    "likes_received": 421,
    "likes_given": 229,
    "topics_entered": 4006,
    "topic_count": 16,
    "post_count": 398,
    "posts_read": 38796,
    "days_visited": 788,
    "user": {
      "id": 216,
      "username": "rickstewart",
      "name": "Rick Stewart",
      "avatar_template": "/forum/user_avatar/www.freecodecamp.org/rickstewart/{size}/66490_1.png",
      "title": null } },

  {
    "id": 7909,
    "time_read": 482195,
    "likes_received": 414,
    "likes_given": 812,
    "topics_entered": 2022,
    "topic_count": 93,
    "post_count": 1483,
    "posts_read": 21089,
    "days_visited": 468,
    "user": {
      "id": 7909,
      "username": "JohnnyBizzel",
      "name": "Johnny Bizzel",
      "avatar_template": "/forum/user_avatar/www.freecodecamp.org/johnnybizzel/{size}/37661_1.png",
      "title": null } },

  {
    "id": 123008,
    "time_read": 442070,
    "likes_received": 412,
    "likes_given": 477,
    "topics_entered": 1772,
    "topic_count": 24,
    "post_count": 402,
    "posts_read": 11573,
    "days_visited": 364,
    "user": {
      "id": 123008,
      "username": "camper",
      "name": "Camper",
      "avatar_template": "/forum/user_avatar/www.freecodecamp.org/camper/{size}/67027_1.png",
      "title": "" } },

  {
    "id": 183,
    "time_read": 119134,
    "likes_received": 407,
    "likes_given": 87,
    "topics_entered": 560,
    "topic_count": 38,
    "post_count": 223,
    "posts_read": 3220,
    "days_visited": 307,
    "user": {
      "id": 183,
      "username": "Selhar1",
      "name": "Selhar",
      "avatar_template": "/forum/user_avatar/www.freecodecamp.org/selhar1/{size}/41868_1.png",
      "title": null } },

  {
    "id": 46594,
    "time_read": 640229,
    "likes_received": 404,
    "likes_given": 458,
    "topics_entered": 2379,
    "topic_count": 9,
    "post_count": 551,
    "posts_read": 27628,
    "days_visited": 578,
    "user": {
      "id": 46594,
      "username": "Soupedenuit",
      "name": "Tony Whomever",
      "avatar_template": "/forum/user_avatar/www.freecodecamp.org/soupedenuit/{size}/32478_1.png",
      "title": "mostly harmless" } },

  {
    "id": 86405,
    "time_read": 373494,
    "likes_received": 404,
    "likes_given": 75,
    "topics_entered": 3036,
    "topic_count": 43,
    "post_count": 1234,
    "posts_read": 11315,
    "days_visited": 148,
    "user": {
      "id": 86405,
      "username": "shimphillip",
      "name": "Phil",
      "avatar_template": "/forum/user_avatar/www.freecodecamp.org/shimphillip/{size}/79619_1.png",
      "title": "" } },

  {
    "id": 3933,
    "time_read": 603943,
    "likes_received": 397,
    "likes_given": 285,
    "topics_entered": 2102,
    "topic_count": 25,
    "post_count": 611,
    "posts_read": 19195,
    "days_visited": 261,
    "user": {
      "id": 3933,
      "username": "MARKJ78",
      "name": "Strictly Business",
      "avatar_template": "/forum/user_avatar/www.freecodecamp.org/markj78/{size}/2673_1.png",
      "title": "" } },

  {
    "id": 16065,
    "time_read": 774796,
    "likes_received": 390,
    "likes_given": 31,
    "topics_entered": 3056,
    "topic_count": 7,
    "post_count": 368,
    "posts_read": 26992,
    "days_visited": 793,
    "user": {
      "id": 16065,
      "username": "astv99",
      "name": "Steve",
      "avatar_template": "/forum/user_avatar/www.freecodecamp.org/astv99/{size}/66820_1.png",
      "title": null } },

  {
    "id": 828,
    "time_read": 142815,
    "likes_received": 387,
    "likes_given": 451,
    "topics_entered": 1697,
    "topic_count": 35,
    "post_count": 371,
    "posts_read": 19709,
    "days_visited": 388,
    "user": {
      "id": 828,
      "username": "michaelhenderson",
      "name": "Michael Henderson",
      "avatar_template": "/forum/user_avatar/www.freecodecamp.org/michaelhenderson/{size}/62184_1.png",
      "title": "Leader" } },

  {
    "id": 73269,
    "time_read": 422548,
    "likes_received": 349,
    "likes_given": 211,
    "topics_entered": 2184,
    "topic_count": 12,
    "post_count": 450,
    "posts_read": 11435,
    "days_visited": 322,
    "user": {
      "id": 73269,
      "username": "honmanyau",
      "name": "Honman Yau",
      "avatar_template": "/forum/user_avatar/www.freecodecamp.org/honmanyau/{size}/45787_1.png",
      "title": null } },

  {
    "id": 1615,
    "time_read": 104652,
    "likes_received": 349,
    "likes_given": 76,
    "topics_entered": 517,
    "topic_count": 9,
    "post_count": 145,
    "posts_read": 3025,
    "days_visited": 72,
    "user": {
      "id": 1615,
      "username": "ChadKreutzer",
      "name": "Chad Kreutzer",
      "avatar_template": "/forum/user_avatar/www.freecodecamp.org/chadkreutzer/{size}/1102_1.png",
      "title": null } },

  {
    "id": 105075,
    "time_read": 247891,
    "likes_received": 332,
    "likes_given": 616,
    "topics_entered": 1725,
    "topic_count": 61,
    "post_count": 826,
    "posts_read": 7108,
    "days_visited": 265,
    "user": {
      "id": 105075,
      "username": "nsuchy",
      "name": "Nathaniel Suchy",
      "avatar_template": "/forum/user_avatar/www.freecodecamp.org/nsuchy/{size}/60979_1.png",
      "title": "Elixir Alchemist" } },

  {
    "id": 26762,
    "time_read": 269808,
    "likes_received": 300,
    "likes_given": 488,
    "topics_entered": 973,
    "topic_count": 13,
    "post_count": 196,
    "posts_read": 26115,
    "days_visited": 475,
    "user": {
      "id": 26762,
      "username": "tropicalchancer",
      "name": "Tropicalchancer",
      "avatar_template": "/forum/user_avatar/www.freecodecamp.org/tropicalchancer/{size}/16219_1.png",
      "title": null } },

  {
    "id": 151146,
    "time_read": 254148,
    "likes_received": 299,
    "likes_given": 9,
    "topics_entered": 798,
    "topic_count": 1,
    "post_count": 588,
    "posts_read": 2614,
    "days_visited": 51,
    "user": {
      "id": 151146,
      "username": "anon18662199",
      "name": null,
      "avatar_template": "/forum/letter_avatar_proxy/v2/letter/a/ecae2f/{size}.png",
      "title": null } },

  {
    "id": 2281,
    "time_read": 645157,
    "likes_received": 285,
    "likes_given": 39,
    "topics_entered": 3085,
    "topic_count": 6,
    "post_count": 725,
    "posts_read": 12947,
    "days_visited": 173,
    "user": {
      "id": 2281,
      "username": "sorinr",
      "name": "Sorin Ruse",
      "avatar_template": "/forum/user_avatar/www.freecodecamp.org/sorinr/{size}/87166_1.png",
      "title": null } },

  {
    "id": 5078,
    "time_read": 9101,
    "likes_received": 277,
    "likes_given": 14,
    "topics_entered": 87,
    "topic_count": 5,
    "post_count": 17,
    "posts_read": 464,
    "days_visited": 36,
    "user": {
      "id": 5078,
      "username": "superking84",
      "name": "Frank Harvey",
      "avatar_template": "/forum/letter_avatar_proxy/v2/letter/s/4da419/{size}.png",
      "title": null } },

  {
    "id": 62372,
    "time_read": 214959,
    "likes_received": 272,
    "likes_given": 45,
    "topics_entered": 775,
    "topic_count": 0,
    "post_count": 391,
    "posts_read": 3483,
    "days_visited": 85,
    "user": {
      "id": 62372,
      "username": "JM-Mendez",
      "name": "John Mendez",
      "avatar_template": "/forum/user_avatar/www.freecodecamp.org/jm-mendez/{size}/33681_1.png",
      "title": null } },

  {
    "id": 158598,
    "time_read": 428500,
    "likes_received": 267,
    "likes_given": 28,
    "topics_entered": 2757,
    "topic_count": 1,
    "post_count": 877,
    "posts_read": 12745,
    "days_visited": 73,
    "user": {
      "id": 158598,
      "username": "Sujith3021",
      "name": "Sujith kumar",
      "avatar_template": "/forum/user_avatar/www.freecodecamp.org/sujith3021/{size}/94202_1.png",
      "title": null } },

  {
    "id": 5158,
    "time_read": 6337,
    "likes_received": 260,
    "likes_given": 0,
    "topics_entered": 20,
    "topic_count": 1,
    "post_count": 6,
    "posts_read": 165,
    "days_visited": 6,
    "user": {
      "id": 5158,
      "username": "yoskakomba",
      "name": "Yoskakomba",
      "avatar_template": "/forum/user_avatar/www.freecodecamp.org/yoskakomba/{size}/3382_1.png",
      "title": null } },

  {
    "id": 72215,
    "time_read": 5417,
    "likes_received": 260,
    "likes_given": 32,
    "topics_entered": 19,
    "topic_count": 1,
    "post_count": 3,
    "posts_read": 142,
    "days_visited": 13,
    "user": {
      "id": 72215,
      "username": "itxchy",
      "name": "Matt Trifilo",
      "avatar_template": "/forum/user_avatar/www.freecodecamp.org/itxchy/{size}/74652_1.png",
      "title": null } },

  {
    "id": 147554,
    "time_read": 643032,
    "likes_received": 245,
    "likes_given": 149,
    "topics_entered": 2391,
    "topic_count": 3,
    "post_count": 418,
    "posts_read": 10007,
    "days_visited": 163,
    "user": {
      "id": 147554,
      "username": "alhazen1",
      "name": "D Wilder",
      "avatar_template": "/forum/user_avatar/www.freecodecamp.org/alhazen1/{size}/85244_1.png",
      "title": null } },

  {
    "id": 749,
    "time_read": 131300,
    "likes_received": 244,
    "likes_given": 142,
    "topics_entered": 1135,
    "topic_count": 35,
    "post_count": 155,
    "posts_read": 10092,
    "days_visited": 477,
    "user": {
      "id": 749,
      "username": "bonham000",
      "name": "Sean Smith",
      "avatar_template": "/forum/user_avatar/www.freecodecamp.org/bonham000/{size}/47196_1.png",
      "title": null } },

  {
    "id": 95192,
    "time_read": 25848,
    "likes_received": 236,
    "likes_given": 36,
    "topics_entered": 215,
    "topic_count": 3,
    "post_count": 19,
    "posts_read": 1972,
    "days_visited": 68,
    "user": {
      "id": 95192,
      "username": "gcamacho079",
      "name": "Guadalupe Camacho",
      "avatar_template": "/forum/user_avatar/www.freecodecamp.org/gcamacho079/{size}/51417_1.png",
      "title": null } },

  {
    "id": 276,
    "time_read": 187842,
    "likes_received": 226,
    "likes_given": 742,
    "topics_entered": 812,
    "topic_count": 9,
    "post_count": 279,
    "posts_read": 14290,
    "days_visited": 499,
    "user": {
      "id": 276,
      "username": "DarrenfJ",
      "name": "Darren F Joy",
      "avatar_template": "/forum/user_avatar/www.freecodecamp.org/darrenfj/{size}/83089_1.png",
      "title": null } },

  {
    "id": 13490,
    "time_read": 36029,
    "likes_received": 219,
    "likes_given": 61,
    "topics_entered": 124,
    "topic_count": 15,
    "post_count": 54,
    "posts_read": 5477,
    "days_visited": 128,
    "user": {
      "id": 13490,
      "username": "Errec",
      "name": "Raniro Coelho",
      "avatar_template": "/forum/user_avatar/www.freecodecamp.org/errec/{size}/15864_1.png",
      "title": null } },

  {
    "id": 49149,
    "time_read": 44594,
    "likes_received": 207,
    "likes_given": 42,
    "topics_entered": 190,
    "topic_count": 3,
    "post_count": 62,
    "posts_read": 2859,
    "days_visited": 117,
    "user": {
      "id": 49149,
      "username": "forkerino",
      "name": "Pieter",
      "avatar_template": "/forum/user_avatar/www.freecodecamp.org/forkerino/{size}/26793_1.png",
      "title": null } },

  {
    "id": 149310,
    "time_read": 408993,
    "likes_received": 206,
    "likes_given": 81,
    "topics_entered": 2767,
    "topic_count": 24,
    "post_count": 645,
    "posts_read": 13367,
    "days_visited": 121,
    "user": {
      "id": 149310,
      "username": "aditya_p",
      "name": "Aditya",
      "avatar_template": "/forum/user_avatar/www.freecodecamp.org/aditya_p/{size}/99447_1.png",
      "title": "" } },

  {
    "id": 15028,
    "time_read": 829619,
    "likes_received": 205,
    "likes_given": 30,
    "topics_entered": 5297,
    "topic_count": 22,
    "post_count": 360,
    "posts_read": 28400,
    "days_visited": 634,
    "user": {
      "id": 15028,
      "username": "JohnL3",
      "name": "John L3",
      "avatar_template": "/forum/user_avatar/www.freecodecamp.org/johnl3/{size}/16459_1.png",
      "title": null } },

  {
    "id": 32913,
    "time_read": 277307,
    "likes_received": 202,
    "likes_given": 0,
    "topics_entered": 1900,
    "topic_count": 10,
    "post_count": 200,
    "posts_read": 13130,
    "days_visited": 402,
    "user": {
      "id": 32913,
      "username": "jamesperrin",
      "name": "James Perrin",
      "avatar_template": "/forum/user_avatar/www.freecodecamp.org/jamesperrin/{size}/18447_1.png",
      "title": null } },

  {
    "id": 1150,
    "time_read": 230313,
    "likes_received": 200,
    "likes_given": 3,
    "topics_entered": 1461,
    "topic_count": 5,
    "post_count": 243,
    "posts_read": 9039,
    "days_visited": 382,
    "user": {
      "id": 1150,
      "username": "Marmiz",
      "name": "Marmiz",
      "avatar_template": "/forum/user_avatar/www.freecodecamp.org/marmiz/{size}/74845_1.png",
      "title": null } },

  {
    "id": 27036,
    "time_read": 177521,
    "likes_received": 199,
    "likes_given": 21,
    "topics_entered": 677,
    "topic_count": 12,
    "post_count": 215,
    "posts_read": 9459,
    "days_visited": 228,
    "user": {
      "id": 27036,
      "username": "arw2015",
      "name": "playing with a full deck, most days",
      "avatar_template": "/forum/user_avatar/www.freecodecamp.org/arw2015/{size}/15368_1.png",
      "title": null } },

  {
    "id": 10660,
    "time_read": 197773,
    "likes_received": 194,
    "likes_given": 170,
    "topics_entered": 1015,
    "topic_count": 46,
    "post_count": 386,
    "posts_read": 6636,
    "days_visited": 315,
    "user": {
      "id": 10660,
      "username": "AdventureBear",
      "name": "Adventure Bear",
      "avatar_template": "/forum/user_avatar/www.freecodecamp.org/adventurebear/{size}/74786_1.png",
      "title": null } }],


  "total_rows_directory_items": 164202,
  "load_more_directory_items": "/forum/directory_items?page=1&period=all" };