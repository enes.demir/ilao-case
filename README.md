## iLAO CASE PROJECT

##### TEKNOLOJİLER
Kullanılan web teknolojileri
- Nodejs
- Reactjs
- Expressjs
- Jquery - duh
## Kurulum
Projenin yerel bir sistemde çalıştırılabilmesi için gerekenler aşağıda belirtilmiştir:
### Wİndows
- Nodejs son sürümünü sisteminize yükleyin
- Komut Sistemini yönetici olarak çalıştırın
>$ npm i gulp-cli -g
- Aşağıdaki komutu projeye bağlı olarak girin (zorunlu değil ama yerel sisteminizde projenin düzenli bir dizin yapısında durmasını dilerseniz tavsiye edilir)
>$ mkdir ilao-case && cd ilao-case
- Eğer SSH-Key tanımlı ise veya Git bash kullanılıyorsa aşağıdaki komutu girin
>$ git clone git@gitlab.com:enes.demir/ilao-case.git
- Eğer SSH-Key tanımlı değilse ve Windows komut sistemi ya da powershell kullanılıyorsa aşağıdaki komutu girin
>$ git clone https://gitlab.com/enes.demir/ilao-case.git
- Proje yerel sisteme başarılı bir şekilde çekildiyse (pull) aşağıdaki komutları sırasıyla girin
>$ cd ilao-case

>$ rm -rf .git

>$ git init

>$ git remote add origin git@gitlab.com:enes.demir/ilao-case.git

>$ git remote add origin https://gitlab.com/enes.demir/ilao-case.git

>$ npm install

>$ npm start

### Linux/Mac

>$ sudo npm i gulp-cli -g

>$ mkdir ilao-case && cd ilao-case

>$ git clone git@gitlab.com:enes.demir/ilao-case.git

>$ git clone https://gitlab.com/enes.demir/ilao-case.git

>$ cd ilao-case

>$ npm i && npm start